"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var https_1 = require("https");
// util
var database_1 = require("./util/database");
// middlewares
var is_auth_middleware_1 = require("./middlewares/is-auth.middleware");
// routes
var crud_routes_1 = require("./routes/crud-routes");
var authRoutes = __importStar(require("./routes/auth-routes"));
var userRoutes = __importStar(require("./routes/user-routes"));
var document_handler_class_1 = require("./model/classes/document-handler.class");
var app = (0, express_1.default)();
app.use(express_1.default.static(path_1.default.join(__dirname, "public")));
app.use(express_1.default.json());
app.use("/auth", function (req, res, next) {
    req["__collectionName"] = document_handler_class_1.DocumentHandler.collectionNames.USER_COLLECTION;
    next();
}, authRoutes.router);
app.use("/users", is_auth_middleware_1.isAuth, function (req, res, next) {
    req["__collectionName"] = document_handler_class_1.DocumentHandler.collectionNames.USER_COLLECTION;
    next();
}, userRoutes.router, (0, crud_routes_1.getRouter)(true, false));
app.use("/cities", is_auth_middleware_1.isAuth, function (req, res, next) {
    req["__collectionName"] = document_handler_class_1.DocumentHandler.collectionNames.CITY_COLLECTION;
    next();
}, (0, crud_routes_1.getRouter)());
app.use("/countries", is_auth_middleware_1.isAuth, function (req, res, next) {
    req["__collectionName"] =
        document_handler_class_1.DocumentHandler.collectionNames.COUNTRY_COLLECTION;
    next();
}, (0, crud_routes_1.getRouter)());
app.use(function (error, req, res, next) {
    console.log(error);
    var status = error.statusCode || 500;
    var data = error.data;
    // error.message -> string koji je prosledjen konstruktoru
    var message = error.message;
    return res.status(status).json({
        msg: message,
        data: data,
    });
});
(0, database_1.mongoConnect)(function () {
    (0, database_1.getDB)().then(function (db) {
        document_handler_class_1.DocumentHandler.db = db;
        (0, https_1.createServer)({
            cert: fs_1.default.readFileSync(path_1.default.join(__dirname, "../cert", "cert.pem")),
            key: fs_1.default.readFileSync(path_1.default.join(__dirname, "../cert", "key.pem")),
        }, app).listen({ port: 3443, host: "0.0.0.0" });
    });
});
//# sourceMappingURL=app.js.map