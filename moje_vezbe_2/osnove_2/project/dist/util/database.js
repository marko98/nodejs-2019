"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDB = exports.mongoConnect = void 0;
var mongodb_1 = require("mongodb");
var _db;
var mongoConnect = function (callback) {
    mongodb_1.MongoClient.connect("mongodb://localhost:27017/swipe")
        .then(function (client) {
        _db = client.db();
        callback();
    })
        .catch(function (err) {
        console.log(err);
        throw err;
    });
};
exports.mongoConnect = mongoConnect;
var getDB = function () {
    return new Promise(function (resolve, reject) {
        if (_db)
            resolve(_db);
        else
            reject("No database found!");
    });
};
exports.getDB = getDB;
//# sourceMappingURL=database.js.map