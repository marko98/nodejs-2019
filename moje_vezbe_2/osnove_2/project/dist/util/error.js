"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorMiddleware = void 0;
var errorMiddleware = function (error, req, res, next) {
    console.log(error);
    var status = error.statusCode || 500;
    var data = error.data;
    // error.message -> string koji je prosledjen konstruktoru
    var message = error.message;
    return res.status(status).json({
        msg: message,
        data: data,
    });
};
exports.errorMiddleware = errorMiddleware;
//# sourceMappingURL=error.js.map