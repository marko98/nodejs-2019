"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentHandler = exports.REST_API_METHODS = void 0;
var mongodb_1 = require("mongodb");
var validator_1 = __importDefault(require("validator"));
var document_out_validator_1 = require("./document-out-validator");
var lodash_1 = require("lodash");
exports.REST_API_METHODS = {
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
    GET: "GET",
};
var DocumentHandler = /** @class */ (function () {
    function DocumentHandler() {
    }
    // fns
    DocumentHandler.getDocRecipe = function (collectionName) {
        return new Promise(function (resolve, reject) {
            var docRecipe = DocumentHandler._docRecipes[collectionName];
            if (docRecipe)
                resolve((0, lodash_1.cloneDeep)(docRecipe));
            reject("Can't get document recipe that belongs to collection " + collectionName + ".");
        });
    };
    DocumentHandler.find = function (collectionName, attrsToPopulate, query) {
        // let attrsToPopulate: {} = {"__livesIn": {"$collectionName": "city", "__country": {"$collectionName": "country"}}};
        return DocumentHandler.db
            .listCollections({ name: collectionName })
            .toArray()
            .then(function (collections) {
            if (collections.length === 0)
                return Promise.reject("Collection " + collectionName + " doesn't exists.");
            // collection exists
            try {
                if (Object.prototype.hasOwnProperty.call(query, "_id"))
                    query["_id"] = new mongodb_1.ObjectId(query["_id"]);
            }
            catch (error) {
                return Promise.reject("Invalid document id in query.");
            }
            return DocumentHandler.db
                .collection(collectionName)
                .find(query)
                .toArray()
                .then(function (docs) {
                // DA LI NAM JE VALIDACIJA ZA DOBAVLJANJE VEC UNETIH DOKUMENATA IZ BAZE POTREBNA?
                //   validate docs
                // return Promise.all(
                //   docs.map((doc) => DocumentHandler.validate(collectionName, doc))
                // );
                // return Promise.all(docs.map((doc) => doc));
                return Promise.all(docs.map(function (doc) {
                    return document_out_validator_1.DocumentOutValidator.validate(collectionName, doc, exports.REST_API_METHODS.GET);
                }));
            })
                .then(function (validatedDocs) {
                var data = [];
                for (var _i = 0, validatedDocs_1 = validatedDocs; _i < validatedDocs_1.length; _i++) {
                    var doc = validatedDocs_1[_i];
                    for (var key in attrsToPopulate) {
                        if (Object.prototype.hasOwnProperty.call(doc, key)) {
                            var collectionName_1 = attrsToPopulate[key]["$collectionName"];
                            var query_1 = attrsToPopulate[key]["$query"] || {};
                            if (Array.isArray(doc[key])) {
                                for (var _a = 0, _b = doc[key]; _a < _b.length; _a++) {
                                    var objId = _b[_a];
                                    if (objId instanceof mongodb_1.ObjectId) {
                                        data.push({
                                            doc: doc,
                                            key: key,
                                            isArray: true,
                                            promise: DocumentHandler.find(collectionName_1, attrsToPopulate[key], __assign({ _id: objId }, query_1)),
                                        });
                                    }
                                }
                                doc[key] = doc[key].filter(function (el) { return !(el instanceof mongodb_1.ObjectId); });
                            }
                            else if (doc[key] instanceof mongodb_1.ObjectId) {
                                data.push({
                                    doc: doc,
                                    key: key,
                                    isArray: false,
                                    promise: DocumentHandler.find(collectionName_1, attrsToPopulate[key], __assign({ _id: doc[key] }, query_1)),
                                });
                                doc[key] = undefined;
                            }
                        }
                    }
                }
                return Promise.all(data.map(function (dataEl) { return dataEl.promise; })).then(function (values) {
                    // values = values.flat();
                    if (values.length === data.length) {
                        for (var index = 0; index < data.length; index++) {
                            var value = values[index];
                            var dataEl = data[index];
                            if (dataEl.isArray && value[0]) {
                                dataEl.doc[dataEl.key].push(value[0]);
                            }
                            else if (value[0]) {
                                dataEl.doc[dataEl.key] = value[0];
                            }
                        }
                    }
                    return Promise.resolve(validatedDocs);
                });
            });
        });
    };
    DocumentHandler.save = function (collectionName, doc) {
        return new Promise(function (resolve, reject) {
            try {
                if (doc["_id"])
                    return reject("Can't save doc, it has _id.");
                else {
                    if (Array.isArray(doc))
                        return reject("Can't save list.");
                    //   checking for attrs that are docs
                    for (var key in doc) {
                        if (key.includes("__")) {
                            if (Array.isArray(doc[key])) {
                                for (var _i = 0, _a = doc[key]; _i < _a.length; _i++) {
                                    var _id = _a[_i];
                                    if (!_id)
                                        return reject("Some attrs, of document from " + collectionName + " collection, that are docs have invalid id.");
                                    try {
                                        _id = new mongodb_1.ObjectId(_id);
                                    }
                                    catch (error) {
                                        return reject("Some attrs, of document from " + collectionName + " collection, that are docs have invalid id.");
                                    }
                                }
                            }
                            else if (doc[key] && !(doc[key] instanceof mongodb_1.ObjectId)) {
                                try {
                                    doc[key] = new mongodb_1.ObjectId(doc[key]);
                                }
                                catch (error) {
                                    return reject("Some attrs, of document from " + collectionName + " collection, that are docs have invalid id.");
                                }
                            }
                        }
                    }
                    // return DocumentHandler.validate(
                    //   collectionName,
                    //   doc,
                    //   REST_API_METHODS.POST
                    // )
                    //   .then((validatedDoc) => {
                    //     return DocumentHandler.db
                    //       .collection(collectionName)
                    //       .insertOne(validatedDoc)
                    //       .then((result: InsertOneResult) => {
                    //         resolve({
                    //           document: validatedDoc,
                    //           result: result,
                    //         });
                    //       })
                    //       .catch((errorMsg) =>
                    //         reject(
                    //           `Document from ${collectionName} collection couldn't be created.`
                    //         )
                    //       );
                    //   })
                    //   .catch((errorMsg: string) => reject(errorMsg));
                    return DocumentHandler.db
                        .collection(collectionName)
                        .insertOne(doc)
                        .then(function (result) {
                        resolve({
                            document: doc,
                            result: result,
                        });
                    })
                        .catch(function (errorMsg) {
                        return reject("Document from " + collectionName + " collection couldn't be created.");
                    });
                }
            }
            catch (errorMsg) {
                reject("Can't save document, error occurred.");
            }
        });
    };
    DocumentHandler.update = function (collectionName, doc) {
        return new Promise(function (resolve, reject) {
            try {
                if (doc["_id"]) {
                    if (Array.isArray(doc))
                        reject("Can't update list.");
                    //   checking for attrs that are docs
                    for (var key in doc) {
                        if (key.includes("__")) {
                            if (Array.isArray(doc[key])) {
                                for (var _i = 0, _a = doc[key]; _i < _a.length; _i++) {
                                    var _id = _a[_i];
                                    if (!_id)
                                        return reject("Some attrs, of document from " + collectionName + " collection, that are docs have invalid id.");
                                    try {
                                        _id = new mongodb_1.ObjectId(_id);
                                    }
                                    catch (error) {
                                        return reject("Some attrs, of document from " + collectionName + " collection, that are docs have invalid id.");
                                    }
                                }
                            }
                            else if (doc[key] && !(doc[key] instanceof mongodb_1.ObjectId)) {
                                try {
                                    doc[key] = new mongodb_1.ObjectId(doc[key]);
                                }
                                catch (error) {
                                    return reject("Some attrs, of document from " + collectionName + " collection, that are docs have invalid id.");
                                }
                            }
                        }
                    }
                    return (DocumentHandler.db
                        .collection(collectionName)
                        // .replaceOne({ _id: (<ObjectId>entity.get_id()).toString() }, doc)
                        .updateOne({ _id: doc["_id"] }, { $set: __assign({}, doc) })
                        .then(function (result) {
                        resolve({
                            document: doc,
                            result: result,
                        });
                    })
                        .catch(function (error) {
                        return reject("Document from " + collectionName + " collection couldn't be updated.");
                    }));
                }
                else
                    reject("Can't update document, it doesn't have _id.");
            }
            catch (error) {
                //   console.log(error);
                reject("Can't update document, error occurred.");
            }
        });
    };
    DocumentHandler.delete = function (collectionName, doc) {
        return new Promise(function (resolve, reject) {
            try {
                if (doc["_id"]) {
                    if (Array.isArray(doc))
                        reject("Can't delete list.");
                    return DocumentHandler.db
                        .collection(collectionName)
                        .deleteOne({ _id: doc["_id"] })
                        .then(function (result) {
                        resolve({
                            document: doc,
                            result: result,
                        });
                    })
                        .catch(function (error) {
                        return reject("Document from " + collectionName + " collection couldn't be deleted.");
                    });
                }
                else
                    reject("Can't delete document, it doesn't have _id.");
            }
            catch (error) {
                //   console.log(error);
                reject("Can't delete document, error occurred.");
            }
        });
    };
    DocumentHandler.collectionNames = {
        USER_COLLECTION: "user",
        CITY_COLLECTION: "city",
        COUNTRY_COLLECTION: "country",
    };
    // MOZDA fieldsThatMustNotBeInDoc NIJE NI POTREBAN
    DocumentHandler._docRecipes = {
        user: {
            validationIn: {
                POST: {
                    fieldsThatMustHaveValue: ["password", "email", "name", "surname"],
                    fieldsThatMustNotBeInDoc: ["_id", "iconPath"],
                    allFieldsRequired: true,
                },
                PUT: {
                    allowedFieldsToChangeValue: ["name", "surname", "__livesIn", "email"],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
                DELETE: {
                    fieldsThatMustHaveValue: ["_id"],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
                GET: {
                    fieldsThatMustHaveValue: [],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
            },
            validationOut: {
                fieldsThatAreForbiddenToReturn: ["password"],
            },
            fields: {
                _id: {
                    validateFn: function (_id) {
                        return Promise.resolve(_id);
                    },
                    defaultValue: null,
                },
                name: {
                    validateFn: function (name) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // name
                                if (!name)
                                    reject("User must have a name.");
                                name = name.trim();
                                if (validator_1.default.isEmpty(name))
                                    reject("User's name mustn't be empty.");
                                else if (!validator_1.default.isLength(name, { min: 2, max: 20 }))
                                    reject("User's name length is invalid.");
                                else if (!validator_1.default.isAlpha(name, undefined, { ignore: "-" }))
                                    reject("User's name must contains only letters.");
                                resolve(name);
                            }
                            catch (error) {
                                reject("User validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
                surname: {
                    validateFn: function (surname) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // surname
                                if (!surname)
                                    reject("User must have a surname.");
                                surname = surname.trim();
                                if (validator_1.default.isEmpty(surname))
                                    reject("User's surname mustn't be empty.");
                                else if (!validator_1.default.isLength(surname, { min: 2, max: 20 }))
                                    reject("User's surname length is invalid.");
                                else if (!validator_1.default.isAlpha(surname, undefined, { ignore: "-" }))
                                    reject("User's surname must contains only letters.");
                                resolve(surname);
                            }
                            catch (error) {
                                reject("User validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
                __livesIn: {
                    validateFn: function (__livesIn) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // __livesIn
                                if (Array.isArray(__livesIn))
                                    reject("User's __livesIn mustn't be an array.");
                                if (__livesIn instanceof mongodb_1.ObjectId)
                                    resolve(__livesIn);
                                if (typeof __livesIn === "object")
                                    resolve(__livesIn);
                                resolve(__livesIn);
                            }
                            catch (error) {
                                reject("User validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
                email: {
                    validateFn: function (email) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // email
                                if (!email)
                                    reject("User must have a email.");
                                email = email.trim();
                                if (validator_1.default.isEmpty(email))
                                    reject("User's email mustn't be empty.");
                                else if (!validator_1.default.isEmail(email))
                                    reject("User's email is invalid.");
                                // email must be unique
                                DocumentHandler.db
                                    .collection(DocumentHandler.collectionNames.USER_COLLECTION)
                                    .find({ email: email })
                                    .toArray()
                                    .then(function (users) {
                                    if (users.length === 0)
                                        resolve(email);
                                    reject("User with email: " + email + ", already exists.");
                                })
                                    .catch(function (error) {
                                    reject("User validation failed.");
                                });
                            }
                            catch (error) {
                                reject("User validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
                password: {
                    validateFn: function (password) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // password
                                if (!password)
                                    reject("User must have a password.");
                                password = password.trim();
                                if (validator_1.default.isEmpty(password))
                                    reject("User's password mustn't be empty.");
                                else if (!validator_1.default.isLength(password, { min: 2 }))
                                    reject("User's password length is invalid.");
                                else if (
                                // minimum one digit
                                // minimum one lowercase letter
                                // minimum one uppercase letter
                                // minimum one char from !@#$%^&*)(+=._-
                                !new RegExp("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*)(+=._-])").test(password))
                                    reject("User's password must contain at least one digit, one lowercase letter, one uppercase letter and one special character.");
                                resolve(password);
                            }
                            catch (error) {
                                reject("User validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
                iconPath: {
                    validateFn: function (iconPath) {
                        return Promise.resolve(iconPath);
                    },
                    defaultValue: null,
                },
            },
        },
        city: {
            validationIn: {
                POST: {
                    fieldsThatMustHaveValue: ["name"],
                    fieldsThatMustNotBeInDoc: ["_id"],
                    allFieldsRequired: true,
                },
                PUT: {
                    allowedFieldsToChangeValue: ["name"],
                    fieldsThatMustNotBeInDoc: ["_id"],
                    allFieldsRequired: false,
                },
                DELETE: {
                    fieldsThatMustHaveValue: ["_id"],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
                GET: {
                    fieldsThatMustHaveValue: [],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
            },
            validationOut: {
                fieldsThatAreForbiddenToReturn: [],
            },
            fields: {
                _id: {
                    validateFn: function (_id) {
                        return Promise.resolve(_id);
                    },
                    defaultValue: null,
                },
                name: {
                    validateFn: function (name) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // name
                                if (!name)
                                    reject("City must have a name.");
                                name = name.trim();
                                if (validator_1.default.isEmpty(name))
                                    reject("City's name mustn't be empty.");
                                else if (!validator_1.default.isLength(name, { min: 2, max: 20 }))
                                    reject("City's name length is invalid.");
                                else if (!validator_1.default.isAlpha(name, undefined, { ignore: " " }))
                                    reject("City's name must contains only letters.");
                                resolve(name);
                            }
                            catch (error) {
                                reject("City validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
                __country: {
                    validateFn: function (__country) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // __country
                                if (Array.isArray(__country))
                                    reject("City's __country mustn't be an array.");
                                if (__country instanceof mongodb_1.ObjectId)
                                    resolve(__country);
                                if (typeof __country === "object")
                                    resolve(__country);
                                resolve(__country);
                            }
                            catch (error) {
                                reject("City validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
            },
        },
        country: {
            validationIn: {
                POST: {
                    fieldsThatMustHaveValue: ["name"],
                    fieldsThatMustNotBeInDoc: ["_id"],
                    allFieldsRequired: true,
                },
                PUT: {
                    allowedFieldsToChangeValue: ["name"],
                    fieldsThatMustNotBeInDoc: ["_id"],
                    allFieldsRequired: false,
                },
                DELETE: {
                    fieldsThatMustHaveValue: ["_id"],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
                GET: {
                    fieldsThatMustHaveValue: [],
                    fieldsThatMustNotBeInDoc: [],
                    allFieldsRequired: false,
                },
            },
            validationOut: {
                fieldsThatAreForbiddenToReturn: [],
            },
            fields: {
                _id: {
                    validateFn: function (_id) {
                        return Promise.resolve(_id);
                    },
                    defaultValue: null,
                },
                name: {
                    validateFn: function (name) {
                        return new Promise(function (resolve, reject) {
                            try {
                                // name
                                if (!name)
                                    reject("Country must have a name.");
                                name = name.trim();
                                if (validator_1.default.isEmpty(name))
                                    reject("Country's name mustn't be empty.");
                                else if (!validator_1.default.isLength(name, { min: 2, max: 20 }))
                                    reject("Country's name length is invalid.");
                                else if (!validator_1.default.isAlpha(name, undefined, { ignore: " " }))
                                    reject("Country's name must contains only letters.");
                                resolve(name);
                            }
                            catch (error) {
                                reject("Country validation failed.");
                            }
                        });
                    },
                    defaultValue: null,
                },
            },
        },
    };
    return DocumentHandler;
}());
exports.DocumentHandler = DocumentHandler;
//# sourceMappingURL=document-handler.class.js.map