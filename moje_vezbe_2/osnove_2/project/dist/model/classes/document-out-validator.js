"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentOutValidator = void 0;
var document_handler_class_1 = require("./document-handler.class");
var DocumentOutValidator = /** @class */ (function () {
    function DocumentOutValidator() {
    }
    DocumentOutValidator.validate = function (collectionName, doc, restApiMethod, options) {
        return new Promise(function (resolve, reject) {
            return document_handler_class_1.DocumentHandler.getDocRecipe(collectionName)
                .then(function (docRecipe) {
                var filteredDoc = {};
                var fieldsThatAreForbiddenToReturn = docRecipe.validationOut.fieldsThatAreForbiddenToReturn;
                var _loop_1 = function (field) {
                    if (!fieldsThatAreForbiddenToReturn.find(function (forbiddenField) { return field === forbiddenField; }))
                        filteredDoc[field] = doc[field];
                };
                for (var field in doc) {
                    _loop_1(field);
                }
                return resolve(filteredDoc);
            })
                .catch(function (errorMsg) { return reject(errorMsg); });
        });
    };
    return DocumentOutValidator;
}());
exports.DocumentOutValidator = DocumentOutValidator;
//# sourceMappingURL=document-out-validator.js.map