"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentInValidator = void 0;
var document_handler_class_1 = require("./document-handler.class");
var DocumentInValidator = /** @class */ (function () {
    function DocumentInValidator() {
    }
    DocumentInValidator.validate = function (collectionName, doc, restApiMethod, docRecipe) {
        return new Promise(function (resolve, reject) {
            var validatedDoc = {};
            var filteredDoc = {};
            var fieldsThatMustHaveValue = [];
            // let fieldsThatMustNotBeInDoc: [] = [];
            var allowedFieldsToChangeValue = [];
            var allFieldsRequired = undefined;
            if (restApiMethod === document_handler_class_1.REST_API_METHODS.POST) {
                fieldsThatMustHaveValue =
                    docRecipe.validationIn.POST.fieldsThatMustHaveValue;
                // fieldsThatMustNotBeInDoc =
                //   docRecipe.validationIn.POST.fieldsThatMustNotBeInDoc;
                allFieldsRequired = docRecipe.validationIn.POST.allFieldsRequired;
            }
            else if (restApiMethod === document_handler_class_1.REST_API_METHODS.PUT) {
                allowedFieldsToChangeValue =
                    docRecipe.validationIn.PUT.allowedFieldsToChangeValue;
                // fieldsThatMustNotBeInDoc =
                //   docRecipe.validationIn.PUT.fieldsThatMustNotBeInDoc;
                allFieldsRequired = docRecipe.validationIn.PUT.allFieldsRequired;
            }
            else if (restApiMethod === document_handler_class_1.REST_API_METHODS.DELETE) {
                fieldsThatMustHaveValue =
                    docRecipe.validationIn.DELETE.fieldsThatMustHaveValue;
                // fieldsThatMustNotBeInDoc =
                //   docRecipe.validationIn.DELETE.fieldsThatMustNotBeInDoc;
                allFieldsRequired = docRecipe.validationIn.DELETE.allFieldsRequired;
            }
            else if (restApiMethod === document_handler_class_1.REST_API_METHODS.GET) {
                fieldsThatMustHaveValue =
                    docRecipe.validationIn.GET.fieldsThatMustHaveValue;
                // fieldsThatMustNotBeInDoc =
                //   docRecipe.validationIn.GET.fieldsThatMustNotBeInDoc;
                allFieldsRequired = docRecipe.validationIn.GET.allFieldsRequired;
            }
            if (restApiMethod === document_handler_class_1.REST_API_METHODS.POST ||
                restApiMethod === document_handler_class_1.REST_API_METHODS.DELETE ||
                restApiMethod === document_handler_class_1.REST_API_METHODS.GET) {
                //   POST, DELETE, GET
                for (var _i = 0, fieldsThatMustHaveValue_1 = fieldsThatMustHaveValue; _i < fieldsThatMustHaveValue_1.length; _i++) {
                    var fieldThatMustHaveValue = fieldsThatMustHaveValue_1[_i];
                    if (!(fieldThatMustHaveValue in doc))
                        return reject("Document validation failed, document is from " + collectionName + " collection, field " + fieldThatMustHaveValue + " is missing.");
                    if (!doc[fieldThatMustHaveValue])
                        return reject("Document validation failed, document is from " + collectionName + " collection, field " + fieldThatMustHaveValue + " that must have value.");
                    filteredDoc[fieldThatMustHaveValue] = doc[fieldThatMustHaveValue];
                }
            }
            else {
                var _loop_1 = function (field) {
                    if (allowedFieldsToChangeValue.find(function (allowedField) { return field === allowedField; }))
                        filteredDoc[field] = doc[field];
                };
                //   PUT
                for (var field in doc) {
                    _loop_1(field);
                }
            }
            if (allFieldsRequired) {
                for (var field in docRecipe.fields) {
                    if (field in doc) {
                        filteredDoc[field] = doc[field];
                    }
                    else {
                        filteredDoc[field] = docRecipe.fields[field].defaultValue;
                    }
                }
            }
            // let _filteredDoc: {} = {};
            // for (let field in filteredDoc) {
            //   if (!fieldsThatMustNotBeInDoc.find((_field) => _field === field))
            //     _filteredDoc[field] = filteredDoc[field];
            // }
            // filteredDoc = _filteredDoc;
            var data = [];
            for (var field in docRecipe.fields) {
                if (field in filteredDoc && filteredDoc[field]) {
                    data.push({
                        attrName: field,
                        promise: docRecipe.fields[field].validateFn(doc[field]),
                    });
                    filteredDoc[field] = undefined;
                }
            }
            return Promise.all(data.map(function (dataEl) { return dataEl.promise; }))
                .then(function (values) {
                if (values.length === data.length) {
                    for (var index = 0; index < data.length; index++) {
                        var value = values[index];
                        var dataEl = data[index];
                        filteredDoc[dataEl.attrName] = value;
                    }
                    validatedDoc = filteredDoc;
                    return resolve(validatedDoc);
                }
                else
                    reject("Document validation failed, document is from " + collectionName + " collection.");
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    return DocumentInValidator;
}());
exports.DocumentInValidator = DocumentInValidator;
//# sourceMappingURL=document-in-validator.class.js.map