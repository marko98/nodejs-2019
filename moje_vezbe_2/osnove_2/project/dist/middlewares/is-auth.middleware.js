"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAuth = void 0;
var jsonwebtoken_1 = require("jsonwebtoken");
var mongodb_1 = require("mongodb");
var consts_1 = require("../consts");
var isAuth = function (req, res, next) {
    /**
     * authHeader -> trebao bi ovako da izgleda:
     * 'Bearer nekavrednosttokena'
     */
    var authHeader = req.get("Authorization");
    if (!authHeader) {
        var error = new Error("Not authenticated.");
        error.statusCode = 401;
        throw error;
    }
    var token = authHeader.split(" ")[1];
    var decodedToken;
    try {
        /**
         * jwt.verify(token, 'somesupersecretsecret') vraca objekat(token) sa
         * payload podacima koji su postavljeni pri kreiranju tokena(controllers/auth.js 66 linija koda),
         *
         * mogu se videti i na jwt.io stranici prilikom unosa tokena(procitaj.txt 29 linija)
         *
         * payload-u se pristupa npr. decodedToken.userId
         */
        decodedToken = ((0, jsonwebtoken_1.verify)(token, consts_1.AUTH_TOKEN_SECRET));
    }
    catch (err) {
        err.statusCode = 500;
        throw err;
    }
    if (!decodedToken) {
        var error = new Error("Not authenticated.");
        error.statusCode = 401;
        throw error;
    }
    req["__userId"] = new mongodb_1.ObjectId(decodedToken.userId);
    return next();
};
exports.isAuth = isAuth;
//# sourceMappingURL=is-auth.middleware.js.map