"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = __importDefault(require("express"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var mongodb_1 = require("mongodb");
var multer_1 = __importDefault(require("multer"));
var uuid_1 = require("uuid");
var path_1 = __importDefault(require("path"));
var path_2 = require("../util/path");
var fs_1 = __importDefault(require("fs"));
// class
var document_handler_class_1 = require("../model/classes/document-handler.class");
// util
var document_in_validator_class_1 = require("../model/classes/document-in-validator.class");
var document_out_validator_1 = require("../model/classes/document-out-validator");
var router = express_1.default.Router();
exports.router = router;
router.put("/change-password", function (req, res, next) {
    // 1) proveri da li sadrzi staru sifru i novu
    // 2) proveri staru - podudaranje
    // 3) verifikuj novu
    // 4) izmeni
    try {
        var doc_1 = req.body;
        if (!doc_1._id) {
            var error = new Error("Document is invalid.");
            error.statusCode = 400;
            throw error;
        }
        doc_1._id = new mongodb_1.ObjectId(doc_1._id);
        if (!doc_1.oldPassword) {
            var error = (new Error("Document is invalid, old password required."));
            error.statusCode = 400;
            throw error;
        }
        if (!doc_1.newPassword) {
            var error = (new Error("Document is invalid, new password required."));
            error.statusCode = 400;
            throw error;
        }
        if (doc_1.oldPassword === doc_1.newPassword) {
            var error = (new Error("Document is invalid, new password and old are the same."));
            error.statusCode = 400;
            throw error;
        }
        var theDoc_1;
        var data_1;
        return document_handler_class_1.DocumentHandler.db
            .collection(req["__collectionName"])
            .findOne({ _id: doc_1._id })
            .then(function (_theDoc) {
            theDoc_1 = _theDoc;
            if (!theDoc_1) {
                var error = new Error("No document found to update.");
                error.statusCode = 404;
                throw error;
            }
            return bcryptjs_1.default.compare(doc_1.oldPassword, theDoc_1.password);
        })
            .then(function (isEqual) {
            if (!isEqual) {
                var error = new Error("Wrong old password.");
                error.statusCode = 401;
                throw error;
            }
            return document_handler_class_1.DocumentHandler.getDocRecipe(req["__collectionName"]);
        })
            .then(function (docRecipe) {
            (docRecipe.validationIn.PUT.allowedFieldsToChangeValue).push("password");
            return document_in_validator_class_1.DocumentInValidator.validate(req["__collectionName"], {
                _id: theDoc_1._id,
                password: doc_1.newPassword,
            }, document_handler_class_1.REST_API_METHODS.PUT, docRecipe);
        })
            .then(function (validatedInDoc) {
            theDoc_1.password = validatedInDoc.password;
            return bcryptjs_1.default.hash(validatedInDoc.password, 12);
        })
            .then(function (hashedPassword) {
            theDoc_1.password = hashedPassword;
            return document_handler_class_1.DocumentHandler.update(req["__collectionName"], theDoc_1);
        })
            .then(function (_data) {
            data_1 = _data;
            return document_handler_class_1.DocumentHandler.find(req["__collectionName"], {}, { _id: data_1.document._id });
        })
            .then(function (docs) {
            return document_out_validator_1.DocumentOutValidator.validate(req["__collectionName"], docs[0], document_handler_class_1.REST_API_METHODS.GET);
        })
            .then(function (validatedOutDoc) {
            data_1.document = validatedOutDoc;
            return res.status(200).json(data_1);
        })
            .catch(function (error) { return next(error); });
    }
    catch (error) {
        return next(error);
    }
});
var createFolders = function (folderRecipes, rootPath) {
    if (rootPath === void 0) { rootPath = path_2.rootDirname; }
    return new Promise(function (resolve, reject) {
        if (typeof folderRecipes === "string") {
            try {
                fs_1.default.mkdirSync(folderRecipes, { recursive: true });
                return resolve();
            }
            catch (err) {
                var error = (new Error("Creation of the folder with path " + folderRecipes + " failed."));
                error.statusCode = 500;
                return reject(error);
            }
        }
        else {
            var promises = [];
            for (var folderName in folderRecipes) {
                var folderPath = path_1.default.join(rootPath, folderName);
                try {
                    if (!fs_1.default.existsSync(folderPath))
                        fs_1.default.mkdirSync(folderPath);
                    var _folderRecipes = folderRecipes[folderName];
                    if (_folderRecipes)
                        promises.push(createFolders(_folderRecipes, folderPath));
                }
                catch (err) {
                    var error = (new Error("Creation of the folder with path " + folderPath + " failed."));
                    error.statusCode = 500;
                    return reject(error);
                }
            }
            return Promise.all(promises).then(function () {
                return resolve();
            });
        }
    });
};
// https://www.npmjs.com/package/multer
router.post("/change-icon", (0, multer_1.default)({
    storage: multer_1.default.diskStorage({
        destination: function (req, file, cb) {
            // folder images mora vec da postoji inace nece raditi
            // cb(null, "images");
            // 1) proveri da li postoji staticki fajl od tog usera
            // 2) izmeni user-a tj icon path field
            // 3) zapisi fajl na toj putanji
            var userId = req["__userId"];
            var theUser;
            var data;
            var destination;
            return document_handler_class_1.DocumentHandler.find(req["__collectionName"], {}, { _id: userId })
                .then(function (docs) {
                theUser = docs[0];
                if (!theUser) {
                    var error = new Error("User not found.");
                    error.statusCode = 401;
                    throw error;
                }
                // create folders where to put file
                destination = path_1.default.join(path_2.rootDirname, "public", "page-" + userId.toString());
                return createFolders(destination);
            })
                .then(function () {
                file.originalname = (0, uuid_1.v4)() + "_" + file.originalname;
                return document_handler_class_1.DocumentHandler.update(req["__collectionName"], __assign(__assign({}, theUser), { iconPath: path_1.default.join("page-" + userId.toString(), file.originalname) }));
            })
                .then(function (_data) {
                data = _data;
                if (data.result.matchedCount === 0) {
                    var error = new Error("No document found to update.");
                    error.statusCode = 404;
                    throw error;
                }
                // delete previous file
                if (theUser.iconPath)
                    try {
                        fs_1.default.unlinkSync(path_1.default.join(path_2.rootDirname, "public", theUser.iconPath));
                    }
                    catch (error) { }
                return cb(null, destination);
            })
                .catch(function (error) { return cb(error, null); });
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        },
    }),
    fileFilter: function (req, file, cb) {
        if (file.mimetype === "image/jpeg")
            cb(null, true);
        else
            cb(null, false);
    },
}).single("icon"), function (req, res, next) {
    var userId = req["__userId"];
    return document_handler_class_1.DocumentHandler.find(req["__collectionName"], {}, { _id: userId })
        .then(function (docs) {
        return document_out_validator_1.DocumentOutValidator.validate(req["__collectionName"], docs[0], document_handler_class_1.REST_API_METHODS.GET);
    })
        .then(function (validatedOutDoc) {
        return res.status(200).json(validatedOutDoc);
    })
        .catch(function (error) {
        return next(error);
    });
});
//# sourceMappingURL=user-routes.js.map