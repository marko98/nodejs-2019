"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = __importDefault(require("express"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var validator_1 = __importDefault(require("validator"));
var jsonwebtoken_1 = require("jsonwebtoken");
// class
var document_handler_class_1 = require("../model/classes/document-handler.class");
// util
var document_in_validator_class_1 = require("../model/classes/document-in-validator.class");
var document_out_validator_1 = require("../model/classes/document-out-validator");
var consts_1 = require("../consts");
var router = express_1.default.Router();
exports.router = router;
router.post("/sign-up", function (req, res, next) {
    try {
        var validatedDoc_1 = JSON.parse(req.body.data);
        if (!validator_1.default.isLength(validatedDoc_1.password, { min: 2, max: 20 })) {
            var error = new Error("User's password length is invalid.");
            error.statusCode = 400;
            throw error;
        }
        var data_1;
        return document_handler_class_1.DocumentHandler.getDocRecipe(req["__collectionName"])
            .then(function (docRecipe) {
            return document_in_validator_class_1.DocumentInValidator.validate(req["__collectionName"], req.body, document_handler_class_1.REST_API_METHODS.POST, docRecipe);
        })
            .then(function (validatedInDoc) {
            validatedDoc_1 = validatedInDoc;
            return bcryptjs_1.default.hash(validatedDoc_1.password, 12);
        })
            .then(function (hashedPassword) {
            validatedDoc_1.password = hashedPassword;
            return document_handler_class_1.DocumentHandler.save(req["__collectionName"], validatedDoc_1);
        })
            .then(function (_data) {
            data_1 = _data;
            return document_out_validator_1.DocumentOutValidator.validate(req["__collectionName"], data_1.document, document_handler_class_1.REST_API_METHODS.GET);
        })
            .then(function (validatedOutDoc) {
            data_1.document = validatedOutDoc;
            return res.status(201).json(data_1);
        })
            .catch(function (error) {
            return next(error);
        });
    }
    catch (error) {
        return next(error);
    }
});
router.post("/login", function (req, res, next) {
    // 1) find user with given email
    // 2) compare password
    // 3) return jwt
    try {
        var data_2 = req.body;
        if (!data_2.email) {
            var error = new Error("User's email is required.");
            error.statusCode = 400;
            throw error;
        }
        if (!data_2.password) {
            var error = new Error("User's password is required.");
            error.statusCode = 400;
            throw error;
        }
        var doc_1;
        return document_handler_class_1.DocumentHandler.db
            .collection(req["__collectionName"])
            .findOne({ email: data_2.email })
            .then(function (_doc) {
            doc_1 = _doc;
            if (!doc_1) {
                var error = new Error("Email or password is incorrect.");
                error.statusCode = 401;
                throw error;
            }
            return bcryptjs_1.default.compare(data_2.password, doc_1.password);
        })
            .then(function (isEqual) {
            if (!isEqual) {
                var error = new Error("Email or password is incorrect.");
                error.statusCode = 401;
                throw error;
            }
            // https://github.com/vercel/ms
            var expiresIn = "1d"; // "1y", "1d", "5m", "10s"
            return (0, jsonwebtoken_1.sign)({ email: data_2.email, userId: doc_1._id.toString() }, consts_1.AUTH_TOKEN_SECRET, { expiresIn: expiresIn });
        })
            .then(function (token) {
            return res.status(200).json({ token: token });
        })
            .catch(function (error) { return next(error); });
    }
    catch (error) {
        return next(error);
    }
});
//# sourceMappingURL=auth-routes.js.map