"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRouter = void 0;
var express_1 = __importDefault(require("express"));
var mongodb_1 = require("mongodb");
// class
var document_handler_class_1 = require("../model/classes/document-handler.class");
// util
var document_out_validator_1 = require("../model/classes/document-out-validator");
var document_in_validator_class_1 = require("../model/classes/document-in-validator.class");
var _getHandler = function (req, res, next) {
    var attrsToPopulate = req.body.attrsToPopulate || {};
    var query = req.body.query || {};
    var validatedDocs = [];
    return document_handler_class_1.DocumentHandler.find(req["__collectionName"], attrsToPopulate, query)
        .then(function (_validatedDocs) {
        validatedDocs = _validatedDocs;
        return Promise.all(validatedDocs.map(function (validatedDoc) {
            return document_out_validator_1.DocumentOutValidator.validate(req["__collectionName"], validatedDoc, document_handler_class_1.REST_API_METHODS.GET);
        }));
    })
        .then(function (_validatedDocs) {
        validatedDocs = _validatedDocs;
        return res.status(200).json(validatedDocs);
    })
        .catch(function (error) {
        return next(error);
    });
};
var _postHandler = function (req, res, next) {
    var data;
    return document_handler_class_1.DocumentHandler.getDocRecipe(req["__collectionName"])
        .then(function (docRecipe) {
        return document_in_validator_class_1.DocumentInValidator.validate(req["__collectionName"], req.body, document_handler_class_1.REST_API_METHODS.POST, docRecipe)
            .then(function (validatedInDoc) {
            return document_handler_class_1.DocumentHandler.save(req["__collectionName"], validatedInDoc);
        })
            .then(function (_data) {
            data = _data;
            return document_handler_class_1.DocumentHandler.find(req["__collectionName"], {}, { _id: data.document._id });
        })
            .then(function (docs) {
            return document_out_validator_1.DocumentOutValidator.validate(req["__collectionName"], docs[0], document_handler_class_1.REST_API_METHODS.GET);
        })
            .then(function (validatedOutDoc) {
            data.document = validatedOutDoc;
            return res.status(201).json(data);
        });
    })
        .catch(function (error) {
        return next(error);
    });
};
var _putHandler = function (req, res, next) {
    try {
        var doc_1 = req.body;
        if (!doc_1._id) {
            var error = new Error("Document is invalid.");
            error.statusCode = 400;
            throw error;
        }
        doc_1._id = new mongodb_1.ObjectId(doc_1._id);
        var data_1;
        return document_handler_class_1.DocumentHandler.getDocRecipe(req["__collectionName"])
            .then(function (docRecipe) {
            return document_in_validator_class_1.DocumentInValidator.validate(req["__collectionName"], doc_1, document_handler_class_1.REST_API_METHODS.PUT, docRecipe);
        })
            .then(function (validatedInDoc) {
            validatedInDoc._id = doc_1._id;
            return document_handler_class_1.DocumentHandler.update(req["__collectionName"], validatedInDoc);
        })
            .then(function (_data) {
            data_1 = _data;
            if (data_1.result.matchedCount === 0) {
                var error = new Error("No document found to update.");
                error.statusCode = 404;
                throw error;
            }
            return document_handler_class_1.DocumentHandler.find(req["__collectionName"], {}, { _id: data_1.document._id });
        })
            .then(function (docs) {
            return document_out_validator_1.DocumentOutValidator.validate(req["__collectionName"], docs[0], document_handler_class_1.REST_API_METHODS.GET);
        })
            .then(function (validatedOutDoc) {
            data_1.document = validatedOutDoc;
            return res.status(200).json(data_1);
        })
            .catch(function (error) {
            return next(error);
        });
    }
    catch (error) {
        return next(error);
    }
};
var _deleteHandler = function (req, res, next) {
    try {
        var doc = req.body;
        if (!doc._id) {
            var error = new Error("Document is invalid.");
            error.statusCode = 400;
            throw error;
        }
        doc._id = new mongodb_1.ObjectId(doc._id);
        return document_handler_class_1.DocumentHandler.delete(req["__collectionName"], doc)
            .then(function (data) {
            if (data.result.deletedCount === 0) {
                var error = new Error("No document found to delete.");
                error.statusCode = 404;
                throw error;
            }
            return res.status(200).json(data);
        })
            .catch(function (error) {
            return next(error);
        });
    }
    catch (error) {
        return next(error);
    }
};
var _deleteIdHandler = function (req, res, next) {
    try {
        if (!req.params["id"]) {
            var error = new Error("Document is invalid.");
            error.statusCode = 400;
            throw error;
        }
        var id = new mongodb_1.ObjectId(req.params["id"]);
        return document_handler_class_1.DocumentHandler.delete(req["__collectionName"], { _id: id })
            .then(function (data) {
            if (data.result.deletedCount === 0) {
                var error = new Error("No document found to delete.");
                error.statusCode = 404;
                throw error;
            }
            return res.status(200).json(data);
        })
            .catch(function (error) {
            return next(error);
        });
    }
    catch (error) {
        return next(error);
    }
};
var getRouter = function (getHandler, postHandler, putHandler, deleteHandler, deleteIdHandler) {
    if (getHandler === void 0) { getHandler = true; }
    if (postHandler === void 0) { postHandler = true; }
    if (putHandler === void 0) { putHandler = true; }
    if (deleteHandler === void 0) { deleteHandler = true; }
    if (deleteIdHandler === void 0) { deleteIdHandler = true; }
    var router = express_1.default.Router();
    if (getHandler)
        router.get("", _getHandler);
    if (postHandler)
        router.post("", _postHandler);
    if (putHandler)
        router.put("", _putHandler);
    if (deleteHandler)
        router.delete("", _deleteHandler);
    if (deleteIdHandler)
        router.delete("/:id", _deleteIdHandler);
    return router;
};
exports.getRouter = getRouter;
//# sourceMappingURL=crud-routes.js.map