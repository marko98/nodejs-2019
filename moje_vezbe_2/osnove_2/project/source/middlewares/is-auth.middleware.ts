import * as express from "express";
import { verify } from "jsonwebtoken";
import { ObjectId } from "mongodb";
import { AUTH_TOKEN_SECRET } from "../consts";

const isAuth = (req: express.Request, res: express.Response, next) => {
  /**
   * authHeader -> trebao bi ovako da izgleda:
   * 'Bearer nekavrednosttokena'
   */
  const authHeader = req.get("Authorization");
  if (!authHeader) {
    const error = <any>new Error(`Not authenticated.`);
    error.statusCode = 401;
    throw error;
  }
  const token = authHeader.split(" ")[1];
  let decodedToken: { email: string; userId: string };
  try {
    /**
     * jwt.verify(token, 'somesupersecretsecret') vraca objekat(token) sa
     * payload podacima koji su postavljeni pri kreiranju tokena(controllers/auth.js 66 linija koda),
     *
     * mogu se videti i na jwt.io stranici prilikom unosa tokena(procitaj.txt 29 linija)
     *
     * payload-u se pristupa npr. decodedToken.userId
     */
    decodedToken = <{ email: string; userId: string }>(
      verify(token, AUTH_TOKEN_SECRET)
    );
  } catch (err) {
    (<any>err).statusCode = 500;
    throw err;
  }

  if (!decodedToken) {
    const error = <any>new Error("Not authenticated.");
    error.statusCode = 401;
    throw error;
  }

  req["__userId"] = new ObjectId(decodedToken.userId);
  return next();
};
export { isAuth };
