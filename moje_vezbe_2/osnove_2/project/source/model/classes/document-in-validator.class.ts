import {
  DocumentHandler,
  REST_API_METHODS,
  REST_API_METHODS_TYPE,
} from "./document-handler.class";

export class DocumentInValidator {
  public static validate<T extends {}>(
    collectionName: string,
    doc: {},
    restApiMethod: REST_API_METHODS_TYPE,
    docRecipe: {
      validationIn: {
        POST: {
          fieldsThatMustHaveValue: [];
          fieldsThatMustNotBeInDoc: [];
          allFieldsRequired: boolean;
        };
        PUT: {
          allowedFieldsToChangeValue: [];
          fieldsThatMustNotBeInDoc: [];
          allFieldsRequired: boolean;
        };
        DELETE: {
          fieldsThatMustHaveValue: [];
          fieldsThatMustNotBeInDoc: [];
          allFieldsRequired: boolean;
        };
        GET: {
          fieldsThatMustHaveValue: [];
          fieldsThatMustNotBeInDoc: [];
          allFieldsRequired: boolean;
        };
      };
      fields: {
        [key: string]: {
          validateFn: Function;
          defaultValue: undefined | [];
        };
      };
    }
  ): Promise<T> {
    return new Promise((resolve, reject) => {
      let validatedDoc: {} = {};
      let filteredDoc: {} = {};

      let fieldsThatMustHaveValue: [] = [];
      // let fieldsThatMustNotBeInDoc: [] = [];
      let allowedFieldsToChangeValue: [] = [];
      let allFieldsRequired: boolean = undefined;

      if (restApiMethod === REST_API_METHODS.POST) {
        fieldsThatMustHaveValue =
          docRecipe.validationIn.POST.fieldsThatMustHaveValue;
        // fieldsThatMustNotBeInDoc =
        //   docRecipe.validationIn.POST.fieldsThatMustNotBeInDoc;
        allFieldsRequired = docRecipe.validationIn.POST.allFieldsRequired;
      } else if (restApiMethod === REST_API_METHODS.PUT) {
        allowedFieldsToChangeValue =
          docRecipe.validationIn.PUT.allowedFieldsToChangeValue;
        // fieldsThatMustNotBeInDoc =
        //   docRecipe.validationIn.PUT.fieldsThatMustNotBeInDoc;
        allFieldsRequired = docRecipe.validationIn.PUT.allFieldsRequired;
      } else if (restApiMethod === REST_API_METHODS.DELETE) {
        fieldsThatMustHaveValue =
          docRecipe.validationIn.DELETE.fieldsThatMustHaveValue;
        // fieldsThatMustNotBeInDoc =
        //   docRecipe.validationIn.DELETE.fieldsThatMustNotBeInDoc;
        allFieldsRequired = docRecipe.validationIn.DELETE.allFieldsRequired;
      } else if (restApiMethod === REST_API_METHODS.GET) {
        fieldsThatMustHaveValue =
          docRecipe.validationIn.GET.fieldsThatMustHaveValue;
        // fieldsThatMustNotBeInDoc =
        //   docRecipe.validationIn.GET.fieldsThatMustNotBeInDoc;
        allFieldsRequired = docRecipe.validationIn.GET.allFieldsRequired;
      }

      if (
        restApiMethod === REST_API_METHODS.POST ||
        restApiMethod === REST_API_METHODS.DELETE ||
        restApiMethod === REST_API_METHODS.GET
      ) {
        //   POST, DELETE, GET
        for (let fieldThatMustHaveValue of fieldsThatMustHaveValue) {
          if (!(fieldThatMustHaveValue in doc))
            return reject(
              `Document validation failed, document is from ${collectionName} collection, field ${fieldThatMustHaveValue} is missing.`
            );
          if (!doc[fieldThatMustHaveValue])
            return reject(
              `Document validation failed, document is from ${collectionName} collection, field ${fieldThatMustHaveValue} that must have value.`
            );

          filteredDoc[fieldThatMustHaveValue] = doc[fieldThatMustHaveValue];
        }
      } else {
        //   PUT
        for (let field in doc)
          if (
            allowedFieldsToChangeValue.find(
              (allowedField) => field === allowedField
            )
          )
            filteredDoc[field] = doc[field];
      }

      if (allFieldsRequired) {
        for (let field in docRecipe.fields) {
          if (field in doc) {
            filteredDoc[field] = doc[field];
          } else {
            filteredDoc[field] = docRecipe.fields[field].defaultValue;
          }
        }
      }

      // let _filteredDoc: {} = {};
      // for (let field in filteredDoc) {
      //   if (!fieldsThatMustNotBeInDoc.find((_field) => _field === field))
      //     _filteredDoc[field] = filteredDoc[field];
      // }
      // filteredDoc = _filteredDoc;

      let data: { attrName: string; promise: Promise<any> }[] = [];
      for (let field in docRecipe.fields) {
        if (field in filteredDoc && filteredDoc[field]) {
          data.push({
            attrName: field,
            promise: docRecipe.fields[field].validateFn(doc[field]),
          });

          filteredDoc[field] = undefined;
        }
      }

      return Promise.all(data.map((dataEl) => dataEl.promise))
        .then((values: any[]) => {
          if (values.length === data.length) {
            for (let index = 0; index < data.length; index++) {
              const value = values[index];
              const dataEl: {
                attrName: string;
                promise: Promise<any>;
              } = data[index];

              filteredDoc[dataEl.attrName] = value;
            }
            validatedDoc = filteredDoc;
            return resolve(<T>validatedDoc);
          } else
            reject(
              `Document validation failed, document is from ${collectionName} collection.`
            );
        })
        .catch((error: string) => {
          reject(error);
        });
    });
  }
}
