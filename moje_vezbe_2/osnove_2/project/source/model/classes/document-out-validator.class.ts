import {
  DocumentHandler,
  REST_API_METHODS,
  REST_API_METHODS_TYPE,
} from "./document-handler.class";

export class DocumentOutValidator {
  public static validate<T extends {}>(
    collectionName: string,
    doc: {},
    restApiMethod: REST_API_METHODS_TYPE,
    options?: {}
  ): Promise<T> {
    return new Promise((resolve, reject) => {
      return DocumentHandler.getDocRecipe(collectionName)
        .then(
          (docRecipe: {
            validationOut: {
              fieldsThatAreForbiddenToReturn: [];
            };
            fields: {
              [key: string]: {
                validateFn: Function;
                defaultValue: undefined | [];
              };
            };
          }) => {
            let filteredDoc: {} = {};

            let fieldsThatAreForbiddenToReturn =
              docRecipe.validationOut.fieldsThatAreForbiddenToReturn;

            for (let field in doc)
              if (
                !fieldsThatAreForbiddenToReturn.find(
                  (forbiddenField) => field === forbiddenField
                )
              )
                filteredDoc[field] = doc[field];

            return resolve(<T>filteredDoc);
          }
        )
        .catch((errorMsg: string) => reject(errorMsg));
    });
  }
}
