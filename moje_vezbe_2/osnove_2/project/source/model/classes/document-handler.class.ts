import {
  Db,
  ObjectId,
  InsertOneResult,
  UpdateResult,
  DeleteResult,
} from "mongodb";
import validator from "validator";
import { DocumentOutValidator } from "./document-out-validator.class";
import { cloneDeep } from "lodash";

export const REST_API_METHODS: {
  POST: "POST";
  PUT: "PUT";
  DELETE: "DELETE";
  GET: "GET";
} = {
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE",
  GET: "GET",
};

export type REST_API_METHODS_TYPE = "POST" | "PUT" | "DELETE" | "GET";

export class DocumentHandler {
  // attrs
  public static db: Db;
  public static collectionNames: { [key: string]: string } = {
    USER_COLLECTION: "user",
    CITY_COLLECTION: "city",
    COUNTRY_COLLECTION: "country",
  };

  // MOZDA fieldsThatMustNotBeInDoc NIJE NI POTREBAN
  private static _docRecipes: { [key: string]: any } = {
    user: {
      validationIn: {
        POST: {
          fieldsThatMustHaveValue: ["password", "email", "name", "surname"],
          fieldsThatMustNotBeInDoc: ["_id", "iconPath"],
          allFieldsRequired: true,
        },
        PUT: {
          allowedFieldsToChangeValue: ["name", "surname", "__livesIn", "email"],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
        DELETE: {
          fieldsThatMustHaveValue: ["_id"],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
        GET: {
          fieldsThatMustHaveValue: [],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
      },
      validationOut: {
        fieldsThatAreForbiddenToReturn: ["password"],
      },
      fields: {
        _id: {
          validateFn: function (_id: ObjectId): Promise<ObjectId> {
            return Promise.resolve(_id);
          },
          defaultValue: null,
        },
        name: {
          validateFn: function (name: string): Promise<string> {
            return new Promise((resolve, reject) => {
              try {
                // name
                if (!name) reject(`User must have a name.`);
                name = name.trim();
                if (validator.isEmpty(name))
                  reject(`User's name mustn't be empty.`);
                else if (!validator.isLength(name, { min: 2, max: 20 }))
                  reject(`User's name length is invalid.`);
                else if (!validator.isAlpha(name, undefined, { ignore: "-" }))
                  reject(`User's name must contains only letters.`);

                resolve(name);
              } catch (error) {
                reject(`User validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
        surname: {
          validateFn: function (surname: string): Promise<string> {
            return new Promise((resolve, reject) => {
              try {
                // surname
                if (!surname) reject(`User must have a surname.`);
                surname = surname.trim();
                if (validator.isEmpty(surname))
                  reject(`User's surname mustn't be empty.`);
                else if (!validator.isLength(surname, { min: 2, max: 20 }))
                  reject(`User's surname length is invalid.`);
                else if (
                  !validator.isAlpha(surname, undefined, { ignore: "-" })
                )
                  reject(`User's surname must contains only letters.`);

                resolve(surname);
              } catch (error) {
                reject(`User validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
        __livesIn: {
          validateFn: function (
            __livesIn: ObjectId | {}
          ): Promise<ObjectId | {}> {
            return new Promise((resolve, reject) => {
              try {
                // __livesIn
                if (Array.isArray(__livesIn))
                  reject(`User's __livesIn mustn't be an array.`);

                if (__livesIn instanceof ObjectId) resolve(__livesIn);
                if (typeof __livesIn === "object") resolve(__livesIn);
                resolve(__livesIn);
              } catch (error) {
                reject(`User validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
        email: {
          validateFn: function (email: string): Promise<string> {
            return new Promise((resolve, reject) => {
              try {
                // email
                if (!email) reject(`User must have a email.`);
                email = email.trim();
                if (validator.isEmpty(email))
                  reject(`User's email mustn't be empty.`);
                else if (!validator.isEmail(email))
                  reject(`User's email is invalid.`);

                // email must be unique
                DocumentHandler.db
                  .collection(DocumentHandler.collectionNames.USER_COLLECTION)
                  .find({ email: email })
                  .toArray()
                  .then((users: {}[]) => {
                    if (users.length === 0) resolve(email);
                    reject(`User with email: ${email}, already exists.`);
                  })
                  .catch((error) => {
                    reject(`User validation failed.`);
                  });
              } catch (error) {
                reject(`User validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
        password: {
          validateFn: function (password: string): Promise<string> {
            return new Promise((resolve, reject) => {
              try {
                // password
                if (!password) reject(`User must have a password.`);
                password = password.trim();
                if (validator.isEmpty(password))
                  reject(`User's password mustn't be empty.`);
                else if (!validator.isLength(password, { min: 2 }))
                  reject(`User's password length is invalid.`);
                else if (
                  // minimum one digit
                  // minimum one lowercase letter
                  // minimum one uppercase letter
                  // minimum one char from !@#$%^&*)(+=._-
                  !new RegExp(
                    "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*)(+=._-])"
                  ).test(password)
                )
                  reject(
                    `User's password must contain at least one digit, one lowercase letter, one uppercase letter and one special character.`
                  );

                resolve(password);
              } catch (error) {
                reject(`User validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
        iconPath: {
          validateFn: function (iconPath: string): Promise<string> {
            return Promise.resolve(iconPath);
          },
          defaultValue: null,
        },
      },
    },
    city: {
      validationIn: {
        POST: {
          fieldsThatMustHaveValue: ["name"],
          fieldsThatMustNotBeInDoc: ["_id"],
          allFieldsRequired: true,
        },
        PUT: {
          allowedFieldsToChangeValue: ["name"],
          fieldsThatMustNotBeInDoc: ["_id"],
          allFieldsRequired: false,
        },
        DELETE: {
          fieldsThatMustHaveValue: ["_id"],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
        GET: {
          fieldsThatMustHaveValue: [],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
      },
      validationOut: {
        fieldsThatAreForbiddenToReturn: [],
      },
      fields: {
        _id: {
          validateFn: function (_id: ObjectId): Promise<ObjectId> {
            return Promise.resolve(_id);
          },
          defaultValue: null,
        },
        name: {
          validateFn: function (name: string): Promise<string> {
            return new Promise((resolve, reject) => {
              try {
                // name
                if (!name) reject(`City must have a name.`);
                name = name.trim();
                if (validator.isEmpty(name))
                  reject(`City's name mustn't be empty.`);
                else if (!validator.isLength(name, { min: 2, max: 20 }))
                  reject(`City's name length is invalid.`);
                else if (!validator.isAlpha(name, undefined, { ignore: " " }))
                  reject(`City's name must contains only letters.`);
                resolve(name);
              } catch (error) {
                reject(`City validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
        __country: {
          validateFn: function (
            __country: ObjectId | {}
          ): Promise<ObjectId | {}> {
            return new Promise((resolve, reject) => {
              try {
                // __country
                if (Array.isArray(__country))
                  reject(`City's __country mustn't be an array.`);

                if (__country instanceof ObjectId) resolve(__country);
                if (typeof __country === "object") resolve(__country);
                resolve(__country);
              } catch (error) {
                reject(`City validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
      },
    },
    country: {
      validationIn: {
        POST: {
          fieldsThatMustHaveValue: ["name"],
          fieldsThatMustNotBeInDoc: ["_id"],
          allFieldsRequired: true,
        },
        PUT: {
          allowedFieldsToChangeValue: ["name"],
          fieldsThatMustNotBeInDoc: ["_id"],
          allFieldsRequired: false,
        },
        DELETE: {
          fieldsThatMustHaveValue: ["_id"],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
        GET: {
          fieldsThatMustHaveValue: [],
          fieldsThatMustNotBeInDoc: [],
          allFieldsRequired: false,
        },
      },
      validationOut: {
        fieldsThatAreForbiddenToReturn: [],
      },
      fields: {
        _id: {
          validateFn: function (_id: ObjectId): Promise<ObjectId> {
            return Promise.resolve(_id);
          },
          defaultValue: null,
        },
        name: {
          validateFn: function (name: string): Promise<string> {
            return new Promise((resolve, reject) => {
              try {
                // name
                if (!name) reject(`Country must have a name.`);
                name = name.trim();
                if (validator.isEmpty(name))
                  reject(`Country's name mustn't be empty.`);
                else if (!validator.isLength(name, { min: 2, max: 20 }))
                  reject(`Country's name length is invalid.`);
                else if (!validator.isAlpha(name, undefined, { ignore: " " }))
                  reject(`Country's name must contains only letters.`);
                resolve(name);
              } catch (error) {
                reject(`Country validation failed.`);
              }
            });
          },
          defaultValue: null,
        },
      },
    },
  };

  // fns
  public static getDocRecipe(collectionName: string): Promise<{
    validationIn: {
      POST: {
        fieldsThatMustHaveValue: [];
        fieldsThatMustNotBeInDoc: [];
        allFieldsRequired: boolean;
      };
      PUT: {
        allowedFieldsToChangeValue: [];
        fieldsThatMustNotBeInDoc: [];
        allFieldsRequired: boolean;
      };
      DELETE: {
        fieldsThatMustHaveValue: [];
        fieldsThatMustNotBeInDoc: [];
        allFieldsRequired: boolean;
      };
      GET: {
        fieldsThatMustHaveValue: [];
        fieldsThatMustNotBeInDoc: [];
        allFieldsRequired: boolean;
      };
    };
    validationOut: {
      fieldsThatAreForbiddenToReturn: [];
    };
    fields: {
      [key: string]: { validateFn: Function; defaultValue: undefined | [] };
    };
  }> {
    return new Promise((resolve, reject) => {
      let docRecipe = DocumentHandler._docRecipes[collectionName];
      if (docRecipe) resolve(cloneDeep(docRecipe));
      reject(
        `Can't get document recipe that belongs to collection ${collectionName}.`
      );
    });
  }

  public static find(
    collectionName: string,
    attrsToPopulate: {},
    query: {}
  ): Promise<{}[]> {
    // let attrsToPopulate: {} = {"__livesIn": {"$collectionName": "city", "__country": {"$collectionName": "country"}}};

    return DocumentHandler.db
      .listCollections({ name: collectionName })
      .toArray()
      .then((collections: any[]) => {
        if (collections.length === 0)
          return Promise.reject(`Collection ${collectionName} doesn't exists.`);

        // collection exists

        try {
          if (Object.prototype.hasOwnProperty.call(query, "_id"))
            query["_id"] = new ObjectId(query["_id"]);
        } catch (error) {
          return Promise.reject(`Invalid document id in query.`);
        }

        return DocumentHandler.db
          .collection(collectionName)
          .find(query)
          .toArray()
          .then((docs) => {
            // DA LI NAM JE VALIDACIJA ZA DOBAVLJANJE VEC UNETIH DOKUMENATA IZ BAZE POTREBNA?
            //   validate docs
            // return Promise.all(
            //   docs.map((doc) => DocumentHandler.validate(collectionName, doc))
            // );
            // return Promise.all(docs.map((doc) => doc));
            return Promise.all(
              docs.map((doc) =>
                DocumentOutValidator.validate(
                  collectionName,
                  doc,
                  REST_API_METHODS.GET
                )
              )
            );
          })
          .then((validatedDocs: {}[]) => {
            let data: {
              doc: any;
              key: string;
              isArray: boolean;
              promise: Promise<any>;
            }[] = [];

            for (const doc of validatedDocs) {
              for (const key in attrsToPopulate) {
                if (Object.prototype.hasOwnProperty.call(doc, key)) {
                  const collectionName =
                    attrsToPopulate[key]["$collectionName"];
                  const query = attrsToPopulate[key]["$query"] || {};

                  if (Array.isArray(doc[key])) {
                    for (const objId of doc[key]) {
                      if (objId instanceof ObjectId) {
                        data.push({
                          doc: doc,
                          key: key,
                          isArray: true,
                          promise: DocumentHandler.find(
                            collectionName,
                            attrsToPopulate[key],
                            { _id: objId, ...query }
                          ),
                        });
                      }
                    }

                    doc[key] = (<[]>doc[key]).filter(
                      (el: any) => !(el instanceof ObjectId)
                    );
                  } else if (doc[key] instanceof ObjectId) {
                    data.push({
                      doc: doc,
                      key: key,
                      isArray: false,
                      promise: DocumentHandler.find(
                        collectionName,
                        attrsToPopulate[key],
                        { _id: doc[key], ...query }
                      ),
                    });

                    doc[key] = undefined;
                  }
                }
              }
            }

            return Promise.all(data.map((dataEl) => dataEl.promise)).then(
              (values) => {
                // values = values.flat();
                if (values.length === data.length) {
                  for (let index = 0; index < data.length; index++) {
                    const value = values[index];
                    const dataEl: {
                      doc: any;
                      key: string;
                      isArray: boolean;
                      promise: Promise<any>;
                    } = data[index];

                    if (dataEl.isArray && value[0]) {
                      dataEl.doc[dataEl.key].push(value[0]);
                    } else if (value[0]) {
                      dataEl.doc[dataEl.key] = value[0];
                    }
                  }
                }
                return Promise.resolve(validatedDocs);
              }
            );
          });
      });
  }

  public static save(
    collectionName: string,
    doc: {}
  ): Promise<{ document: {}; result: InsertOneResult }> {
    return new Promise((resolve, reject) => {
      try {
        if (doc["_id"]) return reject(`Can't save doc, it has _id.`);
        else {
          if (Array.isArray(doc)) return reject(`Can't save list.`);

          //   checking for attrs that are docs
          for (let key in doc) {
            if (key.includes("__")) {
              if (Array.isArray(doc[key])) {
                for (let _id of doc[key]) {
                  if (!_id)
                    return reject(
                      `Some attrs, of document from ${collectionName} collection, that are docs have invalid id.`
                    );
                  try {
                    _id = new ObjectId(_id);
                  } catch (error) {
                    return reject(
                      `Some attrs, of document from ${collectionName} collection, that are docs have invalid id.`
                    );
                  }
                }
              } else if (doc[key] && !(doc[key] instanceof ObjectId)) {
                try {
                  doc[key] = new ObjectId(doc[key]);
                } catch (error) {
                  return reject(
                    `Some attrs, of document from ${collectionName} collection, that are docs have invalid id.`
                  );
                }
              }
            }
          }

          // return DocumentHandler.validate(
          //   collectionName,
          //   doc,
          //   REST_API_METHODS.POST
          // )
          //   .then((validatedDoc) => {
          //     return DocumentHandler.db
          //       .collection(collectionName)
          //       .insertOne(validatedDoc)
          //       .then((result: InsertOneResult) => {
          //         resolve({
          //           document: validatedDoc,
          //           result: result,
          //         });
          //       })
          //       .catch((errorMsg) =>
          //         reject(
          //           `Document from ${collectionName} collection couldn't be created.`
          //         )
          //       );
          //   })
          //   .catch((errorMsg: string) => reject(errorMsg));
          return DocumentHandler.db
            .collection(collectionName)
            .insertOne(doc)
            .then((result: InsertOneResult) => {
              resolve({
                document: doc,
                result: result,
              });
            })
            .catch((errorMsg) =>
              reject(
                `Document from ${collectionName} collection couldn't be created.`
              )
            );
        }
      } catch (errorMsg) {
        reject(`Can't save document, error occurred.`);
      }
    });
  }

  public static update(
    collectionName: string,
    doc: {}
  ): Promise<{ document: {}; result: UpdateResult }> {
    return new Promise<{ document: {}; result: UpdateResult }>(
      (resolve, reject) => {
        try {
          if (doc["_id"]) {
            if (Array.isArray(doc)) reject(`Can't update list.`);

            //   checking for attrs that are docs
            for (let key in doc) {
              if (key.includes("__")) {
                if (Array.isArray(doc[key])) {
                  for (let _id of doc[key]) {
                    if (!_id)
                      return reject(
                        `Some attrs, of document from ${collectionName} collection, that are docs have invalid id.`
                      );
                    try {
                      _id = new ObjectId(_id);
                    } catch (error) {
                      return reject(
                        `Some attrs, of document from ${collectionName} collection, that are docs have invalid id.`
                      );
                    }
                  }
                } else if (doc[key] && !(doc[key] instanceof ObjectId)) {
                  try {
                    doc[key] = new ObjectId(doc[key]);
                  } catch (error) {
                    return reject(
                      `Some attrs, of document from ${collectionName} collection, that are docs have invalid id.`
                    );
                  }
                }
              }
            }

            return (
              DocumentHandler.db
                .collection(collectionName)
                // .replaceOne({ _id: (<ObjectId>entity.get_id()).toString() }, doc)
                .updateOne({ _id: doc["_id"] }, { $set: { ...doc } })
                .then((result: UpdateResult) => {
                  resolve({
                    document: doc,
                    result: result,
                  });
                })
                .catch((error) =>
                  reject(
                    `Document from ${collectionName} collection couldn't be updated.`
                  )
                )
            );
          } else reject(`Can't update document, it doesn't have _id.`);
        } catch (error) {
          //   console.log(error);
          reject(`Can't update document, error occurred.`);
        }
      }
    );
  }

  public static delete(
    collectionName: string,
    doc: {}
  ): Promise<{ document: {}; result: DeleteResult }> {
    return new Promise<{ document: {}; result: DeleteResult }>(
      (resolve, reject) => {
        try {
          if (doc["_id"]) {
            if (Array.isArray(doc)) reject(`Can't delete list.`);

            return DocumentHandler.db
              .collection(collectionName)
              .deleteOne({ _id: doc["_id"] })
              .then((result: DeleteResult) => {
                resolve({
                  document: doc,
                  result: result,
                });
              })
              .catch((error) =>
                reject(
                  `Document from ${collectionName} collection couldn't be deleted.`
                )
              );
          } else reject(`Can't delete document, it doesn't have _id.`);
        } catch (error) {
          //   console.log(error);
          reject(`Can't delete document, error occurred.`);
        }
      }
    );
  }
}
