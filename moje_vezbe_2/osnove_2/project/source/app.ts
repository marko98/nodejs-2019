import express from "express";
import path from "path";
import fs from "fs";
import { Db } from "mongodb";

import { createServer } from "https";

// util
import { getDB, mongoConnect } from "./util/database";

// middlewares
import { isAuth } from "./middlewares/is-auth.middleware";

// routes
import { getRouter } from "./routes/crud-routes";
import * as authRoutes from "./routes/auth-routes";
import * as userRoutes from "./routes/user-routes";
import { DocumentHandler } from "./model/classes/document-handler.class";

const app = express();

app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());

app.use(
  "/auth",
  (req: express.Request, res: express.Response, next: Function) => {
    req["__collectionName"] = DocumentHandler.collectionNames.USER_COLLECTION;
    next();
  },
  authRoutes.router
);

app.use(
  "/users",
  isAuth,
  (req: express.Request, res: express.Response, next: Function) => {
    req["__collectionName"] = DocumentHandler.collectionNames.USER_COLLECTION;
    next();
  },
  userRoutes.router,
  getRouter(true, false)
);
app.use(
  "/cities",
  isAuth,
  (req: express.Request, res: express.Response, next: Function) => {
    req["__collectionName"] = DocumentHandler.collectionNames.CITY_COLLECTION;
    next();
  },
  getRouter()
);
app.use(
  "/countries",
  isAuth,
  (req: express.Request, res: express.Response, next: Function) => {
    req["__collectionName"] =
      DocumentHandler.collectionNames.COUNTRY_COLLECTION;
    next();
  },
  getRouter()
);

app.use((error: any, req: any, res: any, next: any) => {
  console.log(error);

  const status = error.statusCode || 500;
  const data = error.data;
  // error.message -> string koji je prosledjen konstruktoru
  const message = error.message;
  return res.status(status).json({
    msg: message,
    data: data,
  });
});

mongoConnect(() => {
  getDB().then((db: Db) => {
    DocumentHandler.db = db;

    createServer(
      {
        cert: fs.readFileSync(path.join(__dirname, "../cert", "cert.pem")),
        key: fs.readFileSync(path.join(__dirname, "../cert", "key.pem")),
      },
      app
    ).listen({ port: 3443, host: "0.0.0.0" });
  });
});
