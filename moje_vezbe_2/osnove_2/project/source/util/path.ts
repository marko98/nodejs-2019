import path from "path";

const rootDirname = path.dirname(process.mainModule.filename);
export { rootDirname };
