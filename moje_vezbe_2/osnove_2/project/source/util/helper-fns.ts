import path from "path";
import { rootDirname } from "../util/path";
import fs from "fs";

const createFolders = (
  folderRecipes: { [key: string]: {} } | string,
  rootPath: string = rootDirname
): Promise<void> => {
  return new Promise((resolve, reject) => {
    if (typeof folderRecipes === "string") {
      try {
        fs.mkdirSync(folderRecipes, { recursive: true });
        return resolve();
      } catch (err) {
        const error = <any>(
          new Error(`Creation of the folder with path ${folderRecipes} failed.`)
        );
        error.statusCode = 500;
        return reject(error);
      }
    } else {
      let promises: Promise<void>[] = [];

      for (let folderName in folderRecipes) {
        let folderPath: string = path.join(rootPath, folderName);

        try {
          if (!fs.existsSync(folderPath)) fs.mkdirSync(folderPath);

          let _folderRecipes = folderRecipes[folderName];
          if (_folderRecipes)
            promises.push(createFolders(_folderRecipes, folderPath));
        } catch (err) {
          const error = <any>(
            new Error(`Creation of the folder with path ${folderPath} failed.`)
          );
          error.statusCode = 500;
          return reject(error);
        }
      }

      return Promise.all(promises).then(() => {
        return resolve();
      });
    }
  });
};

export { createFolders };
