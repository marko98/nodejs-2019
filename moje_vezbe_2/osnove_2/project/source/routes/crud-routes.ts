import express, { Router } from "express";
import { ObjectId, InsertOneResult, UpdateResult, DeleteResult } from "mongodb";

// class
import {
  DocumentHandler,
  REST_API_METHODS,
} from "../model/classes/document-handler.class";

// util
import { DocumentOutValidator } from "../model/classes/document-out-validator.class";
import { DocumentInValidator } from "../model/classes/document-in-validator.class";

const _getHandler = (req: express.Request, res: express.Response, next) => {
  let attrsToPopulate: {} = req.body.attrsToPopulate || {};
  let query: {} = req.body.query || {};

  let validatedDocs: {}[] = [];
  return DocumentHandler.find(req["__collectionName"], attrsToPopulate, query)
    .then((_validatedDocs: {}[]) => {
      validatedDocs = _validatedDocs;

      return Promise.all(
        validatedDocs.map((validatedDoc) =>
          DocumentOutValidator.validate(
            req["__collectionName"],
            validatedDoc,
            REST_API_METHODS.GET
          )
        )
      );
    })
    .then((_validatedDocs: {}[]) => {
      validatedDocs = _validatedDocs;
      return res.status(200).json(validatedDocs);
    })
    .catch((error: Error) => {
      return next(error);
    });
};

const _postHandler = (req: express.Request, res: express.Response, next) => {
  let data: { document: {}; result: InsertOneResult };
  return DocumentHandler.getDocRecipe(req["__collectionName"])
    .then((docRecipe) => {
      return DocumentInValidator.validate(
        req["__collectionName"],
        req.body,
        REST_API_METHODS.POST,
        docRecipe
      )
        .then((validatedInDoc) => {
          return DocumentHandler.save(req["__collectionName"], validatedInDoc);
        })
        .then((_data: { document: {}; result: InsertOneResult }) => {
          data = _data;

          return DocumentHandler.find(
            req["__collectionName"],
            {},
            { _id: (<any>data.document)._id }
          );
        })
        .then((docs: {}[]) => {
          return DocumentOutValidator.validate(
            req["__collectionName"],
            docs[0],
            REST_API_METHODS.GET
          );
        })
        .then((validatedOutDoc: {}) => {
          data.document = validatedOutDoc;
          return res.status(201).json(data);
        });
    })
    .catch((error: Error) => {
      return next(error);
    });
};

const _putHandler = (req: express.Request, res: express.Response, next) => {
  try {
    let doc: { _id: string | ObjectId } = req.body;
    if (!doc._id) {
      const error = <any>new Error(`Document is invalid.`);
      error.statusCode = 400;
      throw error;
    }

    doc._id = new ObjectId(doc._id);

    let data: { document: {}; result: UpdateResult };
    return DocumentHandler.getDocRecipe(req["__collectionName"])
      .then((docRecipe) => {
        return DocumentInValidator.validate(
          req["__collectionName"],
          doc,
          REST_API_METHODS.PUT,
          docRecipe
        );
      })
      .then((validatedInDoc: {}) => {
        (<any>validatedInDoc)._id = doc._id;
        return DocumentHandler.update(req["__collectionName"], validatedInDoc);
      })
      .then((_data: { document: {}; result: UpdateResult }) => {
        data = _data;
        if (data.result.matchedCount === 0) {
          const error = <any>new Error(`No document found to update.`);
          error.statusCode = 404;
          throw error;
        }

        return DocumentHandler.find(
          req["__collectionName"],
          {},
          { _id: (<any>data.document)._id }
        );
      })
      .then((docs: {}[]) => {
        return DocumentOutValidator.validate(
          req["__collectionName"],
          docs[0],
          REST_API_METHODS.GET
        );
      })
      .then((validatedOutDoc: {}) => {
        data.document = validatedOutDoc;
        return res.status(200).json(data);
      })
      .catch((error: Error) => {
        return next(error);
      });
  } catch (error) {
    return next(error);
  }
};

const _deleteHandler = (req: express.Request, res: express.Response, next) => {
  try {
    let doc: { _id: string | ObjectId } = req.body;
    if (!doc._id) {
      const error = <any>new Error(`Document is invalid.`);
      error.statusCode = 400;
      throw error;
    }

    doc._id = new ObjectId(doc._id);

    return DocumentHandler.delete(req["__collectionName"], doc)
      .then((data: { document: {}; result: DeleteResult }) => {
        if (data.result.deletedCount === 0) {
          const error = <any>new Error(`No document found to delete.`);
          error.statusCode = 404;
          throw error;
        }

        return res.status(200).json(data);
      })
      .catch((error: Error) => {
        return next(error);
      });
  } catch (error) {
    return next(error);
  }
};

const _deleteIdHandler = (
  req: express.Request,
  res: express.Response,
  next
) => {
  try {
    if (!req.params["id"]) {
      const error = <any>new Error(`Document is invalid.`);
      error.statusCode = 400;
      throw error;
    }

    let id = new ObjectId(req.params["id"]);

    return DocumentHandler.delete(req["__collectionName"], { _id: id })
      .then((data: { document: {}; result: DeleteResult }) => {
        if (data.result.deletedCount === 0) {
          const error = <any>new Error(`No document found to delete.`);
          error.statusCode = 404;
          throw error;
        }

        return res.status(200).json(data);
      })
      .catch((error: Error) => {
        return next(error);
      });
  } catch (error) {
    return next(error);
  }
};

const getRouter = (
  getHandler: boolean = true,
  postHandler: boolean = true,
  putHandler: boolean = true,
  deleteHandler: boolean = true,
  deleteIdHandler: boolean = true
): Router => {
  const router = express.Router();
  if (getHandler) router.get("", _getHandler);
  if (postHandler) router.post("", _postHandler);
  if (putHandler) router.put("", _putHandler);
  if (deleteHandler) router.delete("", _deleteHandler);
  if (deleteIdHandler) router.delete("/:id", _deleteIdHandler);

  return router;
};

export { getRouter };
