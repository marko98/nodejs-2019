import express from "express";
import bcryptjs from "bcryptjs";
import { ObjectId, UpdateResult } from "mongodb";
import multer from "multer";
import { v4 as uuidv4 } from "uuid";
import path from "path";
import fs from "fs";

// classes
import {
  DocumentHandler,
  REST_API_METHODS,
} from "../model/classes/document-handler.class";
import { DocumentInValidator } from "../model/classes/document-in-validator.class";
import { DocumentOutValidator } from "../model/classes/document-out-validator.class";

// util
import { rootDirname } from "../util/path";
import { createFolders } from "../util/helper-fns";

const router = express.Router();

router.put(
  "/change-password",
  (req: express.Request, res: express.Response, next) => {
    // 1) proveri da li sadrzi staru sifru i novu
    // 2) proveri staru - podudaranje
    // 3) verifikuj novu
    // 4) izmeni
    try {
      let doc: {
        _id: string | ObjectId;
        oldPassword: string;
        newPassword: string;
      } = req.body;
      if (!doc._id) {
        const error = <any>new Error(`Document is invalid.`);
        error.statusCode = 400;
        throw error;
      }

      doc._id = new ObjectId(doc._id);

      if (!doc.oldPassword) {
        const error = <any>(
          new Error(`Document is invalid, old password required.`)
        );
        error.statusCode = 400;
        throw error;
      }
      if (!doc.newPassword) {
        const error = <any>(
          new Error(`Document is invalid, new password required.`)
        );
        error.statusCode = 400;
        throw error;
      }

      if (doc.oldPassword === doc.newPassword) {
        const error = <any>(
          new Error(`Document is invalid, new password and old are the same.`)
        );
        error.statusCode = 400;
        throw error;
      }

      let theDoc: { _id: ObjectId; password: string };
      let data: { document: {}; result: UpdateResult };
      return DocumentHandler.db
        .collection(req["__collectionName"])
        .findOne({ _id: doc._id })
        .then((_theDoc: any) => {
          theDoc = _theDoc;

          if (!theDoc) {
            const error = <any>new Error(`No document found to update.`);
            error.statusCode = 404;
            throw error;
          }

          return bcryptjs.compare(doc.oldPassword, theDoc.password);
        })
        .then((isEqual: boolean) => {
          if (!isEqual) {
            const error = <any>new Error(`Wrong old password.`);
            error.statusCode = 401;
            throw error;
          }

          return DocumentHandler.getDocRecipe(req["__collectionName"]);
        })
        .then((docRecipe) => {
          (<string[]>(
            docRecipe.validationIn.PUT.allowedFieldsToChangeValue
          )).push("password");

          return DocumentInValidator.validate<{ password: string }>(
            req["__collectionName"],
            {
              _id: theDoc._id,
              password: doc.newPassword,
            },
            REST_API_METHODS.PUT,
            docRecipe
          );
        })
        .then((validatedInDoc: { password: string }) => {
          theDoc.password = validatedInDoc.password;
          return bcryptjs.hash(validatedInDoc.password, 12);
        })
        .then((hashedPassword: string) => {
          theDoc.password = hashedPassword;
          return DocumentHandler.update(req["__collectionName"], theDoc);
        })
        .then((_data: { document: {}; result: UpdateResult }) => {
          data = _data;
          return DocumentHandler.find(
            req["__collectionName"],
            {},
            { _id: (<any>data.document)._id }
          );
        })
        .then((docs: {}[]) => {
          return DocumentOutValidator.validate(
            req["__collectionName"],
            docs[0],
            REST_API_METHODS.GET
          );
        })
        .then((validatedOutDoc: {}) => {
          data.document = validatedOutDoc;
          return res.status(200).json(data);
        })
        .catch((error: Error) => next(error));
    } catch (error) {
      return next(error);
    }
  }
);

// https://www.npmjs.com/package/multer
router.post(
  "/change-icon",
  multer({
    storage: multer.diskStorage({
      destination: function (req: express.Request, file, cb) {
        // folder images mora vec da postoji inace nece raditi
        // cb(null, "images");

        // 1) pronadji user-a
        // 2) kreiraj foldere gde ce se taj fajl kreirati
        // 3) dodaj na ime fajle uuidv4
        // 4) azuriraj user-a
        // 5) obrisi prethodni fajl sa diska
        // 6) kreiraj novi fajl na disku

        let userId: ObjectId = req["__userId"];
        let theUser: any;
        let data: { document: {}; result: UpdateResult };
        let destination: string;
        return DocumentHandler.find(
          req["__collectionName"],
          {},
          { _id: userId }
        )
          .then((docs: {}[]) => {
            theUser = <any>docs[0];
            if (!theUser) {
              const error = <any>new Error(`User not found.`);
              error.statusCode = 401;
              throw error;
            }

            // create folders where to put file
            destination = path.join(
              rootDirname,
              "public",
              "page-" + userId.toString()
            );
            return createFolders(destination);
          })
          .then(() => {
            file.originalname = uuidv4() + "_" + file.originalname;

            return DocumentHandler.update(req["__collectionName"], {
              ...theUser,
              iconPath: path.join(
                "page-" + userId.toString(),
                file.originalname
              ),
            });
          })
          .then((_data: { document: {}; result: UpdateResult }) => {
            data = _data;
            if (data.result.matchedCount === 0) {
              const error = <any>new Error(`No document found to update.`);
              error.statusCode = 404;
              throw error;
            }

            // delete previous file
            if (theUser.iconPath)
              try {
                fs.unlinkSync(
                  path.join(rootDirname, "public", theUser.iconPath)
                );
              } catch (error) {}

            return cb(null, destination);
          })
          .catch((error) => cb(error, null));
      },
      filename: function (req, file, cb) {
        cb(null, file.originalname);
      },
    }),
    fileFilter: (req, file, cb) => {
      if (file.mimetype === "image/jpeg") cb(null, true);
      else cb(null, false);
    },
  }).single("icon"),
  (req: express.Request, res: express.Response, next: Function) => {
    let userId: ObjectId = req["__userId"];

    return DocumentHandler.find(req["__collectionName"], {}, { _id: userId })
      .then((docs: {}[]) => {
        return DocumentOutValidator.validate(
          req["__collectionName"],
          docs[0],
          REST_API_METHODS.GET
        );
      })
      .then((validatedOutDoc: {}) => {
        return res.status(200).json(validatedOutDoc);
      })
      .catch((error: Error) => {
        return next(error);
      });
  }
);

export { router };
