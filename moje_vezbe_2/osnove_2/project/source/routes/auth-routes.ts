import express from "express";
import bcryptjs from "bcryptjs";
import validator from "validator";
import { InsertOneResult } from "mongodb";
import { sign } from "jsonwebtoken";

// class
import {
  DocumentHandler,
  REST_API_METHODS,
} from "../model/classes/document-handler.class";

// util
import { DocumentInValidator } from "../model/classes/document-in-validator.class";
import { DocumentOutValidator } from "../model/classes/document-out-validator.class";
import { AUTH_TOKEN_SECRET } from "../consts";

const router = express.Router();

router.post("/sign-up", (req: express.Request, res: express.Response, next) => {
  try {
    let validatedDoc = JSON.parse(req.body.data);

    if (!validator.isLength(validatedDoc.password, { min: 2, max: 20 })) {
      const error = <any>new Error(`User's password length is invalid.`);
      error.statusCode = 400;
      throw error;
    }

    let data: { document: {}; result: InsertOneResult };
    return DocumentHandler.getDocRecipe(req["__collectionName"])
      .then((docRecipe) => {
        return DocumentInValidator.validate(
          req["__collectionName"],
          req.body,
          REST_API_METHODS.POST,
          docRecipe
        );
      })
      .then((validatedInDoc) => {
        validatedDoc = validatedInDoc;
        return bcryptjs.hash(validatedDoc.password, 12);
      })
      .then((hashedPassword) => {
        validatedDoc.password = hashedPassword;
        return DocumentHandler.save(req["__collectionName"], validatedDoc);
      })
      .then((_data: { document: {}; result: InsertOneResult }) => {
        data = _data;
        return DocumentOutValidator.validate(
          req["__collectionName"],
          data.document,
          REST_API_METHODS.GET
        );
      })
      .then((validatedOutDoc: {}) => {
        data.document = validatedOutDoc;
        return res.status(201).json(data);
      })
      .catch((error: Error) => {
        return next(error);
      });
  } catch (error) {
    return next(error);
  }
});

router.post("/login", (req: express.Request, res: express.Response, next) => {
  // 1) find user with given email
  // 2) compare password
  // 3) return jwt

  try {
    let data: { email: string; password: string } = req.body;
    if (!data.email) {
      const error = <any>new Error(`User's email is required.`);
      error.statusCode = 400;
      throw error;
    }
    if (!data.password) {
      const error = <any>new Error(`User's password is required.`);
      error.statusCode = 400;
      throw error;
    }

    let doc: any;
    return DocumentHandler.db
      .collection(req["__collectionName"])
      .findOne({ email: data.email })
      .then((_doc: any) => {
        doc = _doc;
        if (!doc) {
          const error = <any>new Error(`Email or password is incorrect.`);
          error.statusCode = 401;
          throw error;
        }

        return bcryptjs.compare(data.password, doc.password);
      })
      .then((isEqual: boolean) => {
        if (!isEqual) {
          const error = <any>new Error(`Email or password is incorrect.`);
          error.statusCode = 401;
          throw error;
        }

        // https://github.com/vercel/ms
        let expiresIn: string = "1d"; // "1y", "1d", "5m", "10s"
        return sign(
          { email: data.email, userId: doc._id.toString() },
          AUTH_TOKEN_SECRET,
          { expiresIn: expiresIn }
        );
      })
      .then((token: string) => {
        return res.status(200).json({ token: token });
      })
      .catch((error: Error) => next(error));
  } catch (error) {
    return next(error);
  }
});

export { router };
