export class DocumentHandler {
    // attrs
    public static db: Db;
    public static collectionNames: {} = {};
    private static docRecipes: {} = {};
    
    // getters & setters
    public static getDb (): Db { return DocumentHandler.db; }
    public static setDb (db: Db): void { DocumentHandler.db = db; }
    
    public static getCollectionNames (): {} { return DocumentHandler.collectionNames; }
    public static setCollectionNames (collectionNames: {}): void { DocumentHandler.collectionNames = collectionNames; }
    
    public static getDocRecipes (): {} { return DocumentHandler.docRecipes; }
    public static setDocRecipes (docRecipes: {}): void { DocumentHandler.docRecipes = docRecipes; }
    
    // fns
    private static _validate(collectionName: string, doc: {}, idRequired: boolean): Promise<{}> {}
    public static find(collectionName: string, attrsToPopulate: {}, query: {}): Promise<{}[]> {}
    public static save(collectionName: string, doc: {}): Promise<{ document: {}; result: InsertOneResult }> {}
    public static update(collectionName: string, doc: {}): Promise<{ document: {}; result: UpdateResult }> {}
    public static delete(collectionName: string, doc: {}): Promise<{ document: {}; result: DeleteResult }> {}
    
}