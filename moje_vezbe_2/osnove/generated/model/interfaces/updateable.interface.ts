import { Entitable } from './entitable.interface';
import { FactoryMethodable } from './factory-methodable.interface';
import { Validateable } from './validateable.interface';
import { Idable } from './idable.interface';

export interface Updateable {
    // attrs
    
    // fns
    update<T>(entity: Entitable): Promise<T>;
    
}