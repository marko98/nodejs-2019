import { FactoryMethodable } from './factory-methodable.interface';
import { Validateable } from './validateable.interface';
import { Idable } from './idable.interface';
import { Updateable } from './updateable.interface';

export interface Entitable extends FactoryMethodable, Validateable, Idable, Updateable {
    // attrs
    
    // fns
    
}