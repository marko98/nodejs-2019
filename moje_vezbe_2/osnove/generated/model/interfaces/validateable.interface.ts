export interface Validateable {
    // attrs
    
    // fns
    validate<T>(): Promise<T>;
    
}