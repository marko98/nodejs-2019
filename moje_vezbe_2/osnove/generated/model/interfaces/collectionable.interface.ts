export interface Collectionable {
    // attrs
    
    // fns
    getCollectionName(): string;
    
}