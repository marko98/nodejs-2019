export interface FactoryMethodable {
    // attrs
    
    // fns
    create(): any;
    
}