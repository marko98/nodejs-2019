import { City } from './city.class';
import { Entitable } from '../interfaces/entitable.interface';
import { FactoryMethodable } from '../interfaces/factory-methodable.interface';
import { Validateable } from '../interfaces/validateable.interface';
import { Idable } from '../interfaces/idable.interface';
import { Updateable } from '../interfaces/updateable.interface';

export class User implements Entitable {
    // attrs
    private _name: string = undefined;
    private _surname: string = undefined;
    private static _collectionName: string = "user";
    private static __livesInCollectionName: string = "city";
    private __livesIn: City = undefined;
    
    // getters & setters
    public get_name (): string { return this._name; }
    public set_name (_name: string): void { this._name = _name; }
    
    public get_surname (): string { return this._surname; }
    public set_surname (_surname: string): void { this._surname = _surname; }
    
    public static get_collectionName (): string { return User._collectionName; }
    public static set_collectionName (_collectionName: string): void { User._collectionName = _collectionName; }
    
    public static get__livesInCollectionName (): string { return User.__livesInCollectionName; }
    public static set__livesInCollectionName (__livesInCollectionName: string): void { User.__livesInCollectionName = __livesInCollectionName; }
    
    public get__livesIn (): City { return this.__livesIn; }
    public set__livesIn (__livesIn: City): void { this.__livesIn = __livesIn; }
    
    // fns
    public introduceYourself(): void {}
    
    // interface Entitable attrs & fns
    
    // interface FactoryMethodable attrs & fns
    public create(): any { throw new Error("Not implemented error!"); }
    
    // interface Validateable attrs & fns
    public validate<T>(): Promise<T> { throw new Error("Not implemented error!"); }
    
    // interface Idable attrs & fns
    public get_id(): any { throw new Error("Not implemented error!"); }
    public set_id(id: any): void { throw new Error("Not implemented error!"); }
    
    // interface Updateable attrs & fns
    public update<T>(entity: Entitable): Promise<T> { throw new Error("Not implemented error!"); }
    
}