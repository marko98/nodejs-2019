import { Entitable } from '../interfaces/entitable.interface';
import { FactoryMethodable } from '../interfaces/factory-methodable.interface';
import { Validateable } from '../interfaces/validateable.interface';
import { Idable } from '../interfaces/idable.interface';
import { Updateable } from '../interfaces/updateable.interface';

export class EntityHandler {
    // attrs
    private static db: Db;
    private static registry: Entitable[] = [];
    
    // getters & setters
    public static getDb (): Db { return EntityHandler.db; }
    public static setDb (db: Db): void { EntityHandler.db = db; }
    
    public static getRegistry (): Entitable[] { return EntityHandler.registry; }
    public static setRegistry (registry: Entitable[]): void { EntityHandler.registry = registry; }
    
    // fns
    public static toDocument(entity: Entitable): {} {}
    public static save(entity: Entitable): Promise<{document: {}, result: InsertOneResult}> {}
    public static update(entity: Entitable): Promise<{document: {}, result: UpdateResult}> {}
    public static delete(entity: Entitable): Promise<{document: {}, result: DeleteResult}> {}
    public static find<T extends Entitable>(collectionName: string, attrsToPopulate: {}, query: {}): Promise<T[]> {}
    public static registerEntity(entity: Entitable): boolean {}
    public static getEntityOfDocumentIfExistsInRegistry<T extends Entitable>(document: {}): T {}
    public static updateRegistry(entity: Entitable): boolean {}
    
}