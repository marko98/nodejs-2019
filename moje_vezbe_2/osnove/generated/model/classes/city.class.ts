import { Entitable } from '../interfaces/entitable.interface';
import { FactoryMethodable } from '../interfaces/factory-methodable.interface';
import { Validateable } from '../interfaces/validateable.interface';
import { Idable } from '../interfaces/idable.interface';
import { Updateable } from '../interfaces/updateable.interface';

export class City implements Entitable {
    // attrs
    private _name: string = undefined;
    private static _collectionName: string = "city";
    
    // getters & setters
    public get_name (): string { return this._name; }
    public set_name (_name: string): void { this._name = _name; }
    
    public static get_collectionName (): string { return City._collectionName; }
    public static set_collectionName (_collectionName: string): void { City._collectionName = _collectionName; }
    
    // fns
    
    // interface Entitable attrs & fns
    
    // interface FactoryMethodable attrs & fns
    public create(): any { throw new Error("Not implemented error!"); }
    
    // interface Validateable attrs & fns
    public validate<T>(): Promise<T> { throw new Error("Not implemented error!"); }
    
    // interface Idable attrs & fns
    public get_id(): any { throw new Error("Not implemented error!"); }
    public set_id(id: any): void { throw new Error("Not implemented error!"); }
    
    // interface Updateable attrs & fns
    public update<T>(entity: Entitable): Promise<T> { throw new Error("Not implemented error!"); }
    
}