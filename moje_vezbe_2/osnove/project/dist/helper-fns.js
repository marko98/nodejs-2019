"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCollectionNameFromConstructorName = exports.isObject = void 0;
var isObject = function (entry) {
    if (entry === null) {
        return false;
    }
    return typeof entry === "function" || typeof entry === "object";
};
exports.isObject = isObject;
var getCollectionNameFromConstructorName = function (constructorName) {
    var collectionName = constructorName
        .replace(/[A-Z][a-z]*/g, function (str) { return "_" + str.toLowerCase(); })
        .replace(/(^_)|(_$)/g, "");
    return collectionName;
};
exports.getCollectionNameFromConstructorName = getCollectionNameFromConstructorName;
