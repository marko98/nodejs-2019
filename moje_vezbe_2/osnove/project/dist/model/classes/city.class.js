"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.City = void 0;
var validator_1 = __importDefault(require("validator"));
var City = /** @class */ (function () {
    function City() {
        // attrs
        this._name = undefined;
        this.__country = undefined;
    }
    // getters & setters
    City.prototype.get_name = function () {
        return this._name;
    };
    City.prototype.set_name = function (_name) {
        this._name = _name;
    };
    City.get_collectionName = function () {
        return City._collectionName;
    };
    City.prototype.get__country = function () {
        return this.__country;
    };
    City.prototype.set__country = function (_country) {
        this.__country = _country;
    };
    // fns
    // interface Entitable attrs & fns
    // interface Idable attrs & fns
    City.prototype.get_id = function () {
        return this._id;
    };
    City.prototype.set_id = function (id) {
        if (this._id)
            throw new Error("Id exists already, can't change it.");
        else
            this._id = id;
    };
    // interface FactoryMethodable attrs & fns
    City.prototype.create = function () {
        return new City();
    };
    // interface Validateable attrs & fns
    City.prototype.validate = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                // name
                if (!_this._name)
                    reject("City must have a name.");
                _this._name = _this._name.trim();
                if (validator_1.default.isEmpty(_this._name))
                    reject("City's name mustn't be empty.");
                else if (!validator_1.default.isLength(_this._name, { min: 2, max: 20 }))
                    reject("City's name length is invalid.");
                else if (!validator_1.default.isAlpha(_this._name, undefined, { ignore: " " }))
                    reject("City's name must contains only letters.");
            }
            catch (error) {
                reject("City validation failed.");
            }
            resolve(_this);
        });
    };
    // interface Updateable attrs & fns
    City.prototype.update = function (entity) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                for (var _i = 0, _a = Object.keys(_this).filter(function (key) { return key !== "_id"; }); _i < _a.length; _i++) {
                    var key = _a[_i];
                    _this[key] = entity[key];
                }
                resolve(_this);
            }
            catch (error) {
                reject(error);
            }
        });
    };
    City._collectionName = "city";
    return City;
}());
exports.City = City;
