"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityHandler = void 0;
var mongodb_1 = require("mongodb");
var helper_fns_1 = require("../../helper-fns");
var document_handler_class_1 = require("./document-handler.class");
var EntityHandler = /** @class */ (function () {
    function EntityHandler() {
        // mongoConnect(() => {
        //   getDB().then((db) => {
        //     EntityHandler.db = db;
        // let user = new User();
        // user.set_name("Marko");
        // user.set_surname("Zahorodni");
        // EntityHandler.save(user)
        //   .then((result) => {
        //     console.log(result);
        //   })
        //   .catch((err) => console.log(err));
        // let users: User[];
        // EntityHandler.find<User>("user")
        //   .then((_users) => {
        //     users = _users;
        //     return EntityHandler.find<Country>("country");
        //   })
        //   .then((countries) => {
        //     console.log(users[0]);
        //     users[0].setLivesIn(undefined);
        //     console.log(users[0]);
        //     return EntityHandler.update(users[0]);
        //   })
        //   .then((result) => {
        //     console.log(result);
        //   })
        //   .catch((err) => console.log(err));
        // let country = new Country();
        // country.set_name("Serbia");
        // EntityHandler.save(country)
        //   .then((result) => {
        //     console.log(result);
        //   })
        //   .catch((err) => console.log(err));
        // EntityHandler.find<User>(
        //   "user",
        //   {
        //     _livesIn: {
        //       $collectionName: "city",
        //       _country: { $collectionName: "country" },
        //     },
        //   },
        //   {}
        // )
        //   .then((users) => {
        //     console.log(users);
        //   })
        //   .catch((err) => console.log(err));
        // EntityHandler.find<User>("user")
        //   .then((_users) => {
        //     console.log(_users);
        //     let user = _users.find(
        //       (u) => u.get_id().toString() === "616ee16304b2afc61a46de52"
        //     );
        //     if (user) return EntityHandler.delete(user);
        //   })
        //   .then((result) => {
        //     console.log(result);
        //   })
        //   .catch((err) => console.log(err));
        //   });
        // });
    }
    // getters & setters
    EntityHandler.getDb = function () {
        return EntityHandler.db;
    };
    EntityHandler.setDb = function (db) {
        EntityHandler.db = db;
    };
    // fns
    EntityHandler.updateRegistry = function (entity) {
        var updateHappened = false;
        EntityHandler.registry = EntityHandler.registry.map(function (_entity) {
            if (_entity.get_id().toString() === entity.get_id().toString()) {
                _entity.update(entity);
                updateHappened = true;
                return entity;
            }
            return _entity;
        });
        return updateHappened;
    };
    EntityHandler.registerEntity = function (entity) {
        if (!EntityHandler.registry.find(function (_entity) { return _entity.get_id().toString() === entity.get_id().toString(); })) {
            EntityHandler.registry.push(entity);
            return true;
        }
        return false;
    };
    EntityHandler.unregisterEntity = function (entity) {
        var index = EntityHandler.registry.findIndex(function (_entity) { return _entity.get_id().toString() === entity.get_id().toString(); });
        if (index !== -1) {
            delete EntityHandler.registry[index];
            return true;
        }
        return false;
    };
    EntityHandler.getEntityOfDocumentIfExistsInRegistry = function (document) {
        if (document._id)
            return (EntityHandler.registry.find(function (entity) { return document._id.toString() === entity.get_id().toString(); }));
        return undefined;
    };
    EntityHandler.toDocument = function (entity) {
        // let attrKeys: string[] = Object.keys(entity);
        // let attrValues: any[] = Object.values(entity);
        // let primitiveAttrEntries: [string, unknown][] = Object.entries(entity);
        var attrEntries = Object.entries(entity);
        var doc = {};
        for (var _i = 0, attrEntries_1 = attrEntries; _i < attrEntries_1.length; _i++) {
            var attrEntry = attrEntries_1[_i];
            if (attrEntry[0] === "_id") {
                doc[attrEntry[0]] = attrEntry[1];
                continue;
            }
            if (Array.isArray(attrEntry[1])) {
                doc[attrEntry[0]] = [];
                for (var _a = 0, _b = attrEntry[1]; _a < _b.length; _a++) {
                    var el = _b[_a];
                    if ((0, helper_fns_1.isObject)(el)) {
                        var id = el.get_id();
                        if (id)
                            doc[attrEntry[0]].push(id);
                    }
                    else {
                        doc[attrEntry[0]].push(el);
                    }
                }
                continue;
            }
            if ((0, helper_fns_1.isObject)(attrEntry[1])) {
                // let id = (<Entitable>attrEntry[1]).get_id();
                var id = attrEntry[1]._id;
                if (id)
                    doc[attrEntry[0]] = id;
                // else doc[attrEntry[0]] = null; // mongodb nece uneti vrednosti koje nemaju vrednost
            }
            else {
                doc[attrEntry[0]] = attrEntry[1];
            }
        }
        return doc;
    };
    EntityHandler.save = function (entity) {
        return new Promise(function (resolve, reject) {
            try {
                if (entity.get_id())
                    reject("Can't save entity, it has _id.");
                else {
                    if (Array.isArray(entity))
                        reject("Can't save list.");
                    var collectionName = (0, helper_fns_1.getCollectionNameFromConstructorName)(entity.constructor.name);
                    var doc_1 = EntityHandler.toDocument(entity);
                    EntityHandler.db
                        .collection(collectionName)
                        .insertOne(doc_1)
                        .then(function (result) {
                        entity.set_id(doc_1["_id"]);
                        EntityHandler.registerEntity(entity);
                        resolve({
                            document: doc_1,
                            result: result,
                        });
                    })
                        .catch(function (err) {
                        return reject(entity.constructor.name + " couldn't be created.");
                    });
                }
            }
            catch (error) {
                reject("Can't save, error occurred.");
            }
        });
    };
    EntityHandler.update = function (entity) {
        return new Promise(function (resolve, reject) {
            try {
                if (entity.get_id()) {
                    if (Array.isArray(entity))
                        reject("Can't update list.");
                    var collectionName = (0, helper_fns_1.getCollectionNameFromConstructorName)(entity.constructor.name);
                    var doc_2 = EntityHandler.toDocument(entity);
                    EntityHandler.db
                        .collection(collectionName)
                        // .replaceOne({ _id: (<ObjectId>entity.get_id()).toString() }, doc)
                        .updateOne({ _id: entity.get_id() }, { $set: __assign({}, doc_2) })
                        .then(function (result) {
                        if (result.matchedCount === 1 && result.modifiedCount === 1)
                            EntityHandler.updateRegistry(entity);
                        resolve({
                            document: doc_2,
                            result: result,
                        });
                    })
                        .catch(function (error) {
                        return reject(entity.constructor.name + " couldn't be updated. " + error);
                    }
                    // reject(`${entity.constructor.name} couldn't be updated.`)
                    );
                }
                else
                    reject("Can't update entity, it doesn't have _id.");
            }
            catch (error) {
                console.log(error);
                reject("Can't update, error occurred.");
            }
        });
    };
    EntityHandler.delete = function (entity) {
        return new Promise(function (resolve, reject) {
            try {
                if (entity.get_id()) {
                    if (Array.isArray(entity))
                        reject("Can't delete list.");
                    var collectionName = (0, helper_fns_1.getCollectionNameFromConstructorName)(entity.constructor.name);
                    var doc_3 = EntityHandler.toDocument(entity);
                    EntityHandler.db
                        .collection(collectionName)
                        .deleteOne({ _id: entity.get_id() })
                        .then(function (result) {
                        if (result.deletedCount === 1)
                            entity = undefined;
                        resolve({
                            document: doc_3,
                            result: result,
                        });
                    })
                        .catch(function (err) {
                        return reject(entity.constructor.name + " couldn't be deleted.");
                    });
                }
                else
                    reject("Can't delete entity, it doesn't have _id.");
            }
            catch (error) {
                console.log(error);
                reject("Can't delete, error occurred.");
            }
        });
    };
    EntityHandler.find = function (collectionName, attrsToPopulate, query) {
        if (attrsToPopulate === void 0) { attrsToPopulate = {}; }
        if (query === void 0) { query = {}; }
        return EntityHandler.db
            .listCollections({ name: collectionName })
            .toArray()
            .then(function (collections) {
            if (collections.length === 0)
                return Promise.reject("Collection " + collectionName + " doesn't exists.");
            // collection exists
            return EntityHandler.db
                .collection(collectionName)
                .find(query)
                .toArray()
                .then(function (docs) {
                var promises = docs.map(function (doc) {
                    return document_handler_class_1.DocumentHandler.toEntity(collectionName, doc);
                });
                return Promise.all(promises);
            })
                .then(function (entities) {
                var data = [];
                // NE ZNAM BAS KOLIKO JE DOBRA IDEJA
                // also register fetched entites, or update registry if they exist already
                // ili ipak staviti da se entitet registruje ako vec nije registrovan
                for (var _i = 0, entities_1 = entities; _i < entities_1.length; _i++) {
                    var entity = entities_1[_i];
                    EntityHandler.registerEntity(entity);
                    // if (!EntityHandler.registerEntity(entity))
                    //   EntityHandler.updateRegistry(entity);
                    for (var key in attrsToPopulate) {
                        if (Object.prototype.hasOwnProperty.call(entity, key)) {
                            var collectionName_1 = attrsToPopulate[key]["$collectionName"];
                            var query_1 = attrsToPopulate[key]["$query"] || {};
                            if (Array.isArray(entity[key])) {
                                for (var _a = 0, _b = entity[key]; _a < _b.length; _a++) {
                                    var objId = _b[_a];
                                    if (objId instanceof mongodb_1.ObjectId) {
                                        data.push({
                                            entity: entity,
                                            key: key,
                                            isArray: true,
                                            promise: EntityHandler.find(collectionName_1, attrsToPopulate[key], __assign({ _id: objId }, query_1)),
                                        });
                                    }
                                }
                                entity[key] = entity[key].filter(function (el) { return !(el instanceof mongodb_1.ObjectId); });
                            }
                            else if (entity[key] instanceof mongodb_1.ObjectId) {
                                data.push({
                                    entity: entity,
                                    key: key,
                                    isArray: false,
                                    promise: EntityHandler.find(collectionName_1, attrsToPopulate[key], __assign({ _id: entity[key] }, query_1)),
                                });
                            }
                        }
                    }
                }
                return Promise.all(data.map(function (dataEl) { return dataEl.promise; })).then(function (values) {
                    values = values.flat();
                    // console.log(values);
                    if (values.length === data.length) {
                        for (var index = 0; index < data.length; index++) {
                            var value = values[index];
                            var dataEl = data[index];
                            if (dataEl.isArray) {
                                dataEl.entity[dataEl.key].push(value);
                            }
                            else {
                                dataEl.entity[dataEl.key] = value;
                            }
                        }
                    }
                    return Promise.resolve(entities);
                });
            });
        });
    };
    EntityHandler.registry = [];
    return EntityHandler;
}());
exports.EntityHandler = EntityHandler;
// new EntityHandler();
