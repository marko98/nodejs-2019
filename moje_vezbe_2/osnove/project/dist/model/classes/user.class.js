"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
var validator_1 = __importDefault(require("validator"));
var User = /** @class */ (function () {
    function User() {
        // attrs
        this._name = undefined;
        this._surname = undefined;
        this.__livesIn = undefined;
    }
    // getters & setters
    User.prototype.get_name = function () {
        return this._name;
    };
    User.prototype.set_name = function (_name) {
        this._name = _name;
    };
    User.prototype.get_surname = function () {
        return this._surname;
    };
    User.prototype.set_surname = function (_surname) {
        this._surname = _surname;
    };
    User.get_collectionName = function () {
        return User._collectionName;
    };
    User.get__livesInCollectionName = function () {
        return User.__livesInCollectionName;
    };
    User.prototype.get__livesIn = function () {
        return this.__livesIn;
    };
    User.prototype.set__livesIn = function (livesIn) {
        this.__livesIn = livesIn;
    };
    // fns
    User.prototype.introduceYourself = function () { };
    // interface Entitable attrs & fns
    // interface FactoryMethodable attrs & fns
    User.prototype.create = function () {
        return new User();
    };
    // interface Idable attrs & fns
    User.prototype.get_id = function () {
        return this._id;
    };
    User.prototype.set_id = function (id) {
        if (this._id)
            throw new Error("Id exists already, can't change it.");
        else
            this._id = id;
    };
    // interface Validateable attrs & fns
    User.prototype.validate = function () {
        // throw new Error("Not implemented error!");
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                // _name
                if (!_this._name)
                    reject("User must have a name.");
                _this._name = _this._name.trim();
                if (validator_1.default.isEmpty(_this._name))
                    reject("User's name mustn't be empty.");
                else if (!validator_1.default.isLength(_this._name, { min: 2, max: 20 }))
                    reject("User's name length is invalid.");
                else if (!validator_1.default.isAlpha(_this._name, undefined, { ignore: "-" }))
                    reject("User's name must contains only letters.");
                // _surname
                if (!_this._surname)
                    reject("User must have a surname.");
                _this._surname = _this._surname.trim();
                if (validator_1.default.isEmpty(_this._surname))
                    reject("User's surname mustn't be empty.");
                else if (!validator_1.default.isLength(_this._surname, { min: 2, max: 20 }))
                    reject("User's surname length is invalid.");
                else if (!validator_1.default.isAlpha(_this._surname, undefined, { ignore: "-" }))
                    reject("User's surname must contains only letters.");
                // __livesIn
                if (Array.isArray(_this.__livesIn))
                    reject("User's __livesIn mustn't be an array.");
            }
            catch (error) {
                reject("User validation failed.");
            }
            resolve(_this);
        });
    };
    // interface Collectionable attrs & fns
    User.prototype.getCollectionName = function () {
        return "user";
    };
    // interface Updateable attrs & fns
    User.prototype.update = function (entity) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                for (var _i = 0, _a = Object.keys(_this).filter(function (key) { return key !== "_id"; }); _i < _a.length; _i++) {
                    var key = _a[_i];
                    _this[key] = entity[key];
                }
                resolve(_this);
            }
            catch (error) {
                reject(error);
            }
        });
    };
    User._collectionName = "user";
    User.__livesInCollectionName = "city";
    return User;
}());
exports.User = User;
