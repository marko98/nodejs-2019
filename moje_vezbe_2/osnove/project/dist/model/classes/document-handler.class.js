"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentHandler = void 0;
var user_class_1 = require("./user.class");
var country_class_1 = require("./country.class");
var city_class_1 = require("./city.class");
var mongodb_1 = require("mongodb");
var entity_handler_class_1 = require("./entity-handler.class");
var DocumentHandler = /** @class */ (function () {
    function DocumentHandler() {
    }
    // fns
    DocumentHandler.toEntity = function (collectionName, document, checkRegistry) {
        if (checkRegistry === void 0) { checkRegistry = true; }
        return new Promise(function (resolve, reject) {
            if (document._id) {
                if (!(document._id instanceof mongodb_1.ObjectId))
                    document._id = new mongodb_1.ObjectId(document._id);
                if (checkRegistry) {
                    var entity = entity_handler_class_1.EntityHandler.getEntityOfDocumentIfExistsInRegistry(document);
                    if (entity)
                        resolve(entity);
                }
            }
            var instance = new DocumentHandler.collectionNameEntityClassRegistry[collectionName]();
            var keys = Object.keys(document);
            var methodNames = keys.map(function (attrName) {
                return !(attrName[0] === "_")
                    ? attrName[0].toUpperCase() + attrName.substr(1)
                    : attrName;
            });
            try {
                var data_1 = [];
                for (var index = 0; index < methodNames.length; index++) {
                    var methodName = methodNames[index];
                    var value = document[keys[index]];
                    // vrednost atributa je entitet ili lista entiteta, mora/ju vec postojati
                    if (methodName.includes("__", 0)) {
                        if (Array.isArray(value)) {
                            var _ids = value;
                            for (var _i = 0, _ids_1 = _ids; _i < _ids_1.length; _i++) {
                                var _id = _ids_1[_i];
                                try {
                                    if (!(_id instanceof mongodb_1.ObjectId))
                                        _id = new mongodb_1.ObjectId(_id);
                                }
                                catch (error) {
                                    reject("Entity can't be created, some atribute document is invalid.");
                                }
                                // 1) pogledaj registar
                                var entity = entity_handler_class_1.EntityHandler.getEntityOfDocumentIfExistsInRegistry({
                                    _id: _id,
                                });
                                if (entity) {
                                    var array = instance["get" + methodName]();
                                    array.push(value);
                                    instance["set" + methodName](array);
                                    // instance["set" + methodName](entity);
                                }
                                else {
                                    // 2) dobavi iz baze
                                    var _collectionName = DocumentHandler.collectionNameEntityClassRegistry[collectionName]["get" + keys[index] + "CollectionName"]();
                                    data_1.push({
                                        entity: instance,
                                        methodName: methodName,
                                        isArray: true,
                                        promise: entity_handler_class_1.EntityHandler.find(_collectionName, {}, { _id: _id }),
                                    });
                                }
                            }
                        }
                        else {
                            var _id = value;
                            if (!_id) {
                                instance["set" + methodName](undefined);
                                continue;
                            }
                            try {
                                if (!(_id instanceof mongodb_1.ObjectId))
                                    _id = new mongodb_1.ObjectId(_id);
                            }
                            catch (error) {
                                reject("Entity can't be created, some atribute document is invalid.");
                            }
                            // 1) pogledaj registar
                            var entity = entity_handler_class_1.EntityHandler.getEntityOfDocumentIfExistsInRegistry({
                                _id: _id,
                            });
                            if (entity)
                                instance["set" + methodName](entity);
                            else {
                                // 2) dobavi iz baze
                                var _collectionName = DocumentHandler.collectionNameEntityClassRegistry[collectionName]["get" + keys[index] + "CollectionName"]();
                                data_1.push({
                                    entity: instance,
                                    methodName: methodName,
                                    isArray: false,
                                    promise: entity_handler_class_1.EntityHandler.find(_collectionName, {}, { _id: _id }),
                                });
                            }
                        }
                    }
                    else {
                        instance["set" + methodName](value);
                    }
                }
                return Promise.all(data_1.map(function (dataEl) { return dataEl.promise; }))
                    .then(function (values) {
                    values = values.flat();
                    if (values.length === data_1.length) {
                        for (var index = 0; index < data_1.length; index++) {
                            var value = values[index];
                            var dataEl = data_1[index];
                            if (dataEl.isArray) {
                                var array = dataEl.entity["get" + dataEl.methodName]();
                                array.push(value);
                                dataEl.entity["set" + dataEl.methodName](array);
                            }
                            else {
                                dataEl.entity["set" + dataEl.methodName](value);
                            }
                        }
                    }
                    return resolve(instance);
                })
                    .catch(function (errorMsg) {
                    reject(errorMsg);
                });
            }
            catch (error) {
                reject("Entity can't be created, document is invalid.");
            }
        }).then(function (instance) {
            return instance.validate();
        });
    };
    // attrs
    DocumentHandler.collectionNameEntityClassRegistry = {
        user: user_class_1.User,
        country: country_class_1.Country,
        city: city_class_1.City,
    };
    return DocumentHandler;
}());
exports.DocumentHandler = DocumentHandler;
