"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Country = void 0;
var Country = /** @class */ (function () {
    function Country() {
        // attrs
        this._name = undefined;
    }
    // getters & setters
    Country.prototype.get_name = function () {
        return this._name;
    };
    Country.prototype.set_name = function (_name) {
        this._name = _name;
    };
    Country.get_collectionName = function () {
        return Country._collectionName;
    };
    // fns
    // interface Entitable attrs & fns
    // interface Idable attrs & fns
    Country.prototype.get_id = function () {
        return this._id;
    };
    Country.prototype.set_id = function (id) {
        if (this._id)
            throw new Error("Id exists already, can't change it.");
        else
            this._id = id;
    };
    // interface FactoryMethodable attrs & fns
    Country.prototype.create = function () {
        return new Country();
    };
    // interface Validateable attrs & fns
    Country.prototype.validate = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            resolve(_this);
        });
    };
    // interface Updateable attrs & fns
    Country.prototype.update = function (entity) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                for (var _i = 0, _a = Object.keys(_this).filter(function (key) { return key !== "_id"; }); _i < _a.length; _i++) {
                    var key = _a[_i];
                    _this[key] = entity[key];
                }
                resolve(_this);
            }
            catch (error) {
                reject(error);
            }
        });
    };
    Country._collectionName = "country";
    return Country;
}());
exports.Country = Country;
