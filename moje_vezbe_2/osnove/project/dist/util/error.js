"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorFn = exports.errorMiddleware = void 0;
var errorFn = function (msg, next, statusCode) {
    if (statusCode === void 0) { statusCode = 500; }
    var error = new Error(msg);
    error.httpStatusCode = statusCode;
    return next(error);
};
exports.errorFn = errorFn;
var errorMiddleware = function (error, req, res, next) {
    return res.status(error.httpStatusCode).json({ msg: error.message });
};
exports.errorMiddleware = errorMiddleware;
