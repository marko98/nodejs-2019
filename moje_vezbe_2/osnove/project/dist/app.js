"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var https_1 = require("https");
// util
var error_1 = require("./util/error");
var database_1 = require("./util/database");
// routers
var userRoute = __importStar(require("./routes/user-route"));
var cityRoute = __importStar(require("./routes/city-route"));
// classes
var entity_handler_class_1 = require("./model/classes/entity-handler.class");
var app = (0, express_1.default)();
app.use(express_1.default.static(path_1.default.join(__dirname, "public")));
app.use(express_1.default.json());
app.get("/", function (req, res, next) {
    var db = (0, database_1.getDB)().then(function (db) {
        return res.send({ msg: "hello" });
    }, function (err) { return (0, error_1.errorFn)(err, next); });
});
app.use("/users", userRoute.router);
app.use("/cities", cityRoute.router);
app.use(error_1.errorMiddleware);
(0, database_1.mongoConnect)(function () {
    (0, database_1.getDB)().then(function (db) {
        entity_handler_class_1.EntityHandler.setDb(db);
        (0, https_1.createServer)({
            cert: fs_1.default.readFileSync(path_1.default.join(__dirname, "../cert", "cert.pem")),
            key: fs_1.default.readFileSync(path_1.default.join(__dirname, "../cert", "key.pem")),
        }, app).listen({ port: 3443, host: "0.0.0.0" });
    });
});
