"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = __importDefault(require("express"));
var mongodb_1 = require("mongodb");
// classes
var city_class_1 = require("../model/classes/city.class");
// util
var error_1 = require("../util/error");
var entity_handler_class_1 = require("../model/classes/entity-handler.class");
var document_handler_class_1 = require("../model/classes/document-handler.class");
var router = express_1.default.Router();
exports.router = router;
var CITY_COLLECTION = "city";
router.post("", function (req, res, next) {
    return document_handler_class_1.DocumentHandler.toEntity(CITY_COLLECTION, req.body)
        .then(function (city) {
        return entity_handler_class_1.EntityHandler.save(city);
    })
        .then(function (data) {
        return res.status(201).json(data);
    })
        .catch(function (errorMsg) {
        (0, error_1.errorFn)(errorMsg, next);
    });
});
router.get("", function (req, res, next) {
    // let attrsToPopulate: {} = {"_livesIn": {"$collectionName": "city", "_country": {"$collectionName": "country"}}};
    var attrsToPopulate = req.body.attrsToPopulate || {};
    var query = req.body.query || {};
    return entity_handler_class_1.EntityHandler.find(CITY_COLLECTION, attrsToPopulate, query)
        .then(function (cities) {
        return res.status(200).json(cities);
    })
        .catch(function (errorMsg) {
        (0, error_1.errorFn)(errorMsg, next);
    });
});
router.delete("", function (req, res, next) {
    try {
        if (!req.body._id)
            throw "Document is invalid.";
        var id = new mongodb_1.ObjectId(req.body._id);
        return entity_handler_class_1.EntityHandler.find(city_class_1.City.get_collectionName(), {}, { _id: id })
            .then(function (cities) {
            if (cities.length === 0)
                return (0, error_1.errorFn)("No city found to delete.", next, 404);
            var promises = [];
            for (var _i = 0, cities_1 = cities; _i < cities_1.length; _i++) {
                var city = cities_1[_i];
                promises.push(entity_handler_class_1.EntityHandler.delete(city));
            }
            return Promise.all(promises).then(function (data) {
                return res.status(200).json(data);
            });
        })
            .catch(function (errorMsg) {
            (0, error_1.errorFn)(errorMsg, next);
        });
    }
    catch (error) {
        (0, error_1.errorFn)(typeof error === "string" ? error : "Id invalid.", next);
    }
});
router.delete("/:id", function (req, res, next) {
    try {
        if (!req.params["id"])
            throw "Document is invalid.";
        var id = new mongodb_1.ObjectId(req.params["id"]);
        return entity_handler_class_1.EntityHandler.find(city_class_1.City.get_collectionName(), {}, { _id: id })
            .then(function (cities) {
            if (cities.length === 0)
                return (0, error_1.errorFn)("No city found to delete.", next, 404);
            var promises = [];
            for (var _i = 0, cities_2 = cities; _i < cities_2.length; _i++) {
                var city = cities_2[_i];
                promises.push(entity_handler_class_1.EntityHandler.delete(city));
            }
            return Promise.all(promises).then(function (data) {
                return res.status(200).json(data);
            });
        })
            .catch(function (errorMsg) {
            (0, error_1.errorFn)(errorMsg, next);
        });
    }
    catch (error) {
        (0, error_1.errorFn)(typeof error === "string" ? error : "Id invalid.", next);
    }
});
router.put("", function (req, res, next) {
    try {
        if (!req.body._id)
            throw "Document is invalid.";
        var id = new mongodb_1.ObjectId(req.body._id);
        var cities_3 = [];
        return entity_handler_class_1.EntityHandler.find(CITY_COLLECTION, {}, { _id: id })
            .then(function (_cities) {
            if (_cities.length === 0)
                return (0, error_1.errorFn)("No city found to update.", next, 404);
            cities_3 = _cities;
            return document_handler_class_1.DocumentHandler.toEntity(CITY_COLLECTION, req.body, false);
        })
            .then(function (_city) {
            var promises = [];
            for (var _i = 0, cities_4 = cities_3; _i < cities_4.length; _i++) {
                var _ = cities_4[_i];
                promises.push(entity_handler_class_1.EntityHandler.update(_city));
            }
            return Promise.all(promises).then(function (data) {
                return res.status(200).json(data);
            });
        })
            .catch(function (errorMsg) {
            (0, error_1.errorFn)(errorMsg, next);
        });
    }
    catch (error) {
        (0, error_1.errorFn)(typeof error === "string" ? error : "Id invalid.", next);
    }
});
