"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = __importDefault(require("express"));
var mongodb_1 = require("mongodb");
// classes
var user_class_1 = require("../model/classes/user.class");
// util
var error_1 = require("../util/error");
var entity_handler_class_1 = require("../model/classes/entity-handler.class");
var document_handler_class_1 = require("../model/classes/document-handler.class");
var router = express_1.default.Router();
exports.router = router;
router.post("", function (req, res, next) {
    return document_handler_class_1.DocumentHandler.toEntity(user_class_1.User.get_collectionName(), req.body)
        .then(function (user) {
        return entity_handler_class_1.EntityHandler.save(user);
    })
        .then(function (data) {
        return res.status(201).json(data);
    })
        .catch(function (errorMsg) {
        (0, error_1.errorFn)(errorMsg, next);
    });
});
router.get("", function (req, res, next) {
    // let attrsToPopulate: {} = {"__livesIn": {"$collectionName": "city", "_country": {"$collectionName": "country"}}};
    var attrsToPopulate = req.body.attrsToPopulate || {};
    var query = req.body.query || {};
    return entity_handler_class_1.EntityHandler.find(user_class_1.User.get_collectionName(), attrsToPopulate, query)
        .then(function (users) {
        return res.status(200).json(users);
    })
        .catch(function (errorMsg) {
        (0, error_1.errorFn)(errorMsg, next);
    });
});
router.delete("", function (req, res, next) {
    try {
        if (!req.body._id)
            throw "Document is invalid.";
        var id = new mongodb_1.ObjectId(req.body._id);
        return entity_handler_class_1.EntityHandler.find(user_class_1.User.get_collectionName(), {}, { _id: id })
            .then(function (users) {
            if (users.length === 0)
                return (0, error_1.errorFn)("No user found to delete.", next, 404);
            var promises = [];
            for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
                var user = users_1[_i];
                promises.push(entity_handler_class_1.EntityHandler.delete(user));
            }
            return Promise.all(promises).then(function (data) {
                return res.status(200).json(data);
            });
        })
            .catch(function (errorMsg) {
            (0, error_1.errorFn)(errorMsg, next);
        });
    }
    catch (error) {
        (0, error_1.errorFn)(typeof error === "string" ? error : "Id invalid.", next);
    }
});
router.delete("/:id", function (req, res, next) {
    try {
        if (!req.params["id"])
            throw "Document is invalid.";
        var id = new mongodb_1.ObjectId(req.params["id"]);
        return entity_handler_class_1.EntityHandler.find(user_class_1.User.get_collectionName(), {}, { _id: id })
            .then(function (users) {
            if (users.length === 0)
                return (0, error_1.errorFn)("No user found to delete.", next, 404);
            var promises = [];
            for (var _i = 0, users_2 = users; _i < users_2.length; _i++) {
                var user = users_2[_i];
                promises.push(entity_handler_class_1.EntityHandler.delete(user));
            }
            return Promise.all(promises).then(function (data) {
                return res.status(200).json(data);
            });
        })
            .catch(function (errorMsg) {
            (0, error_1.errorFn)(errorMsg, next);
        });
    }
    catch (error) {
        (0, error_1.errorFn)(typeof error === "string" ? error : "Id invalid.", next);
    }
});
router.put("", function (req, res, next) {
    try {
        if (!req.body._id)
            throw "Document is invalid.";
        var id = new mongodb_1.ObjectId(req.body._id);
        var users_3 = [];
        return entity_handler_class_1.EntityHandler.find(user_class_1.User.get_collectionName(), {}, { _id: id })
            .then(function (_users) {
            if (_users.length === 0)
                return (0, error_1.errorFn)("No user found to update.", next, 404);
            users_3 = _users;
            return document_handler_class_1.DocumentHandler.toEntity(user_class_1.User.get_collectionName(), req.body, false);
        })
            .then(function (_user) {
            var promises = [];
            for (var _i = 0, users_4 = users_3; _i < users_4.length; _i++) {
                var _ = users_4[_i];
                promises.push(entity_handler_class_1.EntityHandler.update(_user));
            }
            return Promise.all(promises).then(function (data) {
                return res.status(200).json(data);
            });
        })
            .catch(function (errorMsg) {
            (0, error_1.errorFn)(errorMsg, next);
        });
    }
    catch (error) {
        (0, error_1.errorFn)(typeof error === "string" ? error : "Id invalid.", next);
    }
});
