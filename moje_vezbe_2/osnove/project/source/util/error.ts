const errorFn = (
  msg: string,
  next: Function,
  statusCode: number = 500
): any => {
  const error = new Error(msg);
  (error as any).httpStatusCode = statusCode;
  return next(error);
};

const errorMiddleware = (error: Error, req: any, res: any, next: any) => {
  return res.status((error as any).httpStatusCode).json({ msg: error.message });
};

export { errorMiddleware, errorFn };
