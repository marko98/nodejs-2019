import { MongoClient, Db } from "mongodb";

let _db: Db;

const mongoConnect = (callback: Function): void => {
  MongoClient.connect("mongodb://localhost:27017/swipe")
    .then((client: MongoClient) => {
      _db = client.db();
      callback();
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

const getDB = (): Promise<Db> =>
  new Promise((resolve, reject) => {
    if (_db) resolve(_db);
    else reject("No database found!");
  });

export { mongoConnect, getDB };
