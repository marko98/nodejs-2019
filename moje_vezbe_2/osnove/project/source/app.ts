import express from "express";
import path from "path";
import fs from "fs";
import { Db } from "mongodb";

import { createServer } from "https";

// util
import { errorFn, errorMiddleware } from "./util/error";
import { getDB, mongoConnect } from "./util/database";

// routers
import * as userRoute from "./routes/user-route";
import * as cityRoute from "./routes/city-route";

// classes
import { EntityHandler } from "./model/classes/entity-handler.class";

const app = express();

app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());

app.get("/", (req, res, next) => {
  let db = getDB().then(
    (db) => {
      return res.send({ msg: "hello" });
    },
    (err) => errorFn(err, next)
  );
});

app.use("/users", userRoute.router);
app.use("/cities", cityRoute.router);

app.use(errorMiddleware);

mongoConnect(() => {
  getDB().then((db: Db) => {
    EntityHandler.setDb(db);

    createServer(
      {
        cert: fs.readFileSync(path.join(__dirname, "../cert", "cert.pem")),
        key: fs.readFileSync(path.join(__dirname, "../cert", "key.pem")),
      },
      app
    ).listen({ port: 3443, host: "0.0.0.0" });
  });
});
