export const isObject = (entry: any) => {
  if (entry === null) {
    return false;
  }
  return typeof entry === "function" || typeof entry === "object";
};

export const getCollectionNameFromConstructorName = (
  constructorName: string
): string => {
  let collectionName: string = constructorName
    .replace(/[A-Z][a-z]*/g, (str) => "_" + str.toLowerCase())
    .replace(/(^_)|(_$)/g, "");

  return collectionName;
};
