import { Entitable } from "./entitable.interface";

export interface Updateable {
  // attrs

  // fns
  update<T>(entity: Entitable): Promise<T>;
}
