import { Idable } from "./idable.interface";
import { FactoryMethodable } from "./factory-methodable.interface";
import { Validateable } from "./validateable.interface";
import { Updateable } from "./updateable.interface";

export interface Entitable
  extends Idable,
    FactoryMethodable,
    Validateable,
    Updateable {
  // attrs
  // fns
}
