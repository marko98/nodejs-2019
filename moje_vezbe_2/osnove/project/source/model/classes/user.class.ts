import { Entitable } from "../interfaces/entitable.interface";

import validator from "validator";
import { City } from "./city.class";

export class User implements Entitable {
  // attrs
  private _name: string = undefined;
  private _surname: string = undefined;
  private static _collectionName: string = "user";
  private __livesIn: City = undefined;
  private static __livesInCollectionName: string = "city";

  // getters & setters
  public get_name(): string {
    return this._name;
  }
  public set_name(_name: string): void {
    this._name = _name;
  }

  public get_surname(): string {
    return this._surname;
  }
  public set_surname(_surname: string): void {
    this._surname = _surname;
  }

  public static get_collectionName(): string {
    return User._collectionName;
  }

  public static get__livesInCollectionName(): string {
    return User.__livesInCollectionName;
  }

  public get__livesIn(): City {
    return this.__livesIn;
  }
  public set__livesIn(livesIn: City): void {
    this.__livesIn = livesIn;
  }

  // fns
  public introduceYourself(): void {}

  // interface Entitable attrs & fns

  // interface FactoryMethodable attrs & fns
  public create(): User {
    return new User();
  }

  // interface Idable attrs & fns
  public get_id(): any {
    return (<any>this)._id;
  }
  public set_id(id: any): void {
    if ((<any>this)._id) throw new Error(`Id exists already, can't change it.`);
    else (<any>this)._id = id;
  }

  // interface Validateable attrs & fns
  public validate<User>(): Promise<User> {
    // throw new Error("Not implemented error!");

    return new Promise<User>((resolve, reject) => {
      try {
        // _name
        if (!this._name) reject(`User must have a name.`);
        this._name = this._name.trim();
        if (validator.isEmpty(this._name))
          reject(`User's name mustn't be empty.`);
        else if (!validator.isLength(this._name, { min: 2, max: 20 }))
          reject(`User's name length is invalid.`);
        else if (!validator.isAlpha(this._name, undefined, { ignore: "-" }))
          reject(`User's name must contains only letters.`);

        // _surname
        if (!this._surname) reject(`User must have a surname.`);
        this._surname = this._surname.trim();
        if (validator.isEmpty(this._surname))
          reject(`User's surname mustn't be empty.`);
        else if (!validator.isLength(this._surname, { min: 2, max: 20 }))
          reject(`User's surname length is invalid.`);
        else if (!validator.isAlpha(this._surname, undefined, { ignore: "-" }))
          reject(`User's surname must contains only letters.`);

        // __livesIn
        if (Array.isArray(this.__livesIn))
          reject(`User's __livesIn mustn't be an array.`);
      } catch (error) {
        reject(`User validation failed.`);
      }

      resolve(<User>(<unknown>this));
    });
  }

  // interface Collectionable attrs & fns
  public getCollectionName(): string {
    return "user";
  }

  // interface Updateable attrs & fns
  public update<User>(entity: Entitable): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      try {
        for (const key of Object.keys(this).filter((key) => key !== "_id")) {
          this[key] = entity[key];
        }
        resolve(<User>(<unknown>this));
      } catch (error) {
        reject(error);
      }
    });
  }
}
