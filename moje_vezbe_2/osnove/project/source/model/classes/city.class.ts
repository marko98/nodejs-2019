import { Entitable } from "../interfaces/entitable.interface";
import { Country } from "./country.class";

import validator from "validator";

export class City implements Entitable {
  // attrs
  private _name: string = undefined;
  private static _collectionName: string = "city";
  private __country: Country = undefined;

  // getters & setters
  public get_name(): string {
    return this._name;
  }
  public set_name(_name: string): void {
    this._name = _name;
  }

  public static get_collectionName(): string {
    return City._collectionName;
  }

  public get__country(): Country {
    return this.__country;
  }
  public set__country(_country: Country): void {
    this.__country = _country;
  }

  // fns

  // interface Entitable attrs & fns

  // interface Idable attrs & fns
  public get_id(): any {
    return (<any>this)._id;
  }
  public set_id(id: any): void {
    if ((<any>this)._id) throw new Error(`Id exists already, can't change it.`);
    else (<any>this)._id = id;
  }

  // interface FactoryMethodable attrs & fns
  public create(): City {
    return new City();
  }

  // interface Validateable attrs & fns
  public validate<City>(): Promise<City> {
    return new Promise<City>((resolve, reject) => {
      try {
        // name
        if (!this._name) reject(`City must have a name.`);
        this._name = this._name.trim();
        if (validator.isEmpty(this._name))
          reject(`City's name mustn't be empty.`);
        else if (!validator.isLength(this._name, { min: 2, max: 20 }))
          reject(`City's name length is invalid.`);
        else if (!validator.isAlpha(this._name, undefined, { ignore: " " }))
          reject(`City's name must contains only letters.`);
      } catch (error) {
        reject(`City validation failed.`);
      }

      resolve(<City>(<unknown>this));
    });
  }

  // interface Updateable attrs & fns
  public update<City>(entity: Entitable): Promise<City> {
    return new Promise<City>((resolve, reject) => {
      try {
        for (const key of Object.keys(this).filter((key) => key !== "_id")) {
          this[key] = entity[key];
        }
        resolve(<City>(<unknown>this));
      } catch (error) {
        reject(error);
      }
    });
  }
}
