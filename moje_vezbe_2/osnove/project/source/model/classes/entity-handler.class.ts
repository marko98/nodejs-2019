import {
  Db,
  DeleteResult,
  InsertOneResult,
  ObjectId,
  UpdateResult,
} from "mongodb";
import {
  getCollectionNameFromConstructorName,
  isObject,
} from "../../helper-fns";
import { getDB, mongoConnect } from "../../util/database";
import { Entitable } from "../interfaces/entitable.interface";
import { DocumentHandler } from "./document-handler.class";
import { User } from "./user.class";

export class EntityHandler {
  // attrs
  private static db: Db;
  private static registry: Entitable[] = [];

  // getters & setters
  public static getDb(): any {
    return EntityHandler.db;
  }
  public static setDb(db: any): void {
    EntityHandler.db = db;
  }

  constructor() {
    // mongoConnect(() => {
    //   getDB().then((db) => {
    //     EntityHandler.db = db;
    // let user = new User();
    // user.set_name("Marko");
    // user.set_surname("Zahorodni");
    // EntityHandler.save(user)
    //   .then((result) => {
    //     console.log(result);
    //   })
    //   .catch((err) => console.log(err));
    // let users: User[];
    // EntityHandler.find<User>("user")
    //   .then((_users) => {
    //     users = _users;
    //     return EntityHandler.find<Country>("country");
    //   })
    //   .then((countries) => {
    //     console.log(users[0]);
    //     users[0].setLivesIn(undefined);
    //     console.log(users[0]);
    //     return EntityHandler.update(users[0]);
    //   })
    //   .then((result) => {
    //     console.log(result);
    //   })
    //   .catch((err) => console.log(err));
    // let country = new Country();
    // country.set_name("Serbia");
    // EntityHandler.save(country)
    //   .then((result) => {
    //     console.log(result);
    //   })
    //   .catch((err) => console.log(err));
    // EntityHandler.find<User>(
    //   "user",
    //   {
    //     _livesIn: {
    //       $collectionName: "city",
    //       _country: { $collectionName: "country" },
    //     },
    //   },
    //   {}
    // )
    //   .then((users) => {
    //     console.log(users);
    //   })
    //   .catch((err) => console.log(err));
    // EntityHandler.find<User>("user")
    //   .then((_users) => {
    //     console.log(_users);
    //     let user = _users.find(
    //       (u) => u.get_id().toString() === "616ee16304b2afc61a46de52"
    //     );
    //     if (user) return EntityHandler.delete(user);
    //   })
    //   .then((result) => {
    //     console.log(result);
    //   })
    //   .catch((err) => console.log(err));
    //   });
    // });
  }

  // fns
  public static updateRegistry(entity: Entitable): boolean {
    let updateHappened: boolean = false;

    EntityHandler.registry = EntityHandler.registry.map((_entity) => {
      if (_entity.get_id().toString() === entity.get_id().toString()) {
        _entity.update(entity);
        updateHappened = true;
        return entity;
      }
      return _entity;
    });

    return updateHappened;
  }

  public static registerEntity(entity: Entitable): boolean {
    if (
      !EntityHandler.registry.find(
        (_entity) => _entity.get_id().toString() === entity.get_id().toString()
      )
    ) {
      EntityHandler.registry.push(entity);
      return true;
    }

    return false;
  }

  public static unregisterEntity(entity: Entitable): boolean {
    let index = EntityHandler.registry.findIndex(
      (_entity) => _entity.get_id().toString() === entity.get_id().toString()
    );

    if (index !== -1) {
      delete EntityHandler.registry[index];
      return true;
    }

    return false;
  }

  public static getEntityOfDocumentIfExistsInRegistry<
    T extends Entitable
  >(document: { _id: ObjectId }): T {
    if (document._id)
      return <T>(
        EntityHandler.registry.find(
          (entity) => document._id.toString() === entity.get_id().toString()
        )
      );
    return undefined;
  }

  public static toDocument(entity: Entitable): {} {
    // let attrKeys: string[] = Object.keys(entity);
    // let attrValues: any[] = Object.values(entity);
    // let primitiveAttrEntries: [string, unknown][] = Object.entries(entity);

    let attrEntries: [string, unknown][] = Object.entries(entity);

    let doc: {} = {};
    for (const attrEntry of attrEntries) {
      if (attrEntry[0] === "_id") {
        doc[attrEntry[0]] = attrEntry[1];

        continue;
      }

      if (Array.isArray(attrEntry[1])) {
        doc[attrEntry[0]] = [];

        for (const el of attrEntry[1]) {
          if (isObject(el)) {
            let id = (<Entitable>el).get_id();

            if (id) doc[attrEntry[0]].push(id);
          } else {
            doc[attrEntry[0]].push(el);
          }
        }

        continue;
      }

      if (isObject(attrEntry[1])) {
        // let id = (<Entitable>attrEntry[1]).get_id();
        let id = (<{ _id: ObjectId }>attrEntry[1])._id;

        if (id) doc[attrEntry[0]] = id;
        // else doc[attrEntry[0]] = null; // mongodb nece uneti vrednosti koje nemaju vrednost
      } else {
        doc[attrEntry[0]] = attrEntry[1];
      }
    }

    return doc;
  }

  public static save(
    entity: Entitable
  ): Promise<{ document: {}; result: InsertOneResult }> {
    return new Promise<{ document: {}; result: InsertOneResult }>(
      (resolve, reject) => {
        try {
          if (entity.get_id()) reject(`Can't save entity, it has _id.`);
          else {
            if (Array.isArray(entity)) reject(`Can't save list.`);

            let collectionName = getCollectionNameFromConstructorName(
              entity.constructor.name
            );

            let doc: {} = EntityHandler.toDocument(entity);

            EntityHandler.db
              .collection(collectionName)
              .insertOne(doc)
              .then((result: InsertOneResult) => {
                entity.set_id(doc["_id"]);
                EntityHandler.registerEntity(entity);
                resolve({
                  document: doc,
                  result: result,
                });
              })
              .catch((err) =>
                reject(`${entity.constructor.name} couldn't be created.`)
              );
          }
        } catch (error) {
          reject(`Can't save, error occurred.`);
        }
      }
    );
  }

  public static update(
    entity: Entitable
  ): Promise<{ document: {}; result: UpdateResult }> {
    return new Promise<{ document: {}; result: UpdateResult }>(
      (resolve, reject) => {
        try {
          if (entity.get_id()) {
            if (Array.isArray(entity)) reject(`Can't update list.`);

            let collectionName = getCollectionNameFromConstructorName(
              entity.constructor.name
            );

            let doc: {} = EntityHandler.toDocument(entity);

            EntityHandler.db
              .collection(collectionName)
              // .replaceOne({ _id: (<ObjectId>entity.get_id()).toString() }, doc)
              .updateOne({ _id: entity.get_id() }, { $set: { ...doc } })
              .then((result: UpdateResult) => {
                if (result.matchedCount === 1 && result.modifiedCount === 1)
                  EntityHandler.updateRegistry(entity);

                resolve({
                  document: doc,
                  result: result,
                });
              })
              .catch(
                (error) =>
                  reject(
                    `${entity.constructor.name} couldn't be updated. ${error}`
                  )
                // reject(`${entity.constructor.name} couldn't be updated.`)
              );
          } else reject(`Can't update entity, it doesn't have _id.`);
        } catch (error) {
          console.log(error);
          reject(`Can't update, error occurred.`);
        }
      }
    );
  }

  public static delete(
    entity: Entitable
  ): Promise<{ document: {}; result: DeleteResult }> {
    return new Promise<{ document: {}; result: DeleteResult }>(
      (resolve, reject) => {
        try {
          if (entity.get_id()) {
            if (Array.isArray(entity)) reject(`Can't delete list.`);

            let collectionName = getCollectionNameFromConstructorName(
              entity.constructor.name
            );

            let doc: {} = EntityHandler.toDocument(entity);

            EntityHandler.db
              .collection(collectionName)
              .deleteOne({ _id: entity.get_id() })
              .then((result: DeleteResult) => {
                if (result.deletedCount === 1) entity = undefined;

                resolve({
                  document: doc,
                  result: result,
                });
              })
              .catch((err) =>
                reject(`${entity.constructor.name} couldn't be deleted.`)
              );
          } else reject(`Can't delete entity, it doesn't have _id.`);
        } catch (error) {
          console.log(error);
          reject(`Can't delete, error occurred.`);
        }
      }
    );
  }

  public static find<T extends Entitable>(
    collectionName: string,
    attrsToPopulate: {} = {},
    query: {} = {}
  ): Promise<T[]> {
    return EntityHandler.db
      .listCollections({ name: collectionName })
      .toArray()
      .then((collections: any[]) => {
        if (collections.length === 0)
          return Promise.reject(`Collection ${collectionName} doesn't exists.`);

        // collection exists

        return EntityHandler.db
          .collection(collectionName)
          .find(query)
          .toArray()
          .then((docs) => {
            let promises: Promise<T>[] = docs.map((doc) =>
              DocumentHandler.toEntity<T>(
                collectionName,
                <{ _id: ObjectId }>doc
              )
            );
            return Promise.all(promises);
          })
          .then((entities: T[]) => {
            let data: {
              entity: any;
              key: string;
              isArray: boolean;
              promise: Promise<any>;
            }[] = [];

            // NE ZNAM BAS KOLIKO JE DOBRA IDEJA
            // also register fetched entites, or update registry if they exist already
            // ili ipak staviti da se entitet registruje ako vec nije registrovan
            for (const entity of entities) {
              EntityHandler.registerEntity(entity);
              // if (!EntityHandler.registerEntity(entity))
              //   EntityHandler.updateRegistry(entity);

              for (const key in attrsToPopulate) {
                if (Object.prototype.hasOwnProperty.call(entity, key)) {
                  const collectionName =
                    attrsToPopulate[key]["$collectionName"];
                  const query = attrsToPopulate[key]["$query"] || {};

                  if (Array.isArray(entity[key])) {
                    for (const objId of entity[key]) {
                      if (objId instanceof ObjectId) {
                        data.push({
                          entity: entity,
                          key: key,
                          isArray: true,
                          promise: EntityHandler.find(
                            collectionName,
                            attrsToPopulate[key],
                            { _id: objId, ...query }
                          ),
                        });
                      }
                    }

                    entity[key] = (<[]>entity[key]).filter(
                      (el: any) => !(el instanceof ObjectId)
                    );
                  } else if (entity[key] instanceof ObjectId) {
                    data.push({
                      entity: entity,
                      key: key,
                      isArray: false,
                      promise: EntityHandler.find(
                        collectionName,
                        attrsToPopulate[key],
                        { _id: entity[key], ...query }
                      ),
                    });
                  }
                }
              }
            }

            return Promise.all(
              data.map(
                (dataEl: {
                  entity: any;
                  key: string;
                  isArray: boolean;
                  promise: Promise<any>;
                }) => dataEl.promise
              )
            ).then((values) => {
              values = values.flat();
              // console.log(values);
              if (values.length === data.length) {
                for (let index = 0; index < data.length; index++) {
                  const value = values[index];
                  const dataEl: {
                    entity: any;
                    key: string;
                    isArray: boolean;
                    promise: Promise<any>;
                  } = data[index];

                  if (dataEl.isArray) {
                    dataEl.entity[dataEl.key].push(value);
                  } else {
                    dataEl.entity[dataEl.key] = value;
                  }
                }
              }
              return Promise.resolve(entities);
            });
          });
      });
  }
}

// new EntityHandler();
