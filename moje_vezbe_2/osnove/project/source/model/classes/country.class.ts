import { Entitable } from "../interfaces/entitable.interface";

export class Country implements Entitable {
  // attrs
  private _name: string = undefined;
  private static _collectionName: string = "country";

  // getters & setters
  public get_name(): string {
    return this._name;
  }
  public set_name(_name: string): void {
    this._name = _name;
  }

  public static get_collectionName(): string {
    return Country._collectionName;
  }

  // fns

  // interface Entitable attrs & fns

  // interface Idable attrs & fns
  public get_id(): any {
    return (<any>this)._id;
  }
  public set_id(id: any): void {
    if ((<any>this)._id) throw new Error(`Id exists already, can't change it.`);
    else (<any>this)._id = id;
  }

  // interface FactoryMethodable attrs & fns
  public create(): Country {
    return new Country();
  }

  // interface Validateable attrs & fns
  public validate<Countr>(): Promise<Countr> {
    return new Promise<Countr>((resolve, reject) => {
      resolve(<Countr>(<unknown>this));
    });
  }

  // interface Updateable attrs & fns
  public update<Country>(entity: Entitable): Promise<Country> {
    return new Promise<Country>((resolve, reject) => {
      try {
        for (const key of Object.keys(this).filter((key) => key !== "_id")) {
          this[key] = entity[key];
        }
        resolve(<Country>(<unknown>this));
      } catch (error) {
        reject(error);
      }
    });
  }
}
