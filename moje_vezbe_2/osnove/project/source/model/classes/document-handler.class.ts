import { Entitable } from "../interfaces/entitable.interface";
import { User } from "./user.class";
import { Country } from "./country.class";
import { City } from "./city.class";

import { ObjectId } from "mongodb";
import { EntityHandler } from "./entity-handler.class";

export class DocumentHandler {
  // attrs
  private static collectionNameEntityClassRegistry: { [key: string]: any } = {
    user: User,
    country: Country,
    city: City,
  };

  // fns

  public static toEntity<T extends Entitable>(
    collectionName: string,
    document: { _id: ObjectId },
    checkRegistry: boolean = true
  ): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      if (document._id) {
        if (!(document._id instanceof ObjectId))
          document._id = new ObjectId(document._id);

        if (checkRegistry) {
          let entity: T =
            EntityHandler.getEntityOfDocumentIfExistsInRegistry<T>(document);
          if (entity) resolve(entity);
        }
      }

      let instance: T = new DocumentHandler.collectionNameEntityClassRegistry[
        collectionName
      ]();

      let keys: string[] = Object.keys(document);
      let methodNames: string[] = keys.map((attrName) =>
        !(attrName[0] === "_")
          ? attrName[0].toUpperCase() + attrName.substr(1)
          : attrName
      );

      try {
        let data: {
          entity: any;
          methodName: string;
          isArray: boolean;
          promise: Promise<any>;
        }[] = [];

        for (let index = 0; index < methodNames.length; index++) {
          const methodName = methodNames[index];
          let value = document[keys[index]];

          // vrednost atributa je entitet ili lista entiteta, mora/ju vec postojati
          if (methodName.includes("__", 0)) {
            if (Array.isArray(value)) {
              let _ids = value;
              for (let _id of _ids) {
                try {
                  if (!(_id instanceof ObjectId)) _id = new ObjectId(_id);
                } catch (error) {
                  reject(
                    `Entity can't be created, some atribute document is invalid.`
                  );
                }

                // 1) pogledaj registar
                let entity: T =
                  EntityHandler.getEntityOfDocumentIfExistsInRegistry<T>({
                    _id: _id,
                  });
                if (entity) {
                  let array = instance["get" + methodName]();
                  array.push(value);
                  instance["set" + methodName](array);
                  // instance["set" + methodName](entity);
                } else {
                  // 2) dobavi iz baze
                  let _collectionName =
                    DocumentHandler.collectionNameEntityClassRegistry[
                      collectionName
                    ]["get" + keys[index] + "CollectionName"]();

                  data.push({
                    entity: instance,
                    methodName: methodName,
                    isArray: true,
                    promise: EntityHandler.find(
                      _collectionName,
                      {},
                      { _id: _id }
                    ),
                  });
                }
              }
            } else {
              let _id = value;

              if (!_id) {
                instance["set" + methodName](undefined);
                continue;
              }

              try {
                if (!(_id instanceof ObjectId)) _id = new ObjectId(_id);
              } catch (error) {
                reject(
                  `Entity can't be created, some atribute document is invalid.`
                );
              }

              // 1) pogledaj registar
              let entity: T =
                EntityHandler.getEntityOfDocumentIfExistsInRegistry<T>({
                  _id: _id,
                });
              if (entity) instance["set" + methodName](entity);
              else {
                // 2) dobavi iz baze
                let _collectionName =
                  DocumentHandler.collectionNameEntityClassRegistry[
                    collectionName
                  ]["get" + keys[index] + "CollectionName"]();

                data.push({
                  entity: instance,
                  methodName: methodName,
                  isArray: false,
                  promise: EntityHandler.find(
                    _collectionName,
                    {},
                    { _id: _id }
                  ),
                });
              }
            }
          } else {
            instance["set" + methodName](value);
          }
        }

        return Promise.all(
          data.map(
            (dataEl: {
              entity: any;
              methodName: string;
              isArray: boolean;
              promise: Promise<any>;
            }) => dataEl.promise
          )
        )
          .then((values: Entitable[]) => {
            values = values.flat();

            if (values.length === data.length) {
              for (let index = 0; index < data.length; index++) {
                const value = values[index];
                const dataEl: {
                  entity: any;
                  methodName: string;
                  isArray: boolean;
                  promise: Promise<any>;
                } = data[index];

                if (dataEl.isArray) {
                  let array: any[] = dataEl.entity["get" + dataEl.methodName]();
                  array.push(value);
                  dataEl.entity["set" + dataEl.methodName](array);
                } else {
                  dataEl.entity["set" + dataEl.methodName](value);
                }
              }
            }
            return resolve(instance);
          })
          .catch((errorMsg: string) => {
            reject(errorMsg);
          });
      } catch (error) {
        reject(`Entity can't be created, document is invalid.`);
      }
    }).then((instance) => {
      return instance.validate<T>();
    });
  }
}
