import express from "express";
import { DeleteResult, InsertOneResult, ObjectId, UpdateResult } from "mongodb";

// classes
import { City } from "../model/classes/city.class";

// util
import { errorFn } from "../util/error";
import { EntityHandler } from "../model/classes/entity-handler.class";
import { DocumentHandler } from "../model/classes/document-handler.class";

const router = express.Router();
const CITY_COLLECTION = "city";

router.post("", (req: express.Request, res: express.Response, next) => {
  return DocumentHandler.toEntity<City>(CITY_COLLECTION, req.body)
    .then((city) => {
      return EntityHandler.save(city);
    })
    .then((data: { document: {}; result: InsertOneResult }) => {
      return res.status(201).json(data);
    })
    .catch((errorMsg: string) => {
      errorFn(errorMsg, next);
    });
});

router.get("", (req: express.Request, res: express.Response, next) => {
  // let attrsToPopulate: {} = {"_livesIn": {"$collectionName": "city", "_country": {"$collectionName": "country"}}};
  let attrsToPopulate: {} = req.body.attrsToPopulate || {};
  let query: {} = req.body.query || {};
  return EntityHandler.find<City>(CITY_COLLECTION, attrsToPopulate, query)
    .then((cities: City[]) => {
      return res.status(200).json(cities);
    })
    .catch((errorMsg: string) => {
      errorFn(errorMsg, next);
    });
});

router.delete("", (req: express.Request, res: express.Response, next) => {
  try {
    if (!req.body._id) throw "Document is invalid.";

    let id: ObjectId = new ObjectId(req.body._id);

    return EntityHandler.find<City>(City.get_collectionName(), {}, { _id: id })
      .then((cities) => {
        if (cities.length === 0)
          return errorFn(`No city found to delete.`, next, 404);

        let promises: Promise<any>[] = [];
        for (const city of cities) promises.push(EntityHandler.delete(city));

        return Promise.all(promises).then(
          (data: { document: {}; result: DeleteResult }[]) => {
            return res.status(200).json(data);
          }
        );
      })
      .catch((errorMsg: string) => {
        errorFn(errorMsg, next);
      });
  } catch (error) {
    errorFn(typeof error === "string" ? error : `Id invalid.`, next);
  }
});

router.delete("/:id", (req: express.Request, res: express.Response, next) => {
  try {
    if (!req.params["id"]) throw "Document is invalid.";

    let id: ObjectId = new ObjectId(req.params["id"]);

    return EntityHandler.find<City>(City.get_collectionName(), {}, { _id: id })
      .then((cities) => {
        if (cities.length === 0)
          return errorFn(`No city found to delete.`, next, 404);

        let promises: Promise<any>[] = [];
        for (const city of cities) promises.push(EntityHandler.delete(city));

        return Promise.all(promises).then(
          (data: { document: {}; result: DeleteResult }[]) => {
            return res.status(200).json(data);
          }
        );
      })
      .catch((errorMsg: string) => {
        errorFn(errorMsg, next);
      });
  } catch (error) {
    errorFn(typeof error === "string" ? error : `Id invalid.`, next);
  }
});

router.put("", (req: express.Request, res: express.Response, next) => {
  try {
    if (!req.body._id) throw "Document is invalid.";

    let id: ObjectId = new ObjectId(req.body._id);

    let cities: City[] = [];
    return EntityHandler.find<City>(CITY_COLLECTION, {}, { _id: id })
      .then((_cities: City[]) => {
        if (_cities.length === 0)
          return errorFn(`No city found to update.`, next, 404);
        cities = _cities;

        return DocumentHandler.toEntity<City>(CITY_COLLECTION, req.body, false);
      })
      .then((_city: City) => {
        let promises: Promise<any>[] = [];
        for (const _ of cities) promises.push(EntityHandler.update(_city));

        return Promise.all(promises).then(
          (data: { document: {}; result: UpdateResult }[]) => {
            return res.status(200).json(data);
          }
        );
      })
      .catch((errorMsg: string) => {
        errorFn(errorMsg, next);
      });
  } catch (error) {
    errorFn(typeof error === "string" ? error : `Id invalid.`, next);
  }
});

export { router };
