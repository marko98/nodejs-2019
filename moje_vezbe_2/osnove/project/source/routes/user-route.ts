import express from "express";
import { DeleteResult, InsertOneResult, ObjectId, UpdateResult } from "mongodb";

// classes
import { User } from "../model/classes/user.class";

// util
import { errorFn } from "../util/error";
import { EntityHandler } from "../model/classes/entity-handler.class";
import { DocumentHandler } from "../model/classes/document-handler.class";

const router = express.Router();

router.post("", (req: express.Request, res: express.Response, next) => {
  return DocumentHandler.toEntity<User>(User.get_collectionName(), req.body)
    .then((user) => {
      return EntityHandler.save(user);
    })
    .then((data: { document: {}; result: InsertOneResult }) => {
      return res.status(201).json(data);
    })
    .catch((errorMsg: string) => {
      errorFn(errorMsg, next);
    });
});

router.get("", (req: express.Request, res: express.Response, next) => {
  // let attrsToPopulate: {} = {"__livesIn": {"$collectionName": "city", "_country": {"$collectionName": "country"}}};
  let attrsToPopulate: {} = req.body.attrsToPopulate || {};
  let query: {} = req.body.query || {};
  return EntityHandler.find<User>(
    User.get_collectionName(),
    attrsToPopulate,
    query
  )
    .then((users: User[]) => {
      return res.status(200).json(users);
    })
    .catch((errorMsg: string) => {
      errorFn(errorMsg, next);
    });
});

router.delete("", (req: express.Request, res: express.Response, next) => {
  try {
    if (!req.body._id) throw "Document is invalid.";

    let id: ObjectId = new ObjectId(req.body._id);

    return EntityHandler.find<User>(User.get_collectionName(), {}, { _id: id })
      .then((users) => {
        if (users.length === 0)
          return errorFn(`No user found to delete.`, next, 404);

        let promises: Promise<any>[] = [];
        for (const user of users) promises.push(EntityHandler.delete(user));

        return Promise.all(promises).then(
          (data: { document: {}; result: DeleteResult }[]) => {
            return res.status(200).json(data);
          }
        );
      })
      .catch((errorMsg: string) => {
        errorFn(errorMsg, next);
      });
  } catch (error) {
    errorFn(typeof error === "string" ? error : `Id invalid.`, next);
  }
});

router.delete("/:id", (req: express.Request, res: express.Response, next) => {
  try {
    if (!req.params["id"]) throw "Document is invalid.";

    let id: ObjectId = new ObjectId(req.params["id"]);

    return EntityHandler.find<User>(User.get_collectionName(), {}, { _id: id })
      .then((users) => {
        if (users.length === 0)
          return errorFn(`No user found to delete.`, next, 404);

        let promises: Promise<any>[] = [];
        for (const user of users) promises.push(EntityHandler.delete(user));

        return Promise.all(promises).then(
          (data: { document: {}; result: DeleteResult }[]) => {
            return res.status(200).json(data);
          }
        );
      })
      .catch((errorMsg: string) => {
        errorFn(errorMsg, next);
      });
  } catch (error) {
    errorFn(typeof error === "string" ? error : `Id invalid.`, next);
  }
});

router.put("", (req: express.Request, res: express.Response, next) => {
  try {
    if (!req.body._id) throw "Document is invalid.";

    let id: ObjectId = new ObjectId(req.body._id);

    let users: User[] = [];
    return EntityHandler.find<User>(User.get_collectionName(), {}, { _id: id })
      .then((_users: User[]) => {
        if (_users.length === 0)
          return errorFn(`No user found to update.`, next, 404);
        users = _users;

        return DocumentHandler.toEntity<User>(
          User.get_collectionName(),
          req.body,
          false
        );
      })
      .then((_user: User) => {
        let promises: Promise<any>[] = [];
        for (const _ of users) promises.push(EntityHandler.update(_user));

        return Promise.all(promises).then(
          (data: { document: {}; result: UpdateResult }[]) => {
            return res.status(200).json(data);
          }
        );
      })
      .catch((errorMsg: string) => {
        errorFn(errorMsg, next);
      });
  } catch (error) {
    errorFn(typeof error === "string" ? error : `Id invalid.`, next);
  }
});

export { router };
