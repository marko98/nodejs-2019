const express = require('express');
const { body } = require('express-validator/check');

const authController = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

// PUT => /auth/signup
router.put('/signup', [
    body('email')
        .isEmail()
        .withMessage('Please enter a valid email.')
        .custom((value, { req }) => {
            return User.findOne({email: value})
                .then(userDoc => {
                    if(userDoc){
                        return Promise.reject('Email address already exists.');
                    }
                })
        })
        .normalizeEmail(),
    body('password').trim().isLength({min: 5}),
    // not().isEmpty() -> da ne bude prazna vrednost za name
    body('name').trim().not().isEmpty()
], authController.putUser);

// POST => /auth/login
router.post('/login', [
    body('email')
        .normalizeEmail()
], authController.postLogin);

module.exports = router;