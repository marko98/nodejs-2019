const express = require('express');
const { body } = require('express-validator/check');

const userController = require('../controllers/user');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// GET -> /user
router.get('', isAuth, userController.getUser);

// PUT -> /user
router.put('', isAuth, [
    body('status')
        .trim()
        .not()
        .isEmpty()
], userController.putUser);

module.exports = router;