const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const multer = require('multer');
const uuidv4 = require('uuid/v4')

const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');

const app = express();

const fileStorage = multer.diskStorage({
    destination: function(req, file, cb) {
        // folder images mora vec da postoji inace nece raditi
        cb(null, 'images');
    },
    filename: function(req, file, cb) {
        cb(null, uuidv4() + file.originalname);
    }
})

const fileFilter = (req, file, cb) => {
    if(
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/jpg'
    ){
        cb(null, true);
    } else {
        cb(null, false);
    }
}

// app.use(bodyParser.urlencoded({extended: false})); // za x-www-forms-urlencoded <- default data ako je submitovana kroz formu
// bodyParser middlewear ce dodati req.body
app.use(bodyParser.json()); // za application/json
app.use(
    multer({storage: fileStorage, fileFilter: fileFilter}).single('image')
);
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res, next) => {
    /**
     * za resavanje CORS gresaka(procitaj.txt 1)) moramo poslati informacije 
     * o tome sta dozvoljavamo na browser da bi on dozvolio komunikaciju izmedju
     * servera i klijenta
     * 
     * 'Access-Control-Allow-Origin' -> definisemo ko sve sme da pristupi nasem server-side serveru
     * npr.:
     * 'Access-Control-Allow-Origin': 'codepen.io,...' -> samo neki odredjeni
     * 'Access-Control-Allow-Origin': '*' -> svi
     * 
     * 'Access-Control-Allow-Methods' -> koje metode dozvoljavamo
     * 
     * 'Access-Control-Allow-Headers' -> koje header-e dozvoljavamo da budu postavljeni
     */
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})

app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);
app.use('/user', userRoutes);

app.use((error, req, res, next) => {
    console.log(error);

    const status = error.statusCode || 500;
    const data = error.data;
    // error.message -> string koji je prosledjen konstruktoru
    const message = error.message;
    res.
        status(status)
        .json({
            message: message,
            data: data
        });
})

mongoose
    .connect('mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/messages?retryWrites=true&w=majority')
    .then(result => {
        // app.listen(...) vraca http.Server
        const httpServer = app.listen(8080);
        /**
         * require('socket.io') vraca f-ju, koja za argument uzima http.Server
         * koji koristi http protokol, a nasem socket-u http protokol sluzi kao 
         * osnova za uspostavljanje websocket protokola
         */
        const io = require('./socket').init(httpServer);
        io.on('connection', socket => {
            console.log('Client connected!');
        });
    })
    .catch(err => console.log(err));
