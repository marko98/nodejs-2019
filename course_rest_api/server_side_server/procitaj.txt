---------------------------------------------------------------------------------------------------
1) CORS Error
Cross-Origin-Resource-Sharing

nastaje ukoliko se serverska i klijentska strana 'servira' na razlicitim domenima, tj. njihovi
serveri(server koji servira front-end i server koji servira serversku stranu za obradu informacija)
'trce' na razlicitim domenima, samim tim i adresama

npr. 
Client -> localhost:4000
Server -> localhost:3000

po defaultu to browseri ne dozvoljavaju i zbog toga se javlja taj error
resenje: na response dodati header-e koji 'govore' browseru da mi to dozvoljavamo
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
2) video 378 -> za windows korisnike(vezano za multer-a)
npm install --save uuid
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
3) JWT(JSON Web Token)
npm install --save jsonwebtoken

JSON Data + Signature(moze biti verifikovan samo od server-side servera uz pomoc 
tajnog kljuca(secret key)) = JWT

na sajtu jwt.io mozes da uneses sa leve strane(Encoded ...) token koji si dobio na frontendu i
da dobijes payload, sa desne strane, koji je server-side server poslao
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
4) Async Await in Node.js

Useful resources:

Async-await - More Details: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
5) Websockets & Socket.io
za server-side server:
npm install --save socket.io
za client-side server: 
npm install --save socket.io-client

Websocket je protokol isto kao i http, ali je nadogradjen na http protokolu, a 
socket.io(3rd party package) koristi websocket protokol

dakle websocket nam omogucava da posaljemo podatke svim konentovanim klijentima nakon kreiranja, 
izmene, brisanja odredjenih podataka

Useful resources:

Socket.io Official Docs: https://socket.io/get-started/chat/

Alternative Websocket Library: https://www.npmjs.com/package/express-ws
---------------------------------------------------------------------------------------------------