const { validationResult } = require('express-validator/check');
const fs = require('fs');
const path = require('path');

const io = require('../socket');
const Post = require('../models/post');
const User = require('../models/user');

module.exports.getPosts = async (req, res, next) => {
    const page = req.query.page || 1;
    const perPage = 2;

    try{
        const totalItems = await Post.find().countDocuments();
        const posts = await Post.find()
                .populate('creator')
                /**
                 * sortiranje po opadajucem redosledu, od najskorije kreiranog posta
                 * -1 -> desc
                 */
                .sort({createdAt: -1})
                .skip((page - 1) * perPage)
                .limit(perPage);
        res.status(200).json({
            message: 'Fetched posts successfully.',
            posts: posts,
            totalItems: totalItems
        });
    } catch(err) {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    }
}

// module.exports.getPosts = (req, res, next) => {
//     const page = req.query.page || 1;
//     const perPage = 2;
//     let totalItems;
//     Post.find().countDocuments().populate('creator')
//     .then(count => {
//         totalItems = count;
//         return Post.find()
//             .skip((page - 1) * perPage)
//             .limit(perPage);
//     })
//     .then(posts => {
//         /**
//          * res.status(200).json({...}) -> ce postaviti header: 'Content-Type': 'application/json' automatski,
//          * dok na strani klijenta kada saljemo zahtev uglavnom mi moramo da stavimo header (pogledaj 
//          * fetch(...) metodu -> code_pen_example/code_pen.js)
//          */
//         res.status(200).json({
//             message: 'Fetched posts successfully.',
//             posts: posts,
//             totalItems: totalItems
//         });
//     })
//     .catch(err => {
//         if(!err.statusCode){
//             err.statusCode = 500;
//         }
//         next(err);
//     })
// }

module.exports.getPost = (req, res, next) => {
    const postId = req.params.postId;
    Post.findById(postId)
        .then(post => {
            if(!post){
                const error = new Error('Could not find post.');
                // 404 statusni kod za not found
                error.statusCode = 404;
                /**
                 * throw error nije ispravno jer je asinhroni kod u pitanju i dovesce do greske, odnosno
                 * ucice u catch(err) blok i param err ce biti ustvari taj error koji je throw-ovan
                 */
                throw error;
            }
            res.status(200).json({
                message: 'Post fetched.',
                post: post
            });
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
}

module.exports.postPost = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation failed, entered data is incorrect.');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }

    if(!req.file){
        const error = new Error('No image provided.');
        error.statusCode = 422;
        throw error;
    }
    const imageUrl = req.file.path.replace("\\" ,"/");
    const title = req.body.title;
    const content = req.body.content;
    let creator;

    const post = new Post({
        title: title, 
        content: content,
        imageUrl: imageUrl,
        creator: req.userId
    });

    post.save()
        .then(post => {
            // console.log(post);
            return User.findById(req.userId);
        })
        .then(user => {
            creator = user;
            user.posts.push(post);
            return user.save();
        })
        .then(user => {
            /**
             * io.getIO().emit(...) salje svim konektovanim klijentima
             * io.getIO().broadcast(...) salje svim konektovanim klijentima sem onom koji je kreirao post
             */
            io.getIO().emit('posts', {action: 'create', post: {...post._doc, creator: {_id: req.userId, name: user.name}}});
            res.status(201).json({
                massage: 'Post created successfully',
                post: post,
                creator: {_id: creator._id, name: creator.name}
            })
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
}

module.exports.putPost = (req, res, next) => {
    const postId = req.params.postId;

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation failed, entered data is incorrect.');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }

    const title = req.body.title;
    const content = req.body.content;
    let imageUrl = req.body.image;
    if(req.file){
        imageUrl = req.file.path.replace("\\","/");
    }

    if(!imageUrl){
        const error = new Error('No file picked.');
        error.statusCode = 422;
        throw error;
    }
    
    Post.findById(postId)
        .populate('creator')
        .then(post => {
            if(!post){
                const error = new Error('Could not find post.');
                error.statusCode = 404;
                throw error;
            }
            if(post.creator._id.toString() !== req.userId){
                const error = new Error('Not authorized.');
                // 403 status code for not authorized
                error.statusCode = 403;
                throw error;
            }
            if(imageUrl !== post.imageUrl){
                deleteImage(post.imageUrl);
            }
            post.title = title;
            post.content = content;
            post.imageUrl = imageUrl;
            return post.save();
        })
        .then(post => {
            io.getIO().emit('posts', {action: 'update', post: post});
            res.status(200).json({
                message: 'Post updated!',
                post: post
            });
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
}

module.exports.deletePost = (req, res, next) => {
    const postId = req.params.postId;
    Post.findById(postId)
        .then(post => {
            if(!post){
                const error = new Error('Could not find post.');
                error.statusCode = 404;
                throw error;
            }
            if(post.creator.toString() !== req.userId){
                const error = new Error('Not authorized.');
                // 403 status code for not authorized
                error.statusCode = 403;
                throw error;
            }

            deleteImage(post.imageUrl);
            return Post.findByIdAndRemove(postId);
        })
        .then(result => {
            // console.log(result);
            return User.findById(req.userId);
        })
        .then(user => {
            user.posts.pull(postId);
            return user.save();
        })
        .then(user => {
            io.getIO().emit('posts', {action: 'delete', post: postId});
            res.status(200).json({message: 'Deleted post.'});
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
}

const deleteImage = filePath => {
    filePath = path.join(__dirname, '..', filePath);
    fs.unlink(filePath, err => {
        if(err){
            console.log(err);
        }
    });
}