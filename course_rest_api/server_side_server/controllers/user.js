const User = require('../models/user');
const { validationResult } = require('express-validator/check');

module.exports.getUser = (req, res, next) => {
    User.findById(req.userId)
        .then(user => {
            if(!user){
                const error = new Error('User not authenticated.');
                // 401 status code for not authenticated
                error.statusCode = 401;
                throw error;
            }
            res.status(200).json({
                message: 'Fetched user successfully.',
                userId: user._id,
                status: user.status
            })
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
}

module.exports.putUser = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation failed, entered data is incorrect.');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }

    const status = req.body.status;
    User.findById(req.userId)
    .then(user => {
        if(!user){
            const error = new Error('User not authenticated.');
            // 401 status code for not authenticated
            error.statusCode = 401;
            throw error;
        }
        user.status = status;
        return user.save();
    })
    .then(user => {
        res.status(200).json({
            message: 'User\'s status updated.',
            userId: user._id
        })
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
}