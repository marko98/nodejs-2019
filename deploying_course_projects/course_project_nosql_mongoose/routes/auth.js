const express = require('express');
/**
 * ukoliko zelimo da exportujemo bas neku f-ju iz 3rd party package-a
 * mozemo koristiti sledecu sintaksu:
 * const { check } = require('express-validator/check');
 * 
 * ili
 * 
 * const expressValidator = require('express-validator/check');
 */
const { check, body } = require('express-validator/check');

const authController = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

router.get('/login', authController.getLogin);

router.post('/login', [
    /**
     * ovde je moguce uraditi i ostale provere iz kontrolera, ali onda nam on ne bi ni trebo na kraju
     * tako da sam provere(npr. provera da li postoji taj email, da li se lozinke poklapaju) ostavio
     * unutar kontrolera
     * 
     * ovde treba proveravati vrednosti inputa, da li je ispravna, a ne i logicku ispravnost
     * Logicka ispravnost treba biti proverena u kontroleru!
     * 
     * dole u okviru skripte se nalaze primeri nekih custom validatora :)
     */
    body('email')
        .isEmail().withMessage('Please enter a valid email.')
        // izmedju ostalog uklanja i white space-ove(npr. uneto je 'Test@test.com ', a posle normalizeEmail() ce biti 'test@test.com')
        .normalizeEmail(),
    body(
        'password',
        'Password has to be valid.'
        )
        .isLength({min: 5, max: 30})
        .isAlphanumeric()
        // uklanjanje white space-ova
        .trim()
], authController.postLogin);

router.post('/logout', authController.postLogout);

router.get('/signup', authController.getSignup);

/**
 * expressValidator.check('email').isEmail() ce tragati za poljem name='email',
 * nakon sto ga pronadje ce da primeni neke f-je za validaciju email-a nad vrednoscu tog polja,
 * zatim ce rezultat(mogu biti i greske) zakaciti za request
 * 
 * withMessage() se uvek odnosi na proveru ispred nje, u nasem slucaju na isEmail()
 * moguce je imati:
 * expressValidator.check('email').isEmail().withMessage().isAlphanumeric().withMessage()...
 */
router.post('/signup', [
    // f-ja check() ce tragati za 'email' svuda i u body-u, i u header-u i u cookie-ima itd
    check('email')
        .isEmail().withMessage('Please enter a valid email.')
        // izmedju ostalog uklanja i white space-ove(npr. uneto je 'Test@test.com ', a posle normalizeEmail() ce biti 'test@test.com')
        .normalizeEmail()
        .custom((value, {req}) => {
            // if(value === 'test@test.com'){
            //     throw Error('This email address is forbidden.')
            // }
            // return true;

            /**
             * ovde je uradjena logicka validacija radi primera kako se radi izmedju ostalog i sa asinhronim kodom
             * logicka ispravnost(validacija) treba biti proverena(napisana) u kontroleru!
             */
            return User.findOne({email: value})
                .then(userDoc => {
                    if(userDoc){
                        /**
                         * ovo je kao da si napisao throw Error('poruka'), ali ispravnije
                         * 
                         * ukoliko bi ipak ovde napisao: throw Error('poruka') van if-a bi trebao dodati:
                         * return true
                         */
                        return Promise.reject('E-Mail exists already, please pick a differnet one.');
                    }
                })
        }),
    /**
     * f-ja body() ce tragati za 'password' samo u body-u,
     * ukoliko imamo poruku koju zelimo da prikazemo bez obzira kod koga validatora je nastala greska mozemo
     * je postaviti kao drugi parametar f-je body(...), odnosice se na sve validatore
     */
    body(
        'password',
        'Please enter a password with only numbers and text and at least 5 characters.'
        )
        // u produkciji sifra treba da bude sto duza i da ima specijalne karaktere!!!
        .isLength({min: 5, max: 30})
        .isAlphanumeric()
        .trim(),
    body('confirmPassword')
        .trim()
        .custom((value, { req }) => {
            if(value !== req.body.password){
                throw Error('Passwords have to match!');
            }
            return true;
        })
], authController.postSignup);

router.get('/reset-password', authController.getResetPassword);

router.post('/reset-password', authController.postResetPassword);

router.get('/new-password/:token', authController.getNewPassword);

router.post('/new-password', authController.postNewPassword);

module.exports = router;