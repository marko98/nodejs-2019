const mongoose = require('mongoose');
const { validationResult } = require('express-validator/check');

const Product = require('../models/product');
const rootDir = require('../util/path');
const fileHelper = require('../util/file');

module.exports.getAddProduct = (req, res, next)=>{
    res.render('admin/add-edit-product', {
        pageTitle: 'Add Product', 
        path: '/admin/add-product',
        editMode: false,
        hasError: false,
        errorMessage: null,
        validationErrors: []
    });
};

module.exports.postAddProduct = (req, res, next)=>{
    const title = req.body.title;
    const image = req.file;
    const price = req.body.price;
    const description = req.body.description;

    // console.log(image);

    if(!image){
        return res.status(422).render('admin/add-edit-product', {
            pageTitle: 'Add Product', 
            path: '/admin/add-product',
            editMode: false,
            hasError: true,
            errorMessage: 'Attached file is not an image or there is no attached file.',
            product: {
                title: title,
                price: price,
                description: description
            },
            validationErrors: []
        })
    }

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).render('admin/add-edit-product', {
                                        pageTitle: 'Add Product', 
                                        path: '/admin/add-product',
                                        editMode: false,
                                        hasError: true,
                                        errorMessage: errors.array()[0].msg,
                                        product: {
                                            title: title,
                                            price: price,
                                            description: description
                                        },
                                        validationErrors: errors.array()
                                    })
    }

    const product = new Product({
        // namerno napravljena greska radi ulaska u catch blok
        // _id: mongoose.Types.ObjectId('5d4ad9ce70f6ad4bacce90ad'),
        title: title, 
        price: price, 
        description: description, 
        imageUrl: fileHelper.makeRelativeImagePath(image.path),
        /**
         * userId: req.user._id ili userId: req.user (monogoose ce iz objekta req.user izvuci id)
         */
        userId: req.user
    });

    product.save()
        .then(result=>{
            console.log('CREATED PRODUCT!');
            res.redirect('/admin/products');
        })
        .catch(err => {
            // console.log('An error occured!');
            // ukoliko imamo tehnicki problem(npr. server baze je srusen) status je 500
            // return res.status(500).render('admin/add-edit-product', {
            //     pageTitle: 'Add Product', 
            //     path: '/admin/add-product',
            //     editMode: false,
            //     hasError: true,
            //     errorMessage: 'Database operation failed, please try again!',
            //     product: {
            //         title: title,
            //         imageUrl: imageUrl,
            //         price: price,
            //         description: description
            //     },
            //     validationErrors: []
            // })
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        }); 
};

module.exports.getEditProduct = (req, res, next)=>{
    const editMode = req.query.edit;
    if(!editMode){
        res.redirect('/');
    }
    const productId = req.params.productId;
    Product.findById(productId)
        .then(product => {
            // za simuliranje greske
            // throw new Error('Dummy error.');

            if(!product){
                res.redirect('/');
            }
            res.render('admin/add-edit-product', {
                pageTitle: 'Edit Product', 
                path: '/admin/edit-product',
                editMode: true,
                product: product,
                errorMessage: null,
                validationErrors: []              
            });
        })
        .catch(err => {
            // console.log(err);
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.postEditProduct = (req, res, next)=>{
    const image = req.file;

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).render('admin/add-edit-product', {                                            
                                            pageTitle: 'Edit Product', 
                                            path: '/admin/edit-product',
                                            editMode: true,
                                            product: new Product({
                                                title: req.body.title, 
                                                price: req.body.price, 
                                                description: req.body.description,
                                                _id: req.body.id
                                            }),
                                            errorMessage: errors.array()[0].msg,
                                            validationErrors: errors.array()
                                        })
    }

    Product.findById(req.body.id)
        .then(product => {
            if(product.userId.toString() !== req.user._id.toString()){
                return res.redirect('/');
            }
            product.title = req.body.title;
            product.price = req.body.price;
            product.description = req.body.description;
            if (image){
                fileHelper.deleteFile(product.imageUrl);
                product.imageUrl = fileHelper.makeRelativeImagePath(image.path);
            }
            return product.save()
                .then(result=>{
                    console.log('UPDATED PRODUCT!');
                    res.redirect('/admin/products');
                })
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

module.exports.getProducts = (req, res, next)=>{
    Product.find({userId: req.user._id})
        //.select('title price -_id') // mozes reci koje atribute zelis da dobavis, sa minusom ispre kazes koje ne zelis da dobavis
        .populate('userId') // na mestu gde je referenca(id) moze dobaviti sve atribute za taj dokument, ili navesti koje atribute tog dokumenta zelis
        // npr. .populate('userId', 'username') za samo _id(default) i username
        .then(products=>{
            // console.log(products);
            res.render('admin/products', {
                products: products, 
                pageTitle: 'Admin Products',
                path: '/admin/products'
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

// module.exports.postDeleteProduct = (req, res, next)=>{
//     Product.findById(req.body.id)
//         .then(product => {
//             if(!product){
//                 return next(new Error('Product not found.'));
//             }
//             fileHelper.deleteFile(product.imageUrl);
//             return Product.deleteOne({_id: req.body.id, userId: req.user._id});
//         })
//         .then(result => {
//             console.log('PRODUCT DELETED!');         
//             res.redirect('/admin/products');
//         })
//         .catch(err=>{
//             const error = new Error(err);
//             error.httpStatusCode = 500;
//             return next(error);
//         });
// }

module.exports.deleteProduct = (req, res, next)=>{
    const productId = req.params.productId;
    Product.findById(productId)
        .then(product => {
            if(!product){
                return next(new Error('Product not found.'));
            }
            fileHelper.deleteFile(product.imageUrl);
            return Product.deleteOne({_id: productId, userId: req.user._id});
        })
        .then(result => {
            console.log('PRODUCT DELETED!');         
            res.status(200).json({message: 'Success!'});
        })
        .catch(err=>{
            res.status(500).json({message: 'Deleting product failed.'});
        });
}