const crypto = require('crypto');

const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const { validationResult } = require('express-validator/check');

const User = require('../models/user');

const SENDGRID_API_KEY = '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';

const transporter = nodemailer.createTransport(sendgridTransport({
    auth: {
        api_key: SENDGRID_API_KEY
    }
}));

module.exports.getLogin = (req, res, next) => {
    // console.log(req.session);
    // console.log(req.session.isLoggedIn);

    // const isLoggedIn = req.get('Cookie').split(';')[0].trim().split('=')[1] === 'true';

    let message = req.flash('error');
    if(message.length > 0){
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/login', {
        pageTitle: 'Login',
        path: '/login'  ,
        errorMessage: message,
        oldInput: {
            email: '',
            password: ''
        },
        validationErrors: []
    })
}

module.exports.postLogin = (req, res, next) => {
    /**
     * nakon sto posaljemo response request je gotov, 'mrtav' tako da i informacija req.isLoggedIn = true se gubi
     * 
     * req.isLoggedIn = true;
     */

    /**
     * postavljanje cookie-a
     * 
     * res.setHeader('Set-Cookie', 'loggedIn=true; HttpOnly');
     */
    const email = req.body.email;
    const password = req.body.password;

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        // status 422 je za neispravnu validaciju
        return res.status(422).render('auth/login', {
                                        pageTitle: 'Login',
                                        path: '/login'  ,
                                        errorMessage: errors.array()[0].msg,
                                        oldInput: {
                                            email: email,
                                            password: password
                                        },
                                        validationErrors: errors.array()
                                    })
    }

    User.findOne({email: email})
        .then(user=>{
            if(!user){
                // req.flash('error', 'Invalid password or email.');
                // return res.redirect('/login');
                return res.render('auth/login', {
                    pageTitle: 'Login',
                    path: '/login'  ,
                    errorMessage: 'Invalid password or email.',
                    oldInput: {
                        email: email,
                        password: password
                    },
                    validationErrors: []
                })
            } else {
                bcrypt.compare(password, user.password)
                    .then(doMatch => {
                        if(!doMatch){
                            // req.flash('error', 'Invalid password or email.');
                            // return res.redirect('/login');
                            return res.render('auth/login', {
                                pageTitle: 'Login',
                                path: '/login'  ,
                                errorMessage: 'Invalid password or email.',
                                oldInput: {
                                    email: email,
                                    password: password
                                },
                                validationErrors: []
                            })
                        } else {
                            /**
                             * postavice session(postoji dok ne zatvorimo browser) cookie po nazivu connect.sid,
                             * cija ce vrednost biti enkriptovana(docice do premene u sesiji i zbog toga do pamcenja u bazi)
                             * uz pomoc njega ce server moci da shvati o kojem user-u je rec tj. koji browser 
                             * je u pitanju i koji user(instanca web sajta) tj.
                             * spojice 'user-a na browser-u' sa sesijom u bazi
                             * 
                             * BITNO!
                             * ukoliko obrisemo cookie na browseru, bilo gde u kodu gde pristupamo sesiji pa potom preko nje
                             * user-u on nece biti dostupan, jer ni sesija nece biti dostupna, nije pronadjena tj identifikavana zbog obrisanog cookie-a
                             */
                            req.session.isLoggedIn = true;
                            req.session.user = user;

                            /**
                             * ovde moze doci do problema ukoliko ne uradimo req.session.save(...), vec samo: res.redirect('/');
                             * posto ovde dolazi do upisa sesije u bazu moguce je da dodje do redirecta tj da se izvrsi pre nego sto se
                             * sesija upise u bazu!
                             * 
                             * DAKLE -> kad god ti je bitno da se nesto uradi nakon sto se sesija upise u bazu 
                             * pozovi f-ju save() nad njom kao sto je ovde i uradjeno
                             */
                            req.session.save((err) => {
                                res.redirect('/');
                            });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        // res.redirect('/login');
                        return res.render('auth/login', {
                            pageTitle: 'Login',
                            path: '/login'  ,
                            errorMessage: 'Invalid password or email.',
                            oldInput: {
                                email: email,
                                password: password
                            },
                            validationErrors: []
                        })
                    })
            }
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        }); 
}

module.exports.postLogout = (req, res, next) => {
    // req.session.destroy -> obrisace sesiju i req.session vise nece biti dostupan
    /**
     * req.session.destroy -> obrisace sesiju(tacnije neka njena dokumenta(npr. user, isLoggedIn)) i req.session vise 
     * nece biti dostupan, inace session cookie ce ostati ali on nije problem, ukoliko zatvorimo tab on ce biti obrisan,
     * a ukoliko i ne uradimo to sve jedno nece moci da se spoji sa sesijom buduci da je ona obrisana
     */
    req.session.destroy((err) => {
        console.log(err);
        res.redirect('/');
    });
}

module.exports.getSignup = (req, res, next) => {
    let message = req.flash('error');
    if(message.length > 0){
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/signup', {
        pageTitle: 'Sign Up',
        path: '/signup',
        errorMessage: message,
        oldInput: {
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationErrors: []
    })
}

module.exports.postSignup = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    /**
     * kupimo greske(rezultat) koje je postavio: expressValidator.check('email').isEmail() na ruti:
     * router.post('/signup', expressValidator.check('email').isEmail(), authController.postSignup);
     */
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        console.log(errors.array());
        // status 422 je za neuspesnu validaciju
        return res.status(422).render('auth/signup', {
                                        pageTitle: 'Sign Up',
                                        path: '/signup',
                                        errorMessage: errors.array()[0].msg,
                                        oldInput: {
                                            email: email,
                                            password: password,
                                            confirmPassword: req.body.confirmPassword
                                        },
                                        validationErrors: errors.array()
                                    })
    }

    bcrypt.hash(password, 12)
        .then(hashedPassword => {
            const user = new User({
                email: email,
                password: hashedPassword,
                cart: {items: []}
            })
            return user.save();                       
        })
        .then(result => {
            res.redirect('/login');
            return transporter.sendMail({
                to: email,
                from: 'shop@node-complete.com',
                subject: 'Signup succeeded!',
                html: '<h1>You successfully signed up!</h1>'
            })
        })
        .catch(err => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

exports.getResetPassword = (req, res, next) => {
    let message = req.flash('error');
    if(message.length > 0){
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/reset-password', {
        pageTitle: 'Reset Password',
        path: '/reset-password',
        errorMessage: message
    })
}

exports.postResetPassword = (req, res, next) => {
    crypto.randomBytes(32, (err, buffer) => {
        if(err){
            console.log(err);
            return res.redirect('/reset-password');
        }
        const token = buffer.toString('hex'); // prosledjen je string koji kaze da treba da prevede hexadecimalnu vrednost u ascii karaktere
        User.findOne({email: req.body.email})
            .then(user => {
                if(!user){
                    req.flash('error', 'No account with that email found.');
                    return res.redirect('/reset-password');
                }
                user.resetPasswordToken = token;
                user.resetPasswordTokenExpiration = Date.now() + 3600000;
                return user.save();
            })
            .then(result => {
                res.redirect('/');
                transporter.sendMail({
                    to: req.body.email,
                    from: 'shop@node-complete.com',
                    subject: 'Reset Password',
                    html: `
                        <p>You requested a password reset</p>
                        <p>Click this <a href="http://localhost:3000/new-password/${token}">link</a> to set a new password.</p>
                    `
                });
            })
            .catch(err => {
                const error = new Error(err);
                error.httpStatusCode = 500;
                return next(error);
            })
    })
}

exports.getNewPassword = (req, res, next) => {
    const token = req.params.token;
    User.findOne({resetPasswordToken: token, resetPasswordTokenExpiration: {$gt: Date.now()}})
        .then(user => {
            // if(!user){
            //     req.flash('error', 'Link was not valid anymore.')
            //     return res.redirect('/reset-password');
            // }
            let message = req.flash('error');
            if(message.length > 0){
                message = message[0];
            } else {
                message = null;
            }
            res.render('auth/new-password', {
                pageTitle: 'New Password',
                path: '/new-password',
                errorMessage: message,
                userId: user._id.toString(),
                resetPasswordToken: token
            })
        })
        .catch(err => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        })
}

exports.postNewPassword = (req, res, next) => {
    const newPassword = req.body.password;
    const userId = req.body.userId;
    const resetPasswordToken = req.body.resetPasswordToken;
    let resetUser;

    User.findOne({resetPasswordToken: resetPasswordToken, resetPasswordTokenExpiration: {$gt: Date.now()}, _id: userId})
        .then(user => {
            // if(!user){

            // }
            resetUser = user;
            return bcrypt.hash(newPassword, 12);
        })
        .then(hashedPassword => {
            resetUser.password = hashedPassword;
            resetUser.resetPasswordToken = null;
            resetUser.resetPasswordTokenExpiration = undefined;
            return resetUser.save();
        })
        .then(result => {
            res.redirect('/login');
        })
        .catch(err => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        })
}