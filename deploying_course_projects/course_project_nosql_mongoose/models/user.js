const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    resetPasswordToken: String,
    resetPasswordTokenExpiration: Date,
    cart: {
        items: [{
            productId: {
                type: mongoose.Types.ObjectId,
                ref: 'Product',
                required: true
            },
            quantity: {
                type: Number,
                required: true
            }
        }]
    }
})

userSchema.methods.addToCart = function(product) {
    const cartProductIndex = this.cart.items.findIndex(cartProduct=>{            
        return cartProduct.productId.toString() === product._id.toString();
    });
    let newQuantity = 1;
    const updatedCartItems = [...this.cart.items];

    if(cartProductIndex >= 0){
        newQuantity = updatedCartItems[cartProductIndex].quantity + 1;
        updatedCartItems[cartProductIndex].quantity = newQuantity;
    } else {
        updatedCartItems.push({
            productId: product._id,
            quantity: newQuantity
        });
    }

    const updatedCart = {
        items: updatedCartItems
    };
    this.cart = updatedCart;

    return this.save();
}

userSchema.methods.deleteItemFromCart = function(itemId) {
    const updatedItems = [...this.cart.items.filter(i=>i._id.toString() !== itemId.toString())];
    this.cart.items = updatedItems;
    return this.save();
}

userSchema.methods.clearCart = function() {
    this.cart = {items: []};
    return this.save();
}

module.exports = mongoose.model('User', userSchema);

// const mongodb = require('mongodb');
// const getDb = require('../util/database').getDb;

// class User{
//     constructor(username, email, id, cart){
//         this.username = username;
//         this.email = email;
//         this._id = id ? new mongodb.ObjectId(id) : null;
//         this.cart = cart; // {items: []};
//     }

//     save(){
//         const db = getDb();
//         return db.collection('users').insertOne(this)
//             .then(result=>{
//                 console.log('USER CREATED!');
//             })
//             .catch(err=>console.log(err));
//     }

//     addToCart(product){
//         const cartProductIndex = this.cart.items.findIndex(cartProduct=>{            
//             return cartProduct.productId.toString() === product._id.toString();
//         });
//         let newQuantity = 1;
//         const updatedCartItems = [...this.cart.items];

//         if(cartProductIndex >= 0){
//             newQuantity = updatedCartItems[cartProductIndex].quantity + 1;
//             updatedCartItems[cartProductIndex].quantity = newQuantity;
//         } else {
//             updatedCartItems.push({
//                 productId: new mongodb.ObjectId(product._id),
//                 quantity: newQuantity
//             });
//         }

//         const updatedCart = {
//             items: updatedCartItems
//         };

//         const db = getDb();
//         return db.collection('users').updateOne({_id: new mongodb.ObjectId(this._id)}, 
//                                                 {$set: {cart: updatedCart}})
//             .then(result=>{
//                 console.log('UPDATED!');
//                 return result;
//             })
//             .catch(err=>console.log(err));
//     }

//     deleteItemFromCart(prodId){
//         const updatedCart = [...this.cart.items.filter(i=>i.productId.toString() !== prodId.toString())]
//         const db = getDb();
//         return db.collection('users').updateOne({_id: new mongodb.ObjectId(this._id)}, 
//                                                 {$set: {cart: {items: updatedCart}}});
//     }

//     addOrder(){
//         const db = getDb();
//         return this.getCart()
//             .then(products=>{
//                 const orders = {
//                     items: products,
//                     user: {
//                         _id: new mongodb.ObjectId(this._id),
//                         username: this.username
//                     }
//                 }
//                 return db.collection('orders').insertOne(orders);
//             })
//             .then(result=>{
//                 return db.collection('users').updateOne({_id: new mongodb.ObjectId(this._id)},
//                                                 {$set: {cart: {items: []}}});
//             })
//             .catch(err=>console.log(err));
//     }

//     getOrders(){
//         const db = getDb();
//         return db.collection('orders').find({'user._id': new mongodb.ObjectId(this._id)}).toArray()
//             .then(orders=>{
//                 return orders;
//             })
//             .catch(err=>console.log(err));
//     }

//     getCart(){
//         const db = getDb();
//         const productIds = this.cart.items.map(i=>{
//             return i.productId;
//         });
//         return db.collection('products').find({_id: {$in: productIds}}).toArray()
//             .then(products=>{
//                 if(products.length !== productIds.length){
//                     const deletedProductsId = [];
//                     productIds.forEach(prodId=>{
//                         let exists = false;
//                         products.forEach(productInCart=>{
//                             if(prodId.toString() === productInCart._id.toString()){
//                                 exists = true;
//                             }
//                         })
//                         if(!exists){
//                             deletedProductsId.push(prodId);
//                         }
//                     });

//                     const updatedCartItems = [...this.cart.items.filter(i=>{
//                         if(!deletedProductsId.includes(i.productId)){
//                             return i;
//                         }
//                     })];
//                     this.cart = {items: updatedCartItems};

//                     return db.collection('users').updateOne({_id: new mongodb.ObjectId(this._id)}, 
//                                                 {$set: {cart: {items: updatedCartItems}}})
//                                 .then(result=>{
//                                     return products.map(p=>{
//                                         return {
//                                             ...p, 
//                                             quantity: this.cart.items.find(i=>{
//                                                     return i.productId.toString() === p._id.toString();
//                                                 }).quantity
//                                         };
//                                     })
//                                 })
//                                 .catch(err=>console.log(err));
                    
//                 }

//                 return products.map(p=>{
//                     return {
//                         ...p, 
//                         quantity: this.cart.items.find(i=>{
//                                 return i.productId.toString() === p._id.toString();
//                             }).quantity
//                     };
//                 })
//             })
//             .catch(err=>console.log(err));
//     }

//     static findById(userId){
//         const db = getDb();
//         /**
//          * moze i ovako: 
//          * return db.collection('users').findOne({_id: new mongodb.ObjectId(userId)}).then(...).catch(...)
//          * ne zovemo na kraju next(), jer nam ne vraca kursor objekat, jer smo zvali findOne()
//          */
//         return db.collection('users').find({_id: new mongodb.ObjectId(userId)}).next()
//             .then(user=>{
//                 return user;
//             })
//             .catch(err=>console.log(err));
//     }
// }

// module.exports = User;