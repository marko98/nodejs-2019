const fs = require('fs');

const rootDir = require('../util/path');

const deleteFile = (relativeFilePath) => {
    const fullFilePath = rootDir + relativeFilePath.replace('/', '\\');
    fs.unlink(fullFilePath, (err) => {
        console.log(fullFilePath);
        if(err) {
            throw(err);
        }
    })
}

// const makeRelativeImagePath = (absoluteImagePath, cb) => {
//     absoluteImagePath = absoluteImagePath.replace(rootDir, '');
//     absoluteImagePath = absoluteImagePath.replace('\\', '/');
//     cb(absoluteImagePath);
// }

const makeRelativeImagePath = (absoluteImagePath) => {
    absoluteImagePath = absoluteImagePath.replace(rootDir, '');
    absoluteImagePath = absoluteImagePath.replace('\\', '/');
    return absoluteImagePath;
}

module.exports.deleteFile = deleteFile;
module.exports.makeRelativeImagePath = makeRelativeImagePath;