const path = require('path');
const fs = require('fs');
const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
/**
 * require('connect-mongodb-session') nam daje f-ju koja prima parametar i vraca konstruktor
 */
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');

const User = require('./models/user');
const rootDir = require('./util/path');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');
const errorController = require('./controllers/error');
const shopController = require('./controllers/shop');
const isAuth = require('./middleware/is-auth');

console.log(process.env.NODE_ENV);
const MONGODB_URI = `mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0-mekhr.mongodb.net/${process.env.MONGODB_DEFAULT_DATABASE}?retryWrites=true&w=majority`;
// Max-u nesto nije radilo pa je napisao ovako: const MONGODB_URI = 'mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/shop';

const app = express();
// -- inicijalizacije --
const store = new MongoDBStore({
    uri: MONGODB_URI,
    // definisemo naziv kolekcije gde ce nam sesija biti cuvana
    collection: 'sessions'
});
const csrfProtection = csrf();

// const privateKey = fs.readFileSync('server.key');
// const certificate = fs.readFileSync('server.cert');

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg'){
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const fileStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        // nece da radi ukoliko destinacija(folder) vec ne postoji
        let uploadPath = path.join(__dirname, 'images');
        if(!fs.existsSync(uploadPath)){
            fs.mkdirSync(uploadPath);
        }
        cb(null, uploadPath);
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString().replace(/[-T:\.Z]/g, "") + '-' + file.originalname);
    }
})
// ---------------------

app.set('view engine', 'ejs');
app.set('views', 'views');

// {flags: 'a'} -> da svaki put doda novu liniju
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});

app.use(helmet());
app.use(compression());
app.use(morgan('combined', {stream: accessLogStream}));
/**
 * bodyParser.urlencoded({extended: false}), ovaj deo: urlencoded nam govori
 * da moze da parsira samo text, plain text koji dobija iz forme, a ne i fajlove
 */
app.use(bodyParser.urlencoded({extended: false}));
/**
 * sa dest: files 'govorimo' multer-u da sacuva(binarno) fajl u koji folder,
 * u nasem slucaju folder files
 * 
 * ukoliko bi na sacuvane fajlove dodali extenziju, koju nam isto multer moze reci,
 * ukoliko uradimo console.log(...), mozemo otvoriti te fajlove
 * 
 * single('image') -> znaci da trazi u formama koje imaju enctype="multipart/form-data",
 * jedno polje koje ima atribut name='image'
 */
app.use(
    multer({
        fileFilter: fileFilter,
        // dest: 'images',
        storage: fileStorage, 
    }).single('image')
);
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(__dirname, 'images')));
/**
 * secret: 'my secret' bi u produkciji trebao da bude dugacak string
 * 
 * resave: false -> sesija ce se sacuvati samo kada dodje do izmene neke(ovo poboljsava performanse)
 * 
 * saveUninitialized: false -> radi slicno sto i resave: false
 * 
 * ovaj middlewear ce dodati req.session kao i cookie(pogledaj auth controller) koji ako sami ne podesimo 
 * ima neke default-ne vrednosti, ali to se dogadja tek kada dodje do neke promene u sesiji jer je tako podesena
 * 
 * store: store -> definisemo gde zelimo da cuvamo sesiju
 */
app.use(
    session({
        secret: 'my secret', 
        resave: false, 
        saveUninitialized: false, 
        store: store
    })
);
// ovaj middlewear ce nam pruziti: req.flash()
app.use(flash()); //mora biti registrovano posle session registracije

app.get('/favicon.ico', (req, res, next)=>{
    res.send();
});

app.use((req, res, next) => {
    /**
     * res.locals nam omogucava da ukoliko imamo neke atribute koje saljemo svim ili vecini views-ima pri renderovanju
     * mozemo ih ovde podesiti, na jednom mestu za sve
     */
    res.locals.isAuthenticated = req.session.isLoggedIn;
    next();
})


/**
 * ovaj middlewear ce se uvek, pri bilo kom zahtevu izvrsiti i mi cemo za request zakaciti novu vrednost,
 * pod kljucem user -> (req.user = user)
 * potom pozivamo f-ju next() koja nam omogucava da se ide dalje u neki drugi middlewear gde cemo 
 * poslati response
 * u bilo kom od sledecih middlewear-a mocicemo da pristupimo user-u
 */ 
app.use((req, res, next)=>{
    /**
     * za sinhroni kod dovoljno je 'baciti' gresku
     * da bi express pozvao middlewear za greske koji smo napisali,
     * dok je za asinhroni kod drugacije(procitaj.txt, linija 185)
     */
    // throw new Error('Dummy error.');

    if(!req.session.user){
        return next();
    }
    User.findById(req.session.user._id)
    .then(user=>{
        // throw new Error('Dummy error.');
        if(!user){
            next();
        }
        req.user = user;
        next();
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
        // console.log(err);
    });
});

app.post('/create-order', isAuth, shopController.postCreateOrder);

// ovaj middlewear ce nam pruziti: req.csrfToken()
app.use(csrfProtection); //mora biti registrovano posle session registracije
app.use((req, res, next) => {
    /**
     * res.locals nam omogucava da ukoliko imamo neke atribute koje saljemo svim ili vecini views-ima pri renderovanju
     * mozemo ih ovde podesiti, na jednom mestu za sve
     */
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.get('/500', errorController.get500);

app.use(errorController.get404);

/**
 * ovaj middlewear je poseban i sluzi za obradu gresaka, 
 * npr. return next(error); -> tada se on poziva
 * 
 * ukoliko imamo vise middlewear-a za obradu gresaka,
 * oni ce se izvrsavati od gore ka dole kao sto je slucaj i sa ostalima
 */
app.use((error, req, res, next) => {
    // res.status(error.httpStatusCode).render(...)
    // res.redirect('/500');

    res.status(500).render('500', {
        pageTitle: 'Error!', 
        path: '/500',
    });
})

mongoose.connect(MONGODB_URI)
    .then(result=>{
        // https
        // .createServer({key: privateKey, cert: certificate}, app)
        // .listen(process.env.PORT || 3000);
        app.listen(process.env.PORT || 3000);
    })
    .catch(err => {
        console.log(err);
    })