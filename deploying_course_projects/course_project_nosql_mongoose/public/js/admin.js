const deleteProduct = (btn) => {
    const prodId = btn.parentNode.querySelector('[name=id]').value;
    const csrf = btn.parentNode.querySelector('[name=_csrf]').value;

    /**
     * element.closest('...') -> nalazi najblizi element koji je roditelj(nisam bas siguran)
     * elementu nad kojim se metoda poziva i ciji tag odgovara prosledjenom parametru
     */
    // ovaj element sadrzi sve podatke i elemente koji predstavljaju jedan product
    const article = btn.closest('article');

    fetch('/admin/product/' + prodId, {
        method: "DELETE",
        headers: {
            'csrf-token': csrf
        }
    })
    .then(result => {
        // buduci da nam kontroler salje json, treba da ga pretvorimo u objekat, da ne bude samo string
        return result.json();
    })
    .then(data => {
        console.log(data);
        article.parentNode.removeChild(article);
    })
    .catch(err => {
        console.log(err);
    })
}