-----------------------------------------------------------------------------------------------------------------------
1) Deploying

1.1) Environment variables
- vrednosti poput nekih kljuceva za koriscenje nekih servisa, username baze podataka, sifre itd umesto da budu hard coded
treba 'zavesti' kao varijable okruzenja i te varijable koristiti

- ukoliko koristimo nodemon pri pokretanju aplikacije, ovako se kod njega konfigurisu varijable okruzenja(nodemon.json):
{
    "env": {
        "MONGODB_USER": "marko",
        "MONGODB_PASSWORD": "marko",
        "MONGODB_DEFAULT_DATABASE": "shop",
        // obicno se port nudi od strane koja pruza deployment i naziva PORT
        // "PORT": 3000,
        "STRIPE_KEY": "sk_test_hML0AqkBSBFDT9W3Zg3AzHTL00QKOy8TVs"
    }
}

u app.js, linija 24 primer dodele vrednosti varijabli okruzenja:
const MONGODB_URI = `mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0-mekhr.mongodb.net/${process.env.MONGODB_DEFAULT_DATABASE}?retryWrites=true&w=majority`;

* BITNO *
string je literal `` i to nam omogucava da ga pisemo u vise redova i da umecemo sa ${} vrednosti

u produkciji necemo koristiti nodemon jer nam ne treba restartovanje server na svaku izmenu, 
mada nje nece ni biti, tako da environment variables moramo drugacije podesiti
izmene:
    package.json:
    {
        "name": "course_project",
        "version": "1.0.0",
        "description": "",
        "main": "app.js",
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1",
            /**
             * ova linija je promenjena sa "start": "nodemon app.js",
             * varijable okruzenja se tu beleze, posto ne koristimo vise nodemon za pokretanje
             *
             * environmental variable: NODE_ENV=production uglavnom postavljaju hosting provajderi
             */
            // za linux i mac:
            "start": "NODE_ENV=production MONGODB_USER=marko MONGODB_PASSWORD=marko MONGODB_DEFAULT_DATABASE=shop STRIPE_KEY=sk_test_hML0AqkBSBFDT9W3Zg3AzHTL00QKOy8TVs node app.js", // poziva se sa: npm start ili node app.js
            // za windows:
            "start": "SET NODE_ENV=production&SET MONGODB_USER=marko& SET MONGODB_PASSWORD=marko& SET MONGODB_DEFAULT_DATABASE=shop& SET STRIPE_KEY=sk_test_hML0AqkBSBFDT9W3Zg3AzHTL00QKOy8TVs& node app.js", // poziva se sa: npm start ili node app.js
            "start:dev": "nodemon app.js" // dodali smo ovu liniju koda, poziva se sa: npm run start:dev
        },
        "author": "Marko Zahorodni",
        "license": "ISC",
        "devDependencies": {
            "mongodb": "^3.2.7",
            "nodemon": "^1.19.1",
            "sequelize": "^5.12.3"
        },
        "dependencies": {
            "@sendgrid/mail": "^6.4.0",
            "@types/express": "^4.17.0",
            "@types/node": "^12.6.9",
            "bcryptjs": "^2.4.3",
            "body-parser": "^1.19.0",
            "connect-flash": "^0.1.1",
            "connect-mongodb-session": "^2.2.0",
            "csurf": "^1.10.0",
            "ejs": "^2.6.2",
            "express": "^4.17.1",
            "express-session": "^1.16.2",
            "express-validator": "^6.1.1",
            "mongoose": "^5.6.8",
            "multer": "^1.4.2",
            "mysql2": "^1.6.5",
            "nodemailer": "^6.3.0",
            "nodemailer-sendgrid-transport": "^0.2.0",
            "pdfkit": "^0.10.0",
            "stripe": "^7.7.0"
        }
    }

1.2) Secure Response Headers with Helmet
npm install --save helmet

ovaj paket nas stiti od jos nekih mogucih napada tako sto stavi na svaki response neke od 'odbrambenih' header-a

1.3) Compressing Assets
npm install --save compression

ovo obicno hosting provajderi pruzaju tako da u tom slucaju ne treba da radimo kompresiju, ali ako ne pruzaju
onda mozemo mi ovo da uradimo

1.3) Request Logging
npm install --save morgan

ovo obicno hosting provajderi pruzaju

More on Logging
Besides using morgan to log requests in general, you can also add your own log messages in your code.
For one, you can of course use the good old console.log() command to write logs.
For a more advanced/ detailed approach on logging (with higher control), see this article: 
https://blog.risingstack.com/node-js-logging-tutorial/

1.4) Setting Up a SSL Server

SslTlsProtection2019
kupio neki PositiveSSL na https://www.ssls.com/
detaljnije:
na email-u naziv mail-a: SSLs.com: Order has been completed


https://slproweb.com/products/Win32OpenSSL.html
za instaliranje OpenSSL-a, exe:
https://slproweb.com/download/Win64OpenSSL-1_1_1c.exe

pravimo svoj kljuc i sertifikat
izvrsiti komandu da bi dobio sertifikat(salje se klijentu) i private key(uvek ostaje na serveru):
openssl req -nodes -new -x509 -keyout server.key -out server.cert

1.5) Hosting Provider (456. A Deployment Example with Heroku)
Heroku
ne pruza:
    compression
pruza:
    SSL/TLS


1) instaliraj heroku cli(https://devcenter.heroku.com/articles/heroku-command-line), potom izvrsi:
- heroku login
2) Create a new Git repository
- git init
- heroku git:remote -a marko98-node-complete-udemy

* BITNO *
3) u package.json dodaj:
"engines": {
    "node": "10.16.0" // verzija node-a koju koristis(mozes dobiti verziju sa komandom: node -v)
},

4) kreiraj novi fajl bez extenzije:
Procfile
- u njemu napisi web: node app.js
to ce mu reci sta da izvrsi kada pokusa da pokrene nasu aplikaciju

5) dodaj fajl .gitignore i napisi:
node_modules
server.cert
server.key

6) Deploy your application
- git add .
- git commit -m "make it better"
- git push heroku master
2) Existing Git repository
For existing repositories, simply add the heroku remote
- heroku git:remote -a marko98-node-complete-udemy

7) heroku logs -> da vidis sta nije proslo kako treba

8) dodati environmental variables ovde(NODE_ENV=production ne treba jer im je to po defaultu namesteno):
https://dashboard.heroku.com/apps/marko98-node-complete-udemy/settings

9) dodati na IP Whitelist-u, na nalogu mongodb.org za svoj cluster, staticku IP adresu za heroku, a
    buduci da on nema jednu nego koristi opseg(sto ne more biti slucaj kod ostalih hosting provajdera)
    dodaj 0.0.0.0/0 sto omogucava da se bilo ko poveze(nije bas idealno bolje koristiti jednu staticku ip adresu,
    ako je provajder daje), no svejedno za pristup bazi treba i username i sifra

10) idi na https://dashboard.heroku.com/apps/marko98-node-complete-udemy pa na more u gornjem desnom uglu, pa na 
    Restart all Dynos

ako zapnes negde: 456. A Deployment Example with Heroku

!!! BITNO !!!
Storing User-generated Files on Heroku
Here's one important note about hosting our app on Heroku!

The user-generated/ uploaded images, are saved and served as intended. 
But like all hosting providers that offer virtual servers, your file storage is not persistent!

Your source code is saved and re-deployed when you shut down the server (or when it goes to sleep, 
as it does automatically after some time in the Heroku free tier).

But your generated and uploaded files are not stored and re-created. They would be lost after a server restart!
Therefore, it's recommended that you use a different storage place when using such a hosting provider.
In cases where you run your own server, which you fully own/ manage, that does of course not apply.

What would be alternatives?
A popular and very efficient + affordable alternative is AWS S3 (Simple Storage Service): https://aws.amazon.com/s3/
You can easily configure multer to store your files there with the help of another package: https://www.npmjs.com/package/multer-s3
To also serve your files, you can use packages like s3-proxy: https://www.npmjs.com/package/s3-proxy
For deleting the files (or interacting with them on your own in general), you'd use the AWS SDK: https://aws.amazon.com/sdk-for-node-js/



Useful resources:

Herokus Docs: https://devcenter.heroku.com/categories/reference

Deploying SPAs (like our React App): https://medium.com/@baphemot/understanding-react-deployment-5a717d4378fd

Alternative Hosting Providers:

Amazon Web Services: https://aws.amazon.com/getting-started/projects/deploy-nodejs-web-app/

DigitalOcean: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04

And of course everything Google yields on "nodejs hosting"
-----------------------------------------------------------------------------------------------------------------------