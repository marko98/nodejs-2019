const { buildSchema } = require('graphql');

/**
 * Query -> Retrieve Data("GET")
 * 
 * Mutation -> Manipulate Data("POST", "PUT", "PATCH", "DELETE")
 * 
 * Subscription -> Set up realtime connection via Websockets
 */
module.exports = buildSchema(`
    type Post {
        _id: ID!
        title: String!
        content: String!
        imageUrl: String!
        creator: User!
        createdAt: String!
        updatedAt: String!
    }

    type User {
        _id: ID!
        email: String!
        name: String!
        password: String!
        status: String!
        posts: [Post!]!
        createdAt: String!
        updatedAt: String!
    }

    type AuthData {
        token: String!
        userId: String!
    }

    type GetPosts {
        message: String!
        posts: [Post!]!
        totalItems: Int!
    }

    input UserInputData {
        email: String
        name: String
        password: String
        status: String
    }

    input PostInputData {
        title: String!
        content: String!
        imageUrl: String!
    }

    type RootMutation {
        postUser(userInput: UserInputData): User!
        putUser(userInput: UserInputData): User!
        postPost(postInput: PostInputData): Post!
        putPost(postId: ID!, postInput: PostInputData): Post!
        deletePost(postId: ID!): Boolean!
    }

    type RootQuery {
        getLogin(email: String!, password: String!): AuthData!
        getPosts(page: Int): GetPosts!
        getPost(postId: ID!): Post!
        getUser: User!
    }

    schema {
        query: RootQuery
        mutation: RootMutation
    }
`);