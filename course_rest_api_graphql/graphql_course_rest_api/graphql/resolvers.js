const bcrypt = require('bcryptjs');
const validator = require('validator');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Post = require('../models/post');
const { deleteImage } = require('../util/file');

module.exports = {
    getUser(args, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return User.findById(req.userId)
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }
                return {
                    ...user._doc,
                    _id: user._id.toString(),
                    createdAt: user.createdAt.toISOString(),
                    updatedAt: user.updatedAt.toISOString()
                }
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    postUser({ userInput }, req) {
        const email = userInput.email;
        const name = userInput.name;
        const password = userInput.password;

        const errors = [];
        if(!validator.isEmail(email)){
            errors.push({message: 'Invalid email.'});
        }
        if(validator.isEmpty(password) || !validator.isLength(password, {min:5})){
            errors.push({message: 'Invalid password.'});
        }
        if(validator.isEmpty(name)){
            errors.push({message: 'Invalid name.'});
        }
        if(errors.length > 0){
            const error = new Error('Invalid data.');
            error.data = errors;
            error.statusCode = 422;
            throw error;
        }

        return User.findOne({email: email})
            .then(userDoc => {
                if(userDoc){
                    const error = new Error('User exists already!');
                    throw error;
                }
                return bcrypt.hash(password, 12);
            })
            .then(hashedPassword => {
                const user = new User({
                    email: email,
                    password: hashedPassword,
                    name: name
                });
                return user.save();
            })
            .then(user => {
                // user._doc -> mongoose ce nam dati samo podatke koje smo mi definisali u schemi, a ne i meta podatke
                return { ...user._doc, _id: user._id.toString() };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    putUser({userInput}, req){
        const status = userInput.status;

        const errors = [];
        if(validator.isEmpty(status)){
            errors.push({message: 'Invalid status.'});
        }
        if(errors.length > 0){
            const error = new Error('Invalid data.');
            error.data = errors;
            error.statusCode = 422;
            throw error;
        }

        return User.findById(req.userId)
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }
                user.status = status;
                return user.save();
            })
            .then(user => {
                return {
                    ...user._doc,
                    _id: user._id.toString(),
                    createdAt: user.createdAt.toISOString(),
                    updatedAt: user.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    postPost({ postInput }, req) {
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        const title = validator.trim(postInput.title);
        const content = validator.trim(postInput.content);

        const errors = [];
        if(validator.isEmpty(title) || !validator.isLength(title, {min: 5})){
            errors.push({message: 'Invalid title.'});
        }
        if(validator.isEmpty(content) || !validator.isLength(content, {min: 5})){
            errors.push({message: 'Invalid content.'});
        }

        if(errors.length > 0){
            const error = new Error('Invalid data.');
            error.data = errors;
            error.statusCode = 422;
            throw error;
        }

        let creator;
        let createdPost;
        return User.findById(req.userId)
            .then(user => {
                if(!user){
                    const error = new Error('Invalid user.');
                    error.statusCode = 401;
                    throw error;
                }
                creator = user;
                const post = new Post({
                    title: title, 
                    content: content,
                    imageUrl: postInput.imageUrl,
                    creator: creator
                });
                return post.save();
            })
            .then(post => {
                createdPost = post;
                // console.log(post);
                creator.posts.push(post);
                return creator.save();
            })
            .then(user => {
                return {
                    ...createdPost._doc, 
                    _id: createdPost._id.toString(), 
                    createdAt: createdPost.createdAt.toISOString(),
                    updatedAt: createdPost.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    putPost({postId, postInput}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return Post.findById(postId)
            .populate('creator')
            .then(post => {
                if(!post){
                    const error = new Error('No post found.');
                    error.statusCode = 404;
                    throw error;
                }
                if(req.userId.toString() !== post.creator._id.toString()){
                    const error = new Error('Not authorized.');
                    error.statusCode = 403;
                    throw error;
                }

                const title = validator.trim(postInput.title);
                const content = validator.trim(postInput.content);

                const errors = [];
                if(validator.isEmpty(title) || !validator.isLength(title, {min: 5})){
                    errors.push({message: 'Invalid title.'});
                }
                if(validator.isEmpty(content) || !validator.isLength(content, {min: 5})){
                    errors.push({message: 'Invalid content.'});
                }

                if(errors.length > 0){
                    const error = new Error('Invalid data.');
                    error.data = errors;
                    error.statusCode = 422;
                    throw error;
                }

                if(postInput.imageUrl !== 'undefined'){
                    post.imageUrl = postInput.imageUrl;
                }
                
                post.title = title;
                post.content = content;
                return post.save();
            })
            .then(post => {
                return {
                    ...post._doc,
                    _id: post._id.toString(),
                    createdAt: post.createdAt.toISOString(),
                    updatedAt: post.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    deletePost({postId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return Post.findById(postId)
            .populate('creator')
            .then(post => {
                if(!post){
                    const error = new Error('No post found.');
                    error.statusCode = 404;
                    throw error;
                }
                if(req.userId.toString() !== post.creator._id.toString()){
                    const error = new Error('Not authorized.');
                    error.statusCode = 403;
                    throw error;
                }

                deleteImage(post.imageUrl);
                return Post.findByIdAndRemove(postId);
            })
            .then(result => {
                // console.log(result);
                return User.findById(req.userId);
            })
            .then(user => {
                user.posts.pull(postId);
                return user.save();
            })
            .then(user => {
                return true;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw(err);
            })
    },

    getLogin({email, password}, req) {
        let loadedUser;        
        return User.findOne({email: email})
            .then(user => {
                if(!user){
                    const error = new Error('A user with this email could not be found.');
                    // 401 status code for not authenticated
                    error.statusCode = 401;
                    throw error;
                }
                loadedUser = user;
                return bcrypt.compare(password, user.password);
            })
            .then(isEqual => {
                if(!isEqual){
                    const error = new Error('Wrong password.');
                    // 401 status code for not authenticated
                    error.statusCode = 401;
                    throw error;
                }
                const token = jwt.sign(
                        {
                            email: loadedUser.email, 
                            userId: loadedUser._id.toString()
                        }, 
                        'somesupersecretsecret', 
                        {expiresIn: '1h'}
                    );
                return {token: token, userId: loadedUser._id.toString()};
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw(err);
            })
    },

    getPosts: async function({page}, req) {
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        if(!page){
            page = 1;
        }
        const perPage = 2;
        // console.log(page);
        try{
            const totalItems = await Post.find().countDocuments();
            const posts = await Post.find()
                    .populate('creator')
                    /**
                     * sortiranje po opadajucem redosledu, od najskorije kreiranog posta
                     * -1 -> desc
                     */
                    .sort({createdAt: -1})
                    .skip((page - 1) * perPage)
                    .limit(perPage);            
            return {
                message: 'Fetched posts successfully.', 
                posts: posts.map(p => {
                    return {
                        ...p._doc,
                        _id: p._id.toString(), 
                        createdAt: p.createdAt.toISOString(),
                        updatedAt: p.updatedAt.toISOString()
                    };
                }), 
                totalItems: totalItems
            };
        } catch(err) {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        }
    },

    getPost({postId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        return Post.findById(postId)
            .populate('creator')
            .then(post => {
                if(!post){
                    const error = new Error('No post found.');
                    error.statusCode = 404;
                    throw error;
                }
                return {
                    ...post._doc,
                    _id: post._id.toString(),
                    createdAt: post.createdAt.toISOString(),
                    updatedAt: post.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    }
}

