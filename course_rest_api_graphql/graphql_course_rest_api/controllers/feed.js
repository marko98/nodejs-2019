const { deleteImage } = require('../util/file');

module.exports.putPostImage = (req, res, next) => {
    if(!req.isAuth){
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }
    if(!req.file){
        return res.status(200).json({message: 'No file provided!'});
    }
    if(req.body.oldPath){
        deleteImage(req.body.oldPath);
    }
    return res.status(200).json({
        message: 'File stored.',
        filePath: req.file.path.replace("\\" ,"/")
    });
}