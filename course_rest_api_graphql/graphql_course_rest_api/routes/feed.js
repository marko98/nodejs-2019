const express = require('express');

const feedController = require('../controllers/feed');

const router = express.Router();

// PUT -> /feed/post-image
router.put('/post-image', feedController.putPostImage);

module.exports = router;