---------------------------------------------------------------------------------------------------
1) CORS Error
Cross-Origin-Resource-Sharing

nastaje ukoliko se serverska i klijentska strana 'servira' na razlicitim domenima, tj. njihovi
serveri(server koji servira front-end i server koji servira serversku stranu za obradu informacija)
'trce' na razlicitim domenima, samim tim i adresama

npr. 
Client -> localhost:4000
Server -> localhost:3000

po defaultu to browseri ne dozvoljavaju i zbog toga se javlja taj error
resenje: na response dodati header-e koji 'govore' browseru da mi to dozvoljavamo
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
2) video 378 -> za windows korisnike(vezano za multer-a)
npm install --save uuid
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
3) JWT(JSON Web Token)
npm install --save jsonwebtoken

JSON Data + Signature(moze biti verifikovan samo od server-side servera uz pomoc 
tajnog kljuca(secret key)) = JWT

na sajtu jwt.io mozes da uneses sa leve strane(Encoded ...) token koji si dobio na frontendu i
da dobijes payload, sa desne strane, koji je server-side server poslao
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
4) Async Await in Node.js

Useful resources:

Async-await - More Details: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
5) Websockets & Socket.io - Nije radjeno sa graphql-om
za server-side server:
npm install --save socket.io
za client-side server: 
npm install --save socket.io-client

Websocket je protokol isto kao i http, ali je nadogradjen na http protokolu, a 
socket.io(3rd party package) koristi websocket protokol

dakle websocket nam omogucava da posaljemo podatke svim konentovanim klijentima nakon kreiranja, 
izmene, brisanja odredjenih podataka

Useful resources:

Socket.io Official Docs: https://socket.io/get-started/chat/

Alternative Websocket Library: https://www.npmjs.com/package/express-ws
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
6) GraphQL i malo o validaciji
npm install graphql express-graphql

Query -> Retrieve Data("GET")
Mutation -> Manipulate Data("POST", "PUT", "PATCH", "DELETE")
Subscription -> Set up realtime connection via Websockets

* BITNO
za svaki request koristis method "POST", jer saljes body u kojem definises parametre 
potrebne za funkcionisanje GraphQL-a, pa cak i kada dobavljas podatke("GET" metod) koristis "POST" da
bi podesio Query polje u schema-i


GraphQL ne dozvoljava requestovima koji nisu metode 'POST' da prodju kroz njegov middlewear,
buduci da browser pre nego sto posalje request metode 'POST' ili 'PUT' salje request metode 'OPTIONS', da bi
video da li sme da posalje zeljeni request, GraphQL ce ga odbiti
    
zato moramo ovde da presretnemo 'OPTIONS' request i da vratimo response sa statusom 200 -> za dozvoljenoi slanje 
zeljenog requesta

app.js, 70 linija koda
if(req.method === 'OPTIONS'){
    return res.sendStatus(200);
}

7) Validacija
npm install --save validator(express-validator taj packet koristi behind the scenes)

* POSTMAN (ne zaboravi da dodas header-e poput 'Content-Type' i 'Authorization', method je uvek 'POST', URL je uvek http://localhost:8080/graphql)
    body: raw JSON(application/json)
        mutation:
        { "query": "mutation{createPost(postInput: {title:\"Spider Man\", content:\"Spider Man :)\", imageUrl:\"some url\"}){title}}" }
        query:
        { "query": "query{getPosts(page:\"1\"){posts{title}\ntotalItems}}" }
    body: GraphQL
        query:
            QUERY:
                query FetchPosts($page: Int) {
                    getPosts(page: $page){
                        posts {
                        _id
                        title
                        content
                        imageUrl
                        creator {
                            name
                        }
                        createdAt
                        }
                        totalItems
                    }
                }
            GRAPHQL VARIABLES:
                {
                    "page": 1
                }
        ili:
            QUERY:
                query{
                    getUser{
                        status
                    }
                }
        mutation:
            QUERY:
                mutation UpdateUserStatus($userStatus: String) {
                    putUser(userInput: {status: $userStatus}){
                        status
                    }
                }
            GRAPHQL VARIABLES:
                {
                    "status": "new status"
                }

*** BITNO ***
ukoliko u schema-i kao tip parametra koji prosledjujemo f-ji stoji npr. String! to znaci da je taj parametar required

recimo da imamo sledece:
type RootMutation {
        putUser(status: String!): User!
    }

schema {
    mutation: RootMutation
}

to dalje znaci, tj iziskuje od nas da i na frontendu u okviru query-ja oznacimo da je taj parametar 
required(sa !) inace ce nam vratiti gresku:

QUERY:
    mutation UpdateUserStatus($userStatus: String!) {
        putUser(status: $userStatus){
            status
        }
    }
GRAPHQL VARIABLES:
    {
        "status": "new status"
    }

Useful resources:

Detailed Guide on GraphQL: https://graphql.org
---------------------------------------------------------------------------------------------------