const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const mongoConnect = require('./util/database').mongoConnect;
const User = require('./models/user');

const rootDir = require('./util/path');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const errorController = require('./controllers/error');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));

/**
 * ovaj middlewear ce se uvek, pri bilo kom zahtevu izvrsiti i mi cemo za request zakaciti novu vrednost,
 * pod kljucem user -> (req.user = user)
 * potom pozivamo f-ju next() koja nam omogucava da se ide dalje u neki drugi middlewear gde cemo 
 * poslati response
 * u bilo kom od sledecih middlewear-a mocicemo da pristupimo user-u
 */ 
app.use((req, res, next)=>{
    User.findById('5d2b0b70359dd828f8c2f935')
        .then(user=>{
            req.user = new User(user.username, user.email, user._id, user.cart);
            next();
        })
        .catch(err=>console.log(err));
    // next();
});

app.get('/favicon.ico', (req, res, next)=>{
    res.send();
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

mongoConnect(()=>{
    app.listen(3000);
});