const Product = require('../models/product');

module.exports.getAddProduct = (req, res, next)=>{
    res.render('admin/add-edit-product', {
        title: 'Add Product', 
        path: '/admin/add-product',
        editMode: false
    });
};

module.exports.postAddProduct = (req, res, next)=>{
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;

    const product = new Product(title, price, imageUrl, description, null, req.user._id);
    product.save()
        .then(result=>{
            console.log('CREATED PRODUCT!');
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));    
};

module.exports.getEditProduct = (req, res, next)=>{
    const editMode = req.query.edit;
    if(!editMode){
        res.redirect('/');
    }
    const productId = req.params.productId;
    Product.findById(productId)
    // Product.findByPk(productId)
        .then(product=>{
            if(!product){
                res.redirect('/');
            }
            res.render('admin/add-edit-product', {
                title: 'Edit Product', 
                path: '/admin/edit-product',
                editMode: true,
                product: product
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postEditProduct = (req, res, next)=>{
    const product = new Product(
        req.body.title,
        req.body.price,
        req.body.imageUrl,
        req.body.description,
        req.body.id
    );
    product.update()
        .then(result=>{
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));
}

module.exports.getProducts = (req, res, next)=>{
    Product.fetchAll()
        .then(products=>{
            res.render('admin/products', {
                products: products, 
                title: 'Admin Products',
                path: '/admin/products'
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postDeleteProduct = (req, res, next)=>{
    Product.delete(req.body.id)
        .then(result=>{
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));
}