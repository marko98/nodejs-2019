const Product = require('../models/product');
// const Cart = require('../models/cart');
// const Order = require('../models/order');
// const OrderItem = require('../models/order-item');

module.exports.getProducts = (req, res, next)=>{
    Product.fetchAll()
        .then(products=>{
            res.render('shop/product-list', {
                products: products, 
                title: 'All Products', 
                path: '/products'
            });
        })
        .catch(err=>console.log(err));
};

module.exports.getProduct = (req, res, next)=>{
    const productId = req.params.productId;
    Product.findById(productId)
        .then(product=>{
            res.render('shop/product-detail', {
                title: product.title, 
                path: '/products', 
                product: product
            });
        })
        .catch(err=>console.log(err));
};

module.exports.getIndex = (req, res, next)=>{
    Product.fetchAll()
    .then(products=>{
        res.render('shop/index', {
            products: products, 
            title: 'Shop', 
            path: '/'
        });
    })
    .catch(err=>console.log(err));
};

module.exports.getCart = (req, res, next)=>{
    req.user.getCart()
        .then(products=>{
            res.render('shop/cart', {
                title: 'Your Cart',
                path: '/cart',
                productsInCart: products
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postCart = (req, res, next)=>{
    const productId = req.body.id;
    Product.findById(productId)
        .then(product=>{
            return req.user.addToCart(product);
        })
        .then(result=>{
            // console.log(result);
            res.redirect('/cart');
        })
        .catch(err=>console.log(err));
};

module.exports.postDeleteCartProduct = (req, res, next)=>{
    req.user.deleteItemFromCart(req.body.id)
        .then(result=>{
            // console.log(result);
            res.redirect('/cart');
        })
        .catch(err=>console.log(err));
};

// module.exports.getCheckout = (req, res, next)=>{
//     res.render('shop/checkout', {
//         title: 'Checkout',
//         path: '/checkout'
//     });
// }

module.exports.getOrders = (req, res, next)=>{
    req.user.getOrders()
        .then(orders=>{
            // console.log(orders);
            res.render('shop/orders', {
                title: 'Your Orders',
                path: '/orders',
                orders: orders
            });
        })
        .catch(err=>console.log(err));
}

module.exports.postCreateOrder = (req, res, next)=>{
    req.user.addOrder()
        .then(result=>{
            res.redirect('/orders');
        })
        .catch(err=>console.log(err));
}