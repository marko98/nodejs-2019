const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = (callback) => {
    MongoClient.connect('mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/shop?retryWrites=true&w=majority')
        .then(client=>{
            console.log('Connected!');
            _db = client.db('shop');
            callback();
        })
        .catch(err=>{
            console.log(err);
            throw err;
        });
}

const getDb = () => {
    if(_db){
        return _db;
    }
    throw 'No database found!'
}

module.exports.mongoConnect = mongoConnect;
module.exports.getDb = getDb;