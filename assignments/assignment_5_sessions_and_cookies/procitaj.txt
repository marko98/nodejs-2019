1) mongoose - ODM(A Object Document Mapping Library)
npm install --save mongoose

Mongoose Official Docs: https://mongoosejs.com/docs/

2) Cookies
za posatvljanje cookie-a
res.setHeader('Set-Cookie', 'loggedIn=true');
// res.setHeader('Set-Cookie', 'loggedIn=true; Max-Age=10'); Cookie ce biti na browser-u 10s
// res.setHeader('Set-Cookie', 'loggedIn=true; HttpOnly); ne mozemo pristupiti cookie-u od strane javascript-a koji 'trci' na browser-u

kada postavimo cookie browser ga salje sa svakim zahtevom(requestom) na server

*BITNO!!!
vrednost i atribute cookie-a se mogu menjati na browseru

uglavnom se koriste za precenje ljudi(tracking pixel)

obicno se koriste neki paketi koji postavljaju cookie na odredjenji nacin npr. za autentifikaciju

3) Sessions
npm install --save express-session

po default-u sesija se cuva u memoriji i dakle nije 'neogranicena' stoga je za 
produkciju lose resenje -> cuvacemo sesiju u bazi(nas slucaj MongoDB)

npm install --save connect-mongodb-session