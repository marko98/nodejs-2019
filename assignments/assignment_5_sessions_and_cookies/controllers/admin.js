const Product = require('../models/product');

module.exports.getAddProduct = (req, res, next)=>{
    res.render('admin/add-edit-product', {
        title: 'Add Product', 
        path: '/admin/add-product',
        editMode: false,
        isAuthenticated: req.session.isLoggedIn
    });
};

module.exports.postAddProduct = (req, res, next)=>{
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;

    const product = new Product({
        title: title, 
        price: price, 
        description: description, 
        imageUrl: imageUrl,
        /**
         * userId: req.session.user._id ili userId: req.session.user (monogoose ce iz objekta req.session.user izvuci id)
         */
        userId: req.session.user
    });

    product.save()
        .then(result=>{
            console.log('CREATED PRODUCT!');
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));    
};

module.exports.getEditProduct = (req, res, next)=>{
    const editMode = req.query.edit;
    if(!editMode){
        res.redirect('/');
    }
    const productId = req.params.productId;
    Product.findById(productId)
        .then(product=>{
            if(!product){
                res.redirect('/');
            }
            res.render('admin/add-edit-product', {
                title: 'Edit Product', 
                path: '/admin/edit-product',
                editMode: true,
                product: product,
                isAuthenticated: req.session.isLoggedIn
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postEditProduct = (req, res, next)=>{
    Product.findById(req.body.id)
        .then(product => {

            product.title = req.body.title;
            product.price = req.body.price;
            product.description = req.body.description;
            product.imageUrl = req.body.imageUrl;

            return product.save();
        })
        .then(result=>{
            console.log('UPDATED PRODUCT!');
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));
}

module.exports.getProducts = (req, res, next)=>{
    Product.find()
        //.select('title price -_id') // mozes reci koje atribute zelis da dobavis, sa minusom ispre kazes koje ne zelis da dobavis
        .populate('userId') // na mestu gde je referenca(id) moze dobaviti sve atribute za taj dokument, ili navesti koje atribute tog dokumenta zelis
        // npr. .populate('userId', 'username') za samo _id(default) i username
        .then(products=>{
            // console.log(products);
            res.render('admin/products', {
                products: products, 
                title: 'Admin Products',
                path: '/admin/products',
                isAuthenticated: req.session.isLoggedIn
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postDeleteProduct = (req, res, next)=>{
    Product.findByIdAndRemove(req.body.id)
        .then(result=>{
            console.log('PRODUCT DELETED!');
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));
}