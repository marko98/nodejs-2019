const Product = require('../models/product');
const Order = require('../models/order');

module.exports.getProducts = (req, res, next)=>{
    // Product.find() u ovom slucaju sa mongoose ne vraca cursor kao sto je slucaj sa mongoDB driver-om
    Product.find()
        .then(products=>{
            res.render('shop/product-list', {
                products: products, 
                title: 'All Products', 
                path: '/products',
                isAuthenticated: req.session.isLoggedIn
            });
        })
        .catch(err=>console.log(err));
};

module.exports.getProduct = (req, res, next)=>{
    const productId = req.params.productId;
    /**
     * posto koristimo mongoose mozemo kao id proslediti i samo string,
     * a on ce ga pretvoriti u onaj objekat, kako je zapisan u bazi(ObjectId('...'))
     */
    Product.findById(productId)
        .then(product=>{
            res.render('shop/product-detail', {
                title: product.title, 
                path: '/products', 
                product: product,
                isAuthenticated: req.session.isLoggedIn
            });
        })
        .catch(err=>console.log(err));
};

module.exports.getIndex = (req, res, next)=>{
    // Product.find() u ovom slucaju sa mongoose ne vraca cursor kao sto je slucaj sa mongoDB driver-om
    Product.find()
    .then(products=>{
        res.render('shop/index', {
            products: products, 
            title: 'Shop', 
            path: '/',
            isAuthenticated: req.session.isLoggedIn
        });
    })
    .catch(err=>console.log(err));
};

module.exports.getCart = (req, res, next)=>{
    req.session.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user => {
            // console.log(user.cart.items);
            const products = user.cart.items;
            res.render('shop/cart', {
                title: 'Your Cart',
                path: '/cart',
                productsInCart: products,
                isAuthenticated: req.session.isLoggedIn
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postCart = (req, res, next)=>{
    const productId = req.body.id;
    Product.findById(productId)
        .then(product=>{
            return req.session.user.addToCart(product);
        })
        .then(result=>{
            // console.log(result);
            res.redirect('/cart');
        })
        .catch(err=>console.log(err));
};

module.exports.postDeleteCartProduct = (req, res, next)=>{
    req.session.user.deleteItemFromCart(req.body.id)
        .then(result=>{
            // console.log(result);
            res.redirect('/cart');
        })
        .catch(err=>console.log(err));
};

// module.exports.getCheckout = (req, res, next)=>{
//     res.render('shop/checkout', {
//         title: 'Checkout',
//         path: '/checkout'
//     });
// }

module.exports.getOrders = (req, res, next)=>{
    Order.find({'user.userId': req.session.user._id})
        .then(orders=>{
            // console.log(orders);
            res.render('shop/orders', {
                title: 'Your Orders',
                path: '/orders',
                orders: orders,
                isAuthenticated: req.session.isLoggedIn
            });
        })
        .catch(err=>console.log(err));
}

module.exports.postCreateOrder = (req, res, next)=>{
    req.session.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user => {
            // console.log(user.cart.items);
            const products = user.cart.items.map(i => {
                /**
                 * {...i.productId._doc}, ._doc je metadata od strane mongoose-a koja nam omogucava da na mestu gde ocekujemo tip Objekat
                 * zaista i to dobijemo, a ne da sacuvamo ObjectId('...')
                 */
                return {quantity: i.quantity, product: {...i.productId._doc}};
            });
            // console.log(products);
            const order = new Order({
                products: products,
                user: {
                    username: req.session.user.username,
                    userId: req.session.user
                }
            });
            return order.save();
        })
        .then(result=>{
            req.session.user.clearCart();
        })
        .then(() => {
            res.redirect('/orders');
        })
        .catch(err=>console.log(err));
}