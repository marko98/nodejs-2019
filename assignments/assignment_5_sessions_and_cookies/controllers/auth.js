const User = require('../models/user');

module.exports.getLogin = (req, res, next) => {
    console.log(req.session);
    console.log(req.session.isLoggedIn);

    // const isLoggedIn = req.get('Cookie').split(';')[0].trim().split('=')[1] === 'true';
    res.render('auth/login', {
        title: 'Login',
        path: '/login',
        isAuthenticated: req.session.isLoggedIn
    })
}

module.exports.postLogin = (req, res, next) => {
    /**
     * nakon sto posaljemo response request je gotov, 'mrtav' tako da i informacija req.isLoggedIn = true se gubi
     * 
     * req.isLoggedIn = true;
     */

    /**
     * postavljanje cookie-a
     * 
     * res.setHeader('Set-Cookie', 'loggedIn=true; HttpOnly');
     */

    User.findById('5d46a3a1611b833134a36cc0')
        .then(user=>{
            /**
             * postavice cookie po nazivu connect.sid cija ce vrednost biti enkriptovana(docice do premene u sesiji i zbog toga do pamcenja u bazi)
             * uz pomoc njega ce server moci da shvati o kojem user-u je rec tj. koji browser je u pitanju i koji user(instanca web sajta) tj.
             * spojice 'user-a na browser-u' sa sesijom u bazi
             * 
             * BITNO!
             * ukoliko obrisemo cookie na browseru, bilo gde u kodu gde pristupamo sesiji pa potom preko nje
             * user-u on nece biti dostupan jer ni sesija nece biti dostupna, nije pronadjena tj identifikavana zbog obrisanog cookie-a
             */
            req.session.isLoggedIn = true;
            req.session.user = user;
            res.redirect('/');
        })
        .catch(err=>console.log(err)); 
}