const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
/**
 * require('connect-mongodb-session') nam daje f-ju koja prima parametar i vraca konstruktor
 */
const MongoDBStore = require('connect-mongodb-session')(session);
const mongoose = require('mongoose');

const User = require('./models/user');
const rootDir = require('./util/path');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');
const errorController = require('./controllers/error');

const MONGODB_URI = 'mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/shop?retryWrites=true&w=majority';
// Max-u nesto nije radilo pa je napisao ovako: const MONGODB_URI = 'mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/shop';

const app = express();
// inicijalizacija
const store = new MongoDBStore({
    uri: MONGODB_URI,
    // definisemo naziv kolekcije gde ce nam sesija biti cuvana
    collection: 'sessions'
});

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));
/**
 * secret: 'my secret' bi u produkciji trebao da bude dugacak string
 * 
 * resave: false -> sesija ce se sacuvati samo kada dodje do izmene neke(ovo poboljsava performanse)
 * 
 * saveUninitialized: false -> radi slicno sto i resave: false
 * 
 * ovaj middlewear ce dodati req.session kao i cookie(pogledaj auth controller) koji ako sami ne podesimo 
 * ima neke default-ne vrednosti, ali to se dogadja tek kada dodje do neke promene u sesiji jer je tako podesena
 * 
 * store: store -> definisemo gde zelimo da cuvamo sesiju
 */
app.use(session({secret: 'my secret', resave: false, saveUninitialized: false, store: store}))

app.get('/favicon.ico', (req, res, next)=>{
    res.send();
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.use(errorController.get404);

mongoose.connect(MONGODB_URI)
    .then(result=>{
        // User.findOne() vraca prvog kojeg nadje u bazi
        User.findOne()
            .then(user => {
                if(!user){
                    const user = new User({
                        username: 'Marko',
                        email: 'marko@test.com',
                        cart: {
                            items: []
                        }
                    });
                    user.save();
                }
            })
            .catch(err => {
                console.log(err);
            });

        app.listen(3000);
    }).catch(err => {
        console.log(err);
    })