const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const routes = require('./routes/routes');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));

app.set('view engine', 'ejs');
app.set('views', 'views');

app.get('/favicon.ico', (req, res, next)=>{
    // console.log('favicon');
    res.send();
});

app.use(routes.router);

app.use((req, res, next)=>{
    // console.log('page not found');
    res.status(404).render('404', {title: 'Page Not Found'});
});

app.listen(3000);