const express = require('express');

const rootDir = require('../util/path');

const router = express.Router();

const usernames = [];

router.get('/', (req, res, next)=>{
    res.render('main', {title: 'Add Username'});
});

router.post('/add-username', (req, res, next)=>{
    usernames.push(req.body.username);
    res.status(302).redirect('/users');
});

router.get('/users', (req, res, next)=>{
    console.log(usernames);
    res.render('users', {title: 'Users', usernames: usernames});
});

module.exports.router = router;