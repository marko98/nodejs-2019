const express = require('express');

const app = express();

// potrebno jer po nekom default-u dobijam i zahtev metode GET sa url: /favicon.ico
app.use('/favicon.ico', (req, res, next)=>{
    res.send();
});

// app.use((req, res, next)=>{
//     console.log("1 middleware");
//     next();
// });

// app.use((req, res, next)=>{
//     console.log("2 middleware");
//     res.send('<h1>Hi :)</h1>');
// });

// za putanje koje pocinju sa /users
app.use('/users', (req, res, next)=>{
    console.log("/users middleware");
    res.send('<h1>The middleware that handles /users</h1>');
});

app.use('/', (req, res, next)=>{
    console.log("/ middleware");
    res.send('<h1>The middleware that handles just /</h1>');
});

app.listen(3000);