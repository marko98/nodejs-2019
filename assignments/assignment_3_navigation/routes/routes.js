const express = require('express');
const path = require('path');

const rootDir = require('../util/path');

const router = express.Router();

// default-ni url je '/'
router.get('/', (req, res, next)=>{
    console.log('url: ' + req.url + ', metod: ' + req.method + ' -> middleware!');
    res.sendFile(path.join(rootDir, 'views', 'main.html'));
});
router.post('/users', (req, res, next)=>{
    console.log('url: ' + req.url + ', metod: ' + req.method + ' -> middleware!');
    console.log(req.body);
    res.sendFile(path.join(rootDir, 'views', 'users.html'));
});

module.exports.router = router;