const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

// na ovoj lokaciji je rootDir isto sto i __dirname
const rootDir = require('./util/path');
const router = require('./routes/routes');

const app = express();

app.get('/favicon.ico', (req, res, next)=>{
    // console.log('url: ' + req.url + ', metod: ' + req.method + ' -> middleware!');
    res.send();
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(router.router);

app.use((req, res, next)=>{
    res.sendFile(path.join(__dirname, 'views', '404.html'));
});

app.listen(3000);