const fs = require('fs');

const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if(url === '/'){
        res.setHeader('Content-type', 'text/html');
        res.write('<html>');
        res.write('<head><title> Hello </title></head>');
        res.write('<body><p>Hi there :)</p><form action="/create-user" method="POST"><input type="text" name="username"><button type="submit">Send</button></form></body>');
        res.write('</html>');
        return res.end();
    }
    
    if(url === '/users'){
        res.setHeader('Content-type', 'text/html');
        res.write('<html>');
        res.write('<head><title> Hello </title></head>');
        res.write('<body><ul><li>User1</li><li>User2</li></ul></body>');
        res.write('</html>');
        return res.end();
    }
    
    if(url === '/create-user' && method === 'POST'){
        const data = [];
        req.on('data', (chunk) => {
            data.push(chunk);
        });

        req.on('end', (err) => {
            const parsedBody = Buffer.concat(data).toString();
            const username = parsedBody.split('=')[1];
            console.log(username);
            // 302 je kod za redirekciju, bez dodele statusa ne radi!
            // treci argument, anonimna f-ja se poziva nakon sto se upis u fajl zavrsi, za err(error) ce se uzeti null ukoliko ne bude greske a u suprotnom ce dobiti neku vrednost
            fs.writeFile('username.txt', username, (err) => {
                res.statusCode = 302;
                res.setHeader('Location', '/');
                res.end();
            })
        });
    }
    // Nema potrebe za return-om buduci da se ispod ne nalazi nikakav drugi kod

    // if(url === '/create-user' && method === 'POST'){
    //     const data = [];
    //     req.on('data', (chunk) => {
    //         data.push(chunk);
    //     });

    //     return req.on('end', (err) => {
    //         const parsedBody = Buffer.concat(data).toString();
    //         const username = parsedBody.split('=')[1];
    //         console.log(username);
    //         // 302 je kod za redirekciju, bez dodele statusa ne radi!
    //         // treci argument, anonimna f-ja se poziva nakon sto se upis u fajl zavrsi, za err(error) ce se uzeti null ukoliko ne bude greske a u suprotnom ce dobiti neku vrednost
    //         fs.writeFile('username.txt', username, (err) => {
    //             res.statusCode = 302;
    //             res.setHeader('Location', '/');
    //             return res.end();
    //         })
    //     });
    // }
}

module.exports.handler = requestHandler;