"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = require("body-parser");
const book_controller_1 = __importDefault(require("./controller/book.controller"));
const app = express_1.default();
app.use(body_parser_1.json());
app.use((req, res, next) => {
    console.log(req.headers);
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET, POST");
    next();
});
app.use("/books", book_controller_1.default);
app.use("", (req, res, next) => {
    return res.status(400).json({ message: "The API endpoint doesn't exists." });
});
app.listen(3000, "0.0.0.0");
