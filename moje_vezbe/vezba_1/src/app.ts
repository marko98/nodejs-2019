import express from "express";
import { json } from "body-parser";

import bookController from "./controller/book.controller";

const app = express();

app.use(json());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET, POST");
  next();
});

app.use("/books", bookController);

app.use("", (req, res, next) => {
  return res.status(400).json({ message: "The API endpoint doesn't exists." });
});

app.listen(3000, "0.0.0.0");
