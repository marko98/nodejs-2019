import { Router } from "express";
import { Booking } from "../model/class/booking.model-class";
import { BookingModelInterface } from "../model/interface/booking.model-interface";

const router = Router();

let bookings: Booking[] = [
  new Booking(
    0,
    0,
    1,
    "Manhattan Mansion",
    2,
    new Date("2020-09-05"),
    new Date("2020-10-05"),
    "Marko",
    "Zahorodni",
    "https://media.bizj.us/view/img/11343209/gettyimages-1124942340*1200xx2061-1159-0-148.jpg"
  ),
  new Booking(
    1,
    2,
    2,
    "Taipei, Taiwan",
    3,
    new Date("2020-08-03"),
    new Date("2020-08-15"),
    "David",
    "Zahorodni",
    "https://media.bizj.us/view/img/11343209/gettyimages-1124942340*1200xx2061-1159-0-148.jpg"
  ),
];

let bookingsAvailableId = 2;

router.get("", (req, res, next) => {
  return res.status(200).json(bookings);
});

router.post("", (req, res, next) => {
  let bookingModelInterface: BookingModelInterface = req.body;

  if (
    !(
      bookingModelInterface.placeId >= 0 &&
      bookingModelInterface.userId >= 0 &&
      bookingModelInterface.placeTitle &&
      bookingModelInterface.guestNumber >= 0
    )
  )
    return res.status(400).json({ message: "Invalid data" });

  let newBooking: Booking = new Booking(
    bookingsAvailableId,
    bookingModelInterface.placeId,
    bookingModelInterface.userId,
    bookingModelInterface.placeTitle,
    bookingModelInterface.guestNumber,
    new Date(bookingModelInterface.dateFromUTCISOString),
    new Date(bookingModelInterface.dateToUTCISOString),
    bookingModelInterface.userFirstName,
    bookingModelInterface.userLastName,
    bookingModelInterface.placeImgUrl
  );

  bookings.push(newBooking);
  bookingsAvailableId++;

  return res.status(201).json(newBooking);
});

router.put("", (req, res, next) => {
  let updatedBookingModelInterface: BookingModelInterface = req.body;

  if (updatedBookingModelInterface.id === undefined)
    return res.status(400).json({ message: "Invalid data" });

  let theBooking = bookings.find(
    (b) => b.id === updatedBookingModelInterface.id
  );
  if (!theBooking)
    return res.status(404).json({ message: "Booking not found" });

  let updatedBooking: Booking = new Booking(
    updatedBookingModelInterface.id,
    updatedBookingModelInterface.placeId,
    updatedBookingModelInterface.userId,
    updatedBookingModelInterface.placeTitle,
    updatedBookingModelInterface.guestNumber,
    new Date(updatedBookingModelInterface.dateFromUTCISOString),
    new Date(updatedBookingModelInterface.dateToUTCISOString),
    updatedBookingModelInterface.userFirstName,
    updatedBookingModelInterface.userLastName,
    updatedBookingModelInterface.placeImgUrl
  );

  bookings = bookings.map((b) => {
    if (b.id === updatedBookingModelInterface.id) return updatedBooking;
    return b;
  });

  return res.status(200).json(updatedBooking);
});

router.delete("/:bookingId", (req, res, next) => {
  const bookingId = +req.params["bookingId"];
  let booking = bookings.find((b) => b.id === bookingId);
  if (!booking) return res.status(404).json({ message: "Booking not found" });
  bookings = bookings.filter((b) => b.id !== bookingId);
  return res.status(200).json();
});

export default router;
