import express from "express";
import { json } from "body-parser";

import placesRouter from "./controller/places.controller";
import bookingsRouter from "./controller/bookings.controller";

const app = express();

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*"); // zbog isprobavanja sa telefona
  // res.setHeader("Access-Control-Allow-Origin", "http://localhost:8100");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, DELETE, POST, PUT"
  );
  res.setHeader("Access-Control-Allow-Headers", "*");
  return next();
});

app.use(json());
app.use("/places", placesRouter);
app.use("/bookings", bookingsRouter);

app.use("", (req, res, next) => {
  return res.status(400).json({ message: "The API endpoint doesn't exists" });
});

app.listen(3000, "0.0.0.0");
