export class Booking {
  constructor(
    public id: number,
    public placeId: number,
    public userId: number,
    public placeTitle: string,
    public guestNumber: number,
    public dateFrom: Date,
    public dateTo: Date,
    public userFirstName: string,
    public userLastName: string,
    public placeImgUrl: string
  ) {}
}
