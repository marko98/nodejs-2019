export declare interface BookingModelInterface {
  id: number;
  placeId: number;
  userId: number;
  placeTitle: string;
  guestNumber: number;
  dateFromUTCISOString: string;
  dateToUTCISOString: string;
  userFirstName: string;
  userLastName: string;
  placeImgUrl: tring;
}
