"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Booking = void 0;
class Booking {
    constructor(id, placeId, userId, placeTitle, guestNumber, dateFrom, dateTo, userFirstName, userLastName, placeImgUrl) {
        this.id = id;
        this.placeId = placeId;
        this.userId = userId;
        this.placeTitle = placeTitle;
        this.guestNumber = guestNumber;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.placeImgUrl = placeImgUrl;
    }
}
exports.Booking = Booking;
