"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Place = void 0;
class Place {
    constructor(id, title, description, imageUrl, price, dateFromUTCISOString, dateToUTCISOString, userId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.price = price;
        this.dateFromUTCISOString = dateFromUTCISOString;
        this.dateToUTCISOString = dateToUTCISOString;
        this.userId = userId;
    }
}
exports.Place = Place;
