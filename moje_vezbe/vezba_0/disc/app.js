"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = require("body-parser");
const places_controller_1 = __importDefault(require("./controller/places.controller"));
const bookings_controller_1 = __importDefault(require("./controller/bookings.controller"));
const app = express_1.default();
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*"); // zbog isprobavanja sa telefona
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:8100");
    res.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET, DELETE, POST, PUT");
    res.setHeader("Access-Control-Allow-Headers", "*");
    return next();
});
app.use(body_parser_1.json());
app.use("/places", places_controller_1.default);
app.use("/bookings", bookings_controller_1.default);
app.use("", (req, res, next) => {
    return res.status(400).json({ message: "The API endpoint doesn't exists" });
});
app.listen(3000, "0.0.0.0");
