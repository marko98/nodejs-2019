"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const place_model_class_1 = require("../model/class/place.model-class");
const placesRouter = express_1.Router();
let places = [
    new place_model_class_1.Place(0, "Manhattan Mansion", "In the heart of New York City.", "https://media.bizj.us/view/img/11343209/gettyimages-1124942340*1200xx2061-1159-0-148.jpg", 149.99, new Date(new Date("2020-05-10T15:00:00").toUTCString()).toISOString(), new Date(new Date("2020-10-05T15:00:00").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(1, "Taipei, Taiwan", "Taipei is a city with a rich history, though at 300 years young it’s a relative newcomer compared to its neighbors in the East.", "https://media.cntraveler.com/photos/5d7a605e82f7680008172f4e/master/w_820,c_limit/Taipei,-Taiwan_GettyImages-181323651.jpg", 89.99, new Date(new Date("2020-06-08").toUTCString()).toISOString(), new Date(new Date("2020-10-10").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(2, "L'Amour Toujours", "A romantic place in Paris!", "https://photos.mandarinoriental.com/is/image/MandarinOriental/paris-2017-home?wid=2880&hei=1280&fmt=jpeg&crop=9,336,2699,1200&anchor=1358,936&qlt=75,0&fit=wrap&op_sharpen=0&resMode=sharp2&op_usm=0,0,0,0&iccEmbed=0&printRes=72", 189.99, new Date(new Date("2020-01-28").toUTCString()).toISOString(), new Date(new Date("2020-06-26").toUTCString()).toISOString(), 1),
    new place_model_class_1.Place(3, "The Foggy Palace", "Not you average city trip!", "https://i.pinimg.com/originals/de/77/80/de778003f90b396cb7e32b8c7fa0d478.jpg", 99.99, new Date(new Date("2020-02-10").toUTCString()).toISOString(), new Date(new Date("2021-10-02").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(4, "Cologne, Germany", "Cologne is often overshadowed by Berlin and Munich, but the 2,000-year-old city on the banks of the Rhine River has its devotees for a reason—think High Gothic architecture, a dozen Romanesque churches, annual literary festivals, and the Museum Ludwig, one of the most important collections of modern art in Europe.", "https://media.cntraveler.com/photos/5d7a6043b18b620008ca1356/master/w_820,c_limit/Cologne,-Germany_GettyImages-1144624537.jpg", 120.99, new Date(new Date("2019-11-10").toUTCString()).toISOString(), new Date(new Date("2020-12-05").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(5, "Monte Carlo, Monaco", "Surrounded by the Maritime Alps on the shore of the Mediterranean Sea, glamorous Monte Carlo has been made famous through pop culture for its depiction in an array of films, from To Catch a Thief to GoldenEye, and even Cars 2.", "https://media.cntraveler.com/photos/5d7a60524875770008344c6a/master/w_820,c_limit/Monte-Carlo,-Monaco_GettyImages-664656765.jpg", 178.99, new Date(new Date("2019-03-10").toUTCString()).toISOString(), new Date(new Date("2021-10-11").toUTCString()).toISOString(), 1),
    new place_model_class_1.Place(6, "Puebla, Mexico", "The secret is out on Puebla. A popular day trip from Mexico City, Puebla is more than worthy as a destination on its own.", "https://media.cntraveler.com/photos/5d7a6055b18b620008ca1358/master/w_820,c_limit/Puebla,-Mexico_GettyImages-610852938.jpg", 169.99, new Date(new Date("2020-05-15").toUTCString()).toISOString(), new Date(new Date("2021-12-21").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(7, "Florence, Italy", "Though Rome is Italy’s much beloved capital and Milan has serious cosmopolitan clout, Florence remains unrivaled in history, art, and architecture (its beauty and cuisine don’t hurt, either).", "https://media.cntraveler.com/photos/5d7a604d6823450008ac0050/master/w_820,c_limit/Florence,-Italy_GettyImages-995968160.jpg", 109.99, new Date(new Date("2019-05-19").toUTCString()).toISOString(), new Date(new Date("2025-08-15").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(8, "Bergen, Norway", "Surrounded by mountains on Norway’s western shore, Bergen is one of Europe’s largest cruise ship ports of call and the gateway to Norway’s world-famous fjords. Discover the city’s Viking roots at the Bergen Museum and explore postcard-worthy Bryggen, a UNESCO World Heritage site along the harbor with shops, restaurants, and museums.", "https://media.cntraveler.com/photos/5d7a60567ffc50000818c7d0/master/w_820,c_limit/Bergen,-Norway_GettyImages-909592060.jpg", 104.99, new Date(new Date("2020-03-10").toUTCString()).toISOString(), new Date(new Date("2022-06-08").toUTCString()).toISOString(), 1),
    new place_model_class_1.Place(9, "Puerto Vallarta, Mexico", "Puerto Vallarta may have a certain youthful energy during spring break, but there’s so much more to this Pacific resort town than sunbathing undergrads.", "https://media.cntraveler.com/photos/5d7a605b65eba500080bec0d/master/w_820,c_limit/Puerto-Vallarta,-Mexico_GettyImages-970167058.jpg", 99.99, new Date(new Date("2020-09-10").toUTCString()).toISOString(), new Date(new Date("2021-11-11").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(10, "Salzburg, Austria", "Classic Salzburg, the birthplace of Mozart, sits divided by the Salzach River: its pedestrian Old City lines its left bank, and the (slightly) newer side is on the right.", "https://media.cntraveler.com/photos/561e8507799ed1fe16a2cd06/master/w_820,c_limit/Salzburg-Austria-Getty.jpg", 99.99, new Date(new Date("2018-05-05").toUTCString()).toISOString(), new Date(new Date("2019-11-05").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(11, "Québec City, Canada", "Hilltop Québec City is a place for all seasons—the charm of its 17th-century castles and cathedrals is only enhanced by snow (and where else can you say that in Canada?!).", "https://media.cntraveler.com/photos/561e85038bd380d2282d7961/master/w_820,c_limit/Quebec-City-Canada-Alamy.jpg", 124.99, new Date(new Date("2020-05-01").toUTCString()).toISOString(), new Date(new Date("2021-02-05").toUTCString()).toISOString(), 1),
    new place_model_class_1.Place(12, "Dresden, Germany", "Left in ruins at the end of World War II, Dresden stands today as a beautifully restored city in the east of Germany.", "https://media.cntraveler.com/photos/5d7a604808a816000807c090/master/w_820,c_limit/Dresden,-Germany_GettyImages-1154982404.jpg", 149.99, new Date(new Date("2020-01-03").toUTCString()).toISOString(), new Date(new Date("2020-06-04").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(13, "Mérida, Mexico", "The streets of Mérida are bursting with the colorful facades of Spanish colonial architecture, but the capital of Mexico’s Yucatan state is also steeped in Mayan history.", "https://media.cntraveler.com/photos/5d9b883b4b1e400008203571/master/w_820,c_limit/Me%25CC%2581rida,-Mexico__Q3A8713-copy.jpg", 199.99, new Date(new Date("2020-05-09").toUTCString()).toISOString(), new Date(new Date("2020-07-19").toUTCString()).toISOString(), 0),
    new place_model_class_1.Place(14, "Sydney, Australia", "Australia’s biggest city is an ideal getaway no matter the season. Dine al fresco, swim like a Sydneysider in rock pools, and head to Bondi and Redleaf beaches in the summer (remember, that’s during our winter).", "https://media.cntraveler.com/photos/5d7a605cb18b620008ca135b/master/w_820,c_limit/Sydney,-Australia_GettyImages-1065215692.jpg", 65.99, new Date(new Date("2020-02-10").toUTCString()).toISOString(), new Date(new Date("2020-11-12").toUTCString()).toISOString(), 1),
];
let placeAvailableId = 15;
placesRouter.get("", (req, res, next) => {
    return res.status(200).json(places);
});
placesRouter.get("/:placeId", (req, res, next) => {
    const placeId = +req.params["placeId"];
    let place = places.find((p) => p.id === placeId);
    if (!place)
        return res.status(404).json({ message: "Place not found." });
    return res.status(200).json(place);
});
placesRouter.post("", (req, res, next) => {
    let placeModelInterface = req.body;
    if (!(placeModelInterface.title &&
        placeModelInterface.description &&
        placeModelInterface.price >= 0 &&
        placeModelInterface.dateFromUTCISOString &&
        placeModelInterface.dateToUTCISOString &&
        placeModelInterface.userId >= 0))
        return res.status(400).json({ message: "Invalid data" });
    let newPlace = new place_model_class_1.Place(placeAvailableId, placeModelInterface.title, placeModelInterface.description, placeModelInterface.imageUrl
        ? placeModelInterface.imageUrl
        : "https://media.tacdn.com/media/attractions-splice-spp-674x446/0a/6d/5e/59.jpg", placeModelInterface.price, placeModelInterface.dateFromUTCISOString, placeModelInterface.dateToUTCISOString, placeModelInterface.userId);
    places.push(newPlace);
    placeAvailableId++;
    return res.status(201).json(newPlace);
});
placesRouter.put("", (req, res, next) => {
    let updatedPlaceModelInterface = req.body;
    if (updatedPlaceModelInterface.id === undefined)
        return res.status(400).json({ message: "Invalid data" });
    let thePlace = places.find((p) => p.id === updatedPlaceModelInterface.id);
    if (!thePlace)
        return res.status(404).json({ message: "Place not found" });
    let updatedPlace = new place_model_class_1.Place(updatedPlaceModelInterface.id, updatedPlaceModelInterface.title, updatedPlaceModelInterface.description, updatedPlaceModelInterface.imageUrl
        ? updatedPlaceModelInterface.imageUrl
        : "https://media.tacdn.com/media/attractions-splice-spp-674x446/0a/6d/5e/59.jpg", updatedPlaceModelInterface.price, updatedPlaceModelInterface.dateFromUTCISOString, updatedPlaceModelInterface.dateToUTCISOString, updatedPlaceModelInterface.userId);
    places = places.map((p) => {
        if (p.id === updatedPlaceModelInterface.id)
            return updatedPlace;
        return p;
    });
    return res.status(200).json(updatedPlace);
});
placesRouter.delete("/:placeId", (req, res, next) => {
    const placeId = +req.params["placeId"];
    let place = places.find((p) => p.id === placeId);
    if (!place)
        return res.status(404).json({ message: "Place not found" });
    places = places.filter((p) => p.id !== placeId);
    return res.status(200).json();
});
exports.default = placesRouter;
