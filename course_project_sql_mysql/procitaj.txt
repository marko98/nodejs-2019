za rad sa mysql bazom podataka iz NodeJs-a
npm install --save mysql2

sequelize(potreban mu je mysql2) -> 3rd party packet
An Object-Relational Mapping Library
npm install --save sequelize

**UKOLIKO NEMAS ZA NESTO INTELLISENSE probaj da ga instaliras i kao --save-dev**

instaliraj -> https://marketplace.visualstudio.com/items?itemName=jvitor83.types-autoinstaller
pogledaj -> https://stackoverflow.com/questions/48780700/visual-studio-code-intellisense-not-working-for-npm-package

(npr. npm install --save-dev sequelize)
ukoliko zatvoris folder u kojem radis ili ugasis racunar i potom se vratis nazad da radis na projektu
i intellisense opet ne radi slobodno uradi: npm install, pa cak i npm install --save-dev sequelize ukoliko 
intellisense za njega ne radi nakon npm install


1) Buduci da imamo definisano ovo u app.js fajlu:

Product.belongsTo(User, {constraints: true, onDelete: 'CASCADE'}); i
User.hasMany(Product);
User.hasOne(Cart);

sequelize ce stvoriti nove f-je: User.createProduct(), User.getProducts() zvace se getProducts(sa s na kraju, 
jer smo napisali User.hasMany(Product)), User.getCart() (bez s na kraju jer smo napisali User.hasOne(Cart))

2)
Cart.belongsToMany(Product, {through: CartItem});
Product.belongsToMany(Cart, {through: CartItem});

sequelize ce stvoriti novu f-ju: Cart.addProduct() -> f-ja ce kreirati product u tabeli products i dodati
novi record u tablu cartitems, naravno ukoliko u tabeli cart items imamo jos neke atribute sem id-a cart-a i
id-a product-a to treba naglasiti -> Cart.addProduct(product, { through: { quantity: newQuantity }});

