// const mysql = require('mysql2');

// const pool = mysql.createPool({
//     host: 'localhost',
//     user: 'root',
//     database: 'node-complete',
//     password: 'password'
// });

// module.exports = pool.promise();

// prvo veliko slovo jer dobavljamo konstruktora
const Sequelize = require('sequelize');

// 
/**
 * pravimo instancu klase Sequelize i ona ce uraditi izmdju ostalog  i ono gore sto smo pisali ranije,
 * ono sto je zakomentarisano
 */
const sequelize = new Sequelize('node-complete', 'root', 'password', {
    dialect: 'mysql', 
    host: 'localhost'
});

module.exports = sequelize;