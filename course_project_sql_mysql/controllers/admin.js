const Product = require('../models/product');
const Cart = require('../models/cart');

module.exports.getAddProduct = (req, res, next)=>{
    res.render('admin/add-edit-product', {
        title: 'Add Product', 
        path: '/admin/add-product',
        editMode: false
    });
};

module.exports.postAddProduct = (req, res, next)=>{
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;
    // Pogledaj procitaj.txt 1)
    req.user.createProduct({
            title: title, 
            imageUrl: imageUrl, 
            price: price, 
            description: description
        })
        .then(result=>{
            console.log('CREATED PRODUCT!');
            res.redirect('/');
        })
        .catch(err=>console.log(err));    
};

module.exports.getEditProduct = (req, res, next)=>{
    const editMode = req.query.edit;
    if(!editMode){
        res.redirect('/');
    }
    const productId = req.params.productId;
    req.user.getProducts({where: {id: productId}})
    // Product.findByPk(productId)
        .then(products=>{
            const product = products[0];
            if(!product){
                res.redirect('/');
            }
            res.render('admin/add-edit-product', {
                title: 'Edit Product', 
                path: '/admin/edit-product',
                editMode: true,
                product: product
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postEditProduct = (req, res, next)=>{
    Product.findByPk(req.body.id)
        .then(product=>{
            product.title = req.body.title;
            product.imageUrl = req.body.imageUrl;
            product.price = req.body.price;
            product.description = req.body.description;
            return product.save();
        })
        .then(result=>{
            console.log('UPDATED PRODUCT!');
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));
}

module.exports.getProducts = (req, res, next)=>{
    req.user.getProducts()
    // Product.findAll()
        .then(products=>{
            res.render('admin/products', {
                products: products, 
                title: 'Admin Products',
                path: '/admin/products'
            });
        })
        .catch(err=>console.log(err));
};

module.exports.postDeleteProduct = (req, res, next)=>{
    Product.findByPk(req.body.id)
        .then(product=>{
            return product.destroy();
        })
        .then(result=>{
            console.log('DELETED PRODUCT!');
            res.redirect('/admin/products');
        })
        .catch(err=>console.log(err));
}