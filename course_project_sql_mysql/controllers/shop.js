const Product = require('../models/product');
const Cart = require('../models/cart');
const Order = require('../models/order');
const OrderItem = require('../models/order-item');

module.exports.getProducts = (req, res, next)=>{
    Product.findAll()
        .then(products=>{
            res.render('shop/product-list', {
                products: products, 
                title: 'All Products', 
                path: '/products'
            });
        })
        .catch(err=>console.log(err));
};

module.exports.getProduct = (req, res, next)=>{
    const productId = req.params.productId;
    Product.findByPk(productId)
        .then(product=>{
            res.render('shop/product-detail', {
                title: product.title, 
                path: '/products', 
                product: product
            });
        })
        .catch(err=>console.log(err));
};

module.exports.getIndex = (req, res, next)=>{
    Product.findAll()
    .then(products=>{
        res.render('shop/index', {
            products: products, 
            title: 'Shop', 
            path: '/'
        });
    })
    .catch(err=>console.log(err));
};

module.exports.getCart = (req, res, next)=>{
    req.user.getCart()
        .then(cart=>{
            // console.log(cart);
            /**
             * ovde ce sequelize-r odraditi posao buduci da je veza izmedju Cart i Product n:m i stoga
             * je formirana nova tabela cartitems uz pomoc modela CartItem
             * dakle sequelize-r ce iterirati kroz cartitems tabelu
             */
            cart.getProducts()
                .then(products=>{
                    // vraca i cartItem rekord vezan za taj product
                    // console.log(products);
                    res.render('shop/cart', {
                        title: 'Your Cart',
                        path: '/cart',
                        productsInCart: products
                    });
                })
                .catch(err=>console.log(err))
        })
        .catch(err=>console.log(err));
};

module.exports.postCart = (req, res, next)=>{
    const productId = req.body.id;
    let fetchedCart;
    let newQuantity = 1;

    req.user.getCart()
        .then(cart=>{
            fetchedCart = cart;
            return cart.getProducts({where: {id: productId}});
        })
        .then(products=>{
            let product;
            if(products.length > 0){
                product = products[0];
            }

            if(product){
                const oldQuantity = product.cartItem.quantity;
                newQuantity = oldQuantity + 1;
                return product;
            }
            return Product.findByPk(productId);
        })
        .then(product=>{
            /**
             * za Cart.addProduct() pogledaj procitaj.txt 2)
             * 
             * { through: { quantity: newQuantity }} -> buduci da tabela cartitems ima jos neke
             * atribute sem id-a cart-a i id-a product-a treba reci, naglasiti sequelizer-u da
             * i njima dodeli vrednosti
             */
            fetchedCart.addProduct(product, {through: {quantity: newQuantity}});
        })
        .then(()=>{
            res.redirect('/products');
        })
        .catch(err=>console.log(err));
};

module.exports.postDeleteCartProduct = (req, res, next)=>{
    Product.findById(req.body.id, product => {
        Cart.deleteProductFromCart(product.id, product.price, (err)=>{
            // console.log(err);
            res.redirect('/products');
        });
    });
};

module.exports.getCheckout = (req, res, next)=>{
    res.render('shop/checkout', {
        title: 'Checkout',
        path: '/checkout'
    });
}

module.exports.getOrders = (req, res, next)=>{
    req.user.getOrders({include: ['products']})
        .then(orders=>{
            // console.log(orders);
            res.render('shop/orders', {
                title: 'Your Orders',
                path: '/orders',
                orders: orders
            });
        })
        .catch(err=>console.log(err));
}

module.exports.postCreateOrder = (req, res, next)=>{
    let fetchedCart;
    req.user.getCart()
        .then(cart=>{
            fetchedCart = cart;
            return cart.getProducts();
        })
        .then(products=>{
            // console.log(products);
            return req.user.createOrder()
                .then(order=>{
                    return order.addProducts(products.map(product=>{
                        product.orderItem = {quantity: product.cartItem.quantity};
                        return product;
                    }));
                })
                .catch(err=>console.log(err));
        })
        .then(result=>{
            fetchedCart.setProducts(null);
        })
        .then(result=>{
            res.redirect('/orders');
        })
        .catch(err=>console.log(err))
}