const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const sequelize = require('./util/database');
const Product = require('./models/product');
const User = require('./models/user');
const Cart = require('./models/cart');
const CartItem = require('./models/cart-item');
const Order = require('./models/order');
const OrderItem = require('./models/order-item');

const rootDir = require('./util/path');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const errorController = require('./controllers/error');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));

/**
 * ovaj middlewear ce se uvek, pri bilo kom zahtevu izvrsiti i mi cemo za request zakaciti novu vrednost,
 * pod kljucem user -> (req.user = user)
 * potom pozivamo f-ju next() koja nam omogucava da se ide dalje u neki drugi middlewear gde cemo 
 * poslati response
 * u bilo kom od sledecih middlewear-a mocicemo da pristupimo user-u
 */ 
app.use((req, res, next)=>{
    User.findByPk(1)
        .then(user=>{
            req.user = user;
            next();
        })
        .catch(err=>console.log(err));
});

app.get('/favicon.ico', (req, res, next)=>{
    res.send();
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

/**
 * Buduci da imamo definisano ovo:
 * 
 * Product.belongsTo(User, {constraints: true, onDelete: 'CASCADE'}); i
 * User.hasMany(Product);
 * 
 * sequelize ce stvoriti novu f-ju: User.createProduct();
 */
// Product.belongsTo(User, {constraints: true, onDelete: 'CASCADE'});
// User.hasMany(Product);
// Cart.belongsTo(User);
// User.hasOne(Cart);
/**
 * buduci da ove relacije dve dole u sustini formiraju novu tabelu napravili smo cartItem
 * sa ovim -> {through: CartItem} govorimo u koju tabelu zelimo da belezimo podatke
 */
// Cart.belongsToMany(Product, {through: CartItem});
// Product.belongsToMany(Cart, {through: CartItem});

// MOJA POSTAVKA RELACIJA
User.hasMany(Product, {constraints: true, onDelete: 'CASCADE'});
User.hasOne(Cart);
Cart.belongsToMany(Product, {through: CartItem});
Product.belongsToMany(Cart, {through: CartItem});
User.hasMany(Order);
Order.belongsToMany(Product, {through: OrderItem});
Product.belongsToMany(Order, {through: OrderItem});

/**
 * .sync({force: true}) -> ponovo kreira tabele i veze
 * jer inace nece kreirati vezu ukoliko je odredjena tabela vec postojala
 */
sequelize
    // .sync({force: true})
    .sync()
        .then(result=>{
            return User.findByPk(1);            
        })
        .then(user=>{
            if(!user){
                return User.create({name: 'Marko', email: 'test@test.com'});
            }
            /**
             * buduci da unutar if-a vracamo objekat tipa Promise, trebali bi i van if-a
             * npr. return Promise.resolve(user),
             * ali buduci da se nalazimo unutar then() f-je ona ce, iako se vrati samo vrednost
             * od nje napraviti objekat tipa Promise, obuhvati ce je
             */
            return user;
        })
        .then(user=>{
            // console.log(user);
            return user.getCart();            
        })
        .then(cart=>{
            if(!cart){
                User.findByPk(1)
                    .then(user=>{
                        return user.createCart();
                    })
                    .catch(err=>console.log(err));
            }
            return cart;
        })
        .then(cart=>{
            // console.log(cart);
            app.listen(3000);
        })
        .catch(err=>{
            console.log(err);
        });