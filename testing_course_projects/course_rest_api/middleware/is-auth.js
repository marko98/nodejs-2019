const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    /**
     * authHeader -> trebao bi ovako da izgleda:
     * 'Bearer nekavrednosttokena'
     */
    const authHeader = req.get('Authorization');
    if(!authHeader){
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }
    const token = authHeader.split(' ')[1];
    let decodedToken;
    try {
        /**
         * jwt.verify(token, 'somesupersecretsecret') vraca objekat(token) sa
         * payload podacima koji su postavljeni pri kreiranju tokena(controllers/auth.js 66 linija koda),
         * 
         * mogu se videti i na jwt.io stranici prilikom unosa tokena(procitaj.txt 29 linija)
         * 
         * payload-u se pristupa npr. decodedToken.userId
         */
        decodedToken = jwt.verify(token, 'somesupersecretsecret');
    } catch(err) {
        err.statusCode = 500;
        throw err;
    }
    
    if(!decodedToken){
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }

    req.userId = decodedToken.userId;
    next();
}