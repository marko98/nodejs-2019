const expect = require('chai').expect;
const sinon = require('sinon');
const mongoose = require('mongoose');

const feedController = require('../controllers/feed');
const User = require('../models/user');
const Post = require('../models/post');

/**
 * it(...) -> definises test case
 * describe(...) -> obuhvata test case-ove
 * before(...) -> izvrsava se pre svih test case-ova i postavlja neke vrednosti(npr. otvara konekciju ka bazi i pravi user-a)
 * after(...) -> izvrsava se posle svih test case-ova
 * beforeEach(...) -> izvrsava se pre svakog test case-a
 * afterEach(...) -> izvrsava se posle svakog test case-a
 */
describe('Feed Controller', function(){
    before(function(done){
        mongoose
            .connect('mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/test-messages?retryWrites=true&w=majority')
            .then(result => {
                const user = new User({
                    email: 'test@test.com',
                    password: 'tester',
                    name: 'Test',
                    posts: [],
                    _id: '5c0f66b979af55031b34728a'
                });
                return user.save();
            })
            .then(() => {
                done();
            });
    });

    it('should send a response with a valid user status for an existing user', function(done){
        const req = {
            body: {
                title: 'Test Post',
                content: 'A Test Post'
            },
            file: {
                path: 'abc'
            },
            userId: '5c0f66b979af55031b34728a'
        };

        const res = {
            status: function() { 
                return this; 
            }, 
            json: function() {}
        };

        feedController.postPost(req, res, ()=>{})
            .then(savedUser => {
                expect(savedUser).to.have.property('posts');
                expect(savedUser.posts).to.have.length(1);
                done();
            });
    });

    after(function(done){
        // svi user-i su obrisani
        User.deleteMany({}).then(() => {
            return mongoose.disconnect();
        })
        .then(() => {
            done();
        });
    });
})