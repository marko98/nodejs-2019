const expect = require('chai').expect;
const sinon = require('sinon');
const mongoose = require('mongoose');

const authController = require('../controllers/auth');
const User = require('../models/user');

/**
 * it(...) -> definises test case
 * describe(...) -> obuhvata test case-ove
 * before(...) -> izvrsava se pre svih test case-ova i postavlja neke vrednosti(npr. otvara konekciju ka bazi i pravi user-a)
 * after(...) -> izvrsava se posle svih test case-ova
 * beforeEach(...) -> izvrsava se pre svakog test case-a
 * afterEach(...) -> izvrsava se posle svakog test case-a
 */
describe('Auth Controller', function(){
    before(function(done){
        mongoose
            .connect('mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/test-messages?retryWrites=true&w=majority')
            .then(result => {
                const user = new User({
                    email: 'test@test.com',
                    password: 'tester',
                    name: 'Test',
                    posts: [],
                    _id: '5c0f66b979af55031b34728a'
                });
                return user.save();
            })
            .then(() => {
                done();
            });
    });

    it('should throw an error with code 500 if accessing the database fails', function(done){
        sinon.stub(User, 'findOne');
        User.findOne.throws();

        /**
         * ukoliko testiras asinhroni kod(bas sa async i await) moras dodati na kraju, u slucaju greske,
         * dakle u catch blok: return err;
         * ili u slucaju uspeha samo return;
         * pogledaj controllers/auth.js, module.exports.postLogin = async (req, res, next) => {...}
         */

        const req = {
            body: {
                email: 'test@test.com',
                password: 'tester'
            }
        }

        authController.postLogin(req, {}, () => {})
            .then(result => {
                expect(result).to.be.an('error');
                expect(result).to.have.property('statusCode', 500);

                /**
                 * BITNO!!!
                 * ukoliko ne bismo prosledili f-ji done i pozvali ga ovda test bi bio uspesan, a to ne bi valjalo cak i ako bismo 
                 * promenili statusCode da ocekujemo npr. 401
                 * 
                 * done() -> govorimo mocha-i da saceka dok se asinhroni kod ne izvrsi, pa tek onda da proveri rezultat
                 */
                done();
            })

        User.findOne.restore();
    });

    it('should send a response with a valid user status for an existing user', function(done){
        const req = {userId: '5c0f66b979af55031b34728a'};
        const res = {
            statusCode: 500,
            userStatus: null,
            status: function(code) {
                this.statusCode = code;
                return this;
            },
            json: function(data) {
                this.userStatus = data.status;
            }
        };
        authController.getUserStatus(req, res, () =>{})
            .then(() => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.userStatus).to.be.equal('I am new!');
                done();
            });
    });

    after(function(done){
        // svi user-i su obrisani
        User.deleteMany({}).then(() => {
            return mongoose.disconnect();
        })
        .then(() => {
            done();
        });
    });
})