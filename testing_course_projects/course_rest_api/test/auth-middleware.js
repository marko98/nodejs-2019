const expect = require('chai').expect;
const jwt = require('jsonwebtoken');
const sinon = require('sinon');

const authMiddleware = require('../middleware/is-auth');

describe('Auth middleware', function() {
  it('should throw an error if no authorization header is present', function() {
    const req = {
      get: function(headerName) {
        return null;
      }
    };
    expect(authMiddleware.bind(this, req, {}, () => {})).to.throw(
      'Not authenticated.'
    );
  });

  it('should throw an error if the authorization header is only one string', function() {
    const req = {
      get: function(headerName) {
        return 'xyz';
      }
    };
    expect(authMiddleware.bind(this, req, {}, () => {})).to.throw();
  });

  it('should throw an error if the token cannot be verified', function() {
    const req = {
      get: function(headerName) {
        return 'Bearer xyz';
      }
    }

    expect(authMiddleware.bind(this, req, {}, () => {})).to.throw();
  });

  it('should yield a userId after decoding the token', function() {
    const req = {
      get: function(headerName) {
        return 'Bearer asdasdasdsadvcdasv';
      }
    }

    /** 
     * ovo je nezgodno u slucajevima kada se ispod ovog testa pise jos neki
     * koji zahteva normalnu f-ju jwt.verify(), jer ona vise to nece biti, jer smo
     * je mi override-ovali
     * 
     * da bismo to izbegli kortistimo paket sinon koji nam omogucava pravljenje stub-ova, a to
     * znaci izmena koda, ali i mogucnost vracanja na staro(restore, defaultnu f-ju jwt.verify())
     */
    // jwt.verify = () => { 
    //   return {userId: 'abc'}; 
    // };

    /**
     * prvi parametar je objekat koji sadrzi f-ju koja nam treba
     * drugi parametar je tring, naziv f-je koja nam treba
     * 
     * nakon izvrsenja sinon.stub(jwt, 'verify') jwt.verify() postaje stub(prazna f-ja)
     * nad jwt.verify mozemo pozvati:
     * 1) .returns(definisemo sta zelimo da f-ja vraca) -> npr. jwt.verify.returns({userId: 'abc'});
     * 2) .restore(), obicno se poziva na kraju testa, posle expect(...), vraca f-ju na staro(restore) -> npr.:
     * 
     *     expect(req).to.have.property('userId');
     *     jwt.verify.restore();
     */
    sinon.stub(jwt, 'verify');
    jwt.verify.returns({userId: 'abc'});

    authMiddleware(req, {}, () => {});
    expect(req).to.have.property('userId');
    expect(req).to.have.property('userId', 'abc');
    expect(jwt.verify.called).to.be.true;
    jwt.verify.restore();
  });
});
