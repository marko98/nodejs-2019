const path = require('path');
const express = require('express');

const rootDir = require('../util/path');

const router = express.Router();

router.get('/', (req, res, next)=>{
    // console.log('url: ' + req.url + ', metod: ' + req.method + ' -> middleware!');
    // pogledaj 1) u dokumentu procitaj.txt za objasnjenje i detalje
    res.sendFile(path.join(rootDir, 'views', 'shop.html'));
});

module.exports.router = router;