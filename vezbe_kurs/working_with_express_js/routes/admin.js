const path = require('path');
const express = require('express');

const rootDir = require('../util/path');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product', (req, res, next)=>{
    // console.log('url: ' + '/admin/' + req.url + ', metod: ' + req.method + ' -> middleware!');
    // pogledaj 1) u dokumentu procitaj.txt za objasnjenje i detalje
    res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
});

// /admin/add-product => POST
router.post('/add-product', (req, res, next)=>{
    // console.log('url: ' + '/admin/' + req.url + ', metod: ' + req.method + ' -> middleware!');
    console.log(req.body);
    res.redirect('/');
});

module.exports.router = router;