npm init
npm install --express
npm install --save-dev nodemon
npm install --save body-parser, paket za parsiranje body-a u okviru request-a, koristimo --save jer ga zelimo i u produkcionom
okruzenju

-----------
!!!BITNO!!!

const app = express();
const router = express.Router();

metoda use koja se moze pozvati nad app metodom ili objektom tipa Router kao prvi parametar moze primiti definisani DEO 
url-a koji treba da se poklopi sa gadjanim url-om da bi se zahtev prosledio tom middlewear-u

metode get, post, put, delete koje se mogu pozvati nad app metodom ili objektom tipa Router kao prvi parametar mogu primiti 
definisani url(KOMPLETAN, CEO) koji treba da se poklopi sa gadjanim url-om da bi se zahtev prosledio tom middlewear-u
*NAPOMENA* za metode get, post, put, delete nisam siguran, ali izgleda da se mora navesti url bez obzira bio on default-ni('/')
ili ne

npr. gadjamo http://localhost:3000/add-product => url = "/add-product"

ovom middlewear-u ce se proslediti zahtev
app.use('/', (req, res, next)=>{
    next(); // je f-ja koja dozvoljava request-u da ide u sledeci middleware
})

ovom middlewear-u se nece proslediti zahtev
app.get('/', (req, res, next)=>{

})
-----------

1)
const path = require('path');

__dirname nam daje putanju do foldera u kojem se nalazimo(detektuje operativni sistem i na osnovu toga vraca putanju)
'../' vracamo se u jedan folder nazad, a zatim pristupamo folderu views i html dokumentu
npr. res.sendFile(path.join(__dirname, '../', 'views', 'shop.html'));

2) - u fajlu app.js
// express.static(path.join(rootDir, 'public')) za pocetak stavi kursor misa preko static() i vidi sta ti pise,
// u sustini dozvolice za bilo koji fajl koji trazi neke staticne fajlove kao css, js, img itd ukoliko se
// nalaze u folderu public da budu dobavljeni tj poslati toj stranici(html) koja ih potrazuje
app.use(express.static(path.join(rootDir, 'public')));