const path = require('path');

// process.mainModule.filename vraca takoreci fajl koji je glavni koji se pokrece da bi sve osatlo radilo
// path.dirname() ce nam vratiti putanju do fajla kojeg navedemo u zagradi
module.exports = path.dirname(process.mainModule.filename);