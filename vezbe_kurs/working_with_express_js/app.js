const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

// u ovom fajlu nam je __dirname = rootDir tako da cemo koristiti __dirname
const rootDir = require('./util/path');

// buduci da se ovde koristi metoda use i da deo putanje koji mora da se poklopi sa gadjanim url-om svaki
// zahtev ce prvo proci kroz ovaj middlewear gde ce body biti parsiran i zbog toga cemo moci da ga citamo u 
// nekom drugom middlewear-u
// pogledaj !!!BITNO!!! iz procitaj.txt
app.use(bodyParser.urlencoded({extended: false}));

// express.static(path.join(rootDir, 'public')) za pocetak stavi kursor misa preko static() i vidi sta ti pise,
// u sustini dozvolice za bilo koji fajl koji trazi neke staticne fajlove kao css, js, img itd ukoliko se
// nalaze u folderu public da budu dobavljeni tj poslati toj stranici(html) koja ih potrazuje
app.use(express.static(path.join(__dirname, 'public')));

// potrebno jer po nekom default-u dobijam i zahtev metode GET sa url: /favicon.ico
app.get('/favicon.ico', (req, res, next)=>{
    res.send();
});

app.use('/admin', adminRoutes.router);
app.use(shopRoutes.router);

app.use((req, res, next)=>{
    res.status(404).sendFile(path.join(__dirname, 'views', '404.html'));
});

app.listen(3000);