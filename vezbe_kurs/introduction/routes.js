const fs = require('fs');

const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if(url === '/'){
        res.write('<html>');
        res.write('<head><title> Enter message </title></head>');
        res.write('<body><form action="/message" method="POST"><input type="text" name="message"><button type="submit">Send</button></form></body>');
        res.write('</html>');
        return res.end();
    }
    
    if(url === '/message' && method === 'POST'){
        const body = [];
    
        req.on('data', (chunk) => {
            console.log(chunk);
            body.push(chunk);
        });
    
        return req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            console.log(parsedBody);
            const message = parsedBody.split('=')[0];
            // sinhrono upisivanje treba izbegavati, eventualno ako znamo da je bas neki mali podatak(male velicine), ali i onda
            // fs.writeFileSync('message.txt', message);
    
            // asinhrono upisivanje
            fs.writeFile('message.txt', message, (err) => {
                // 302 je kod za redirekciju, bez dodele statusa ne radi!
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end();
            });
        })
    }
    
    res.setHeader('Content-type', 'text/html');
    res.write('<html>');
    res.write('<head><title> My Title </title></head>');
    res.write('<body><h1>Hello from my first Node.js Server!</h1></body>');
    res.write('</html>');
    res.end();
}

// module.exports = {
//     handler: requestHandler,
//     someText: 'Some hard coded text'
// };

// module.exports.handler = requestHandler;
// module.exports.someText = 'Some hard coded text';

// postoji i kraci zapis
exports.handler = requestHandler;
exports.someText = 'Some hard coded text';