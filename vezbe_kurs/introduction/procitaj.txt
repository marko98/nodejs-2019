npm init sluzi da se postave neke osnovne stvari za projekat na kome radimo
stvorice package.json file
C:\Users\minag\Desktop\nodejs\vezbeKurs\introduction> npm init

unutar package.json fajla, pod "scripts" mozemo dodati
neke vec postojece kljuceve kao "start" potom u terminalu izvrsiti: npm start
doduse moguce je dodati i svoje kljuceve(npr. "start-server") ali se u terminalu onda izvrsava: npm run start-server

u okviru package.json fajla moze da postoji i kljuc "devDependencies"
koji sadrzi neke dodatne pakete koji su instalirani kao development dependencies ili
kljuc "dependencies" koji sadrzi neke dodatne pakete koji su instalirani kao production dependencies

pri instaliranju dodatnih paketa stvara se folder node_modules koji moze da zauzima prilicno mesta
stoga mozemo ga obrisati prilikom slanja na git ili deljenja, a prilikom rada na projektu
ukoliko imamo neke dodatne pakete ali nemamo folder node_modules mozemo izvrsiti komandu: npm install
u terminalu i ona ce instalirati ili update-ovati sve dodatne pakete -> stvorice folder node_modules

prilikom instaliranja dodatnog paketa postoji par opcija:
npr. hocemo da instaliramo nodemon koji ce nam omoguciti restartovanje servera(aplikacije) prilikom pamcenja(ctrl+s) usled neke izmene
npm install --save nodemon (production dependencies)
npm install --save-dev nodemon (development dependencies)
ukoliko smo instalirali nodemon na nacin iznad (npm install nodemon --save-dev) komanda u terminalu nodemon app.js nece raditi buduci da se u terminalu
koriste globani paketi i zbog toga ta komanda nije vidljiva
npm install -g nodemon (global dependencies)

prilikom instaliranja nekog od dodatdnih paketa stvorice se i fajl package-lock.json, a on sluzi
da ukoliko jos neko uzme da radi na projektu moze da instalira bas te verzije paketa, a ne neke druge

za podesavanje debuger-a, konfiguracioni fajl(Debug->Open Configurations) treba da bude otprilike ovako podesen:

{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Launch Program",
            "program": "${workspaceFolder}\\app.js",
            "restart": true,
            "runtimeExecutable": "nodemon",
            "console": "integratedTerminal",
        }
    ]
}

pocetno podesavnaje izgleda otprilike ovako:

{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Launch Program",
            "program": "${workspaceFolder}\\app.js",
        }
    ]
}

nakon podesavanja moze doci do greske zbog "runtimeExecutable": "nodemon", jer nije globalno instaliran
da bismo resili ovaj problem izvrsiti komandu u terminalu: npm install -g nodemon
