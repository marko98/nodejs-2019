const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
// buduci da express-handlebars nije ugradjen engine template u express moramo ga dodati
// const expressHandlebars = require('express-handlebars');

const rootDir = require('./util/path');
const adminData = require('./routes/admin');
const shopData = require('./routes/shop');

const app = express();

/*
    - app.engine('handlebars', expressHandlebars()) korisitmo f-ju engine() kada neki engine nije 'ugradjen'

    - prvi parametar je za ime(definise extenziju samog fajla -> dakle imacemo npr. 404.handlebars unutar views foldera), 
      treba obratiti paznju da se ne podudari sa nekim vec postavljenim engine-om npr. pug
    - drugi parametar je za inicijalizaciju samog engine template-a(poziamo f-ju koja ce to da uradi)
*/

// app.engine('handlebars', expressHandlebars({layoutsDir: 'views/layouts/', defaultLayout: 'main-layout', extname: 'handlebars'}));

/*
    - postavlja odredjeni template engine koji app potom koristi

    - buduci da je pug engine template vec ugradjen u express nismo morali da ga dodajemo(const ... = require('...')) i 
      nismo morali da izvsimo f-ju app.engine() buduci da ona inicijalizuje template enigne koji nije vec 'ugradjen'

    - app.set('view engine', 'pug') => mogli smo da koristimo f-ju set() jer je pug template engine 'ugradjen' u express
*/

// pug template engine jeste ugradjen engine template u express
// app.set('view engine', 'pug');

// express-handlebars nije ugradjen engine template u express moramo ga dodati
// app.set('view engine', 'handlebars');

// ejs template engine jeste ugradjen engine template u express
app.set('view engine', 'ejs');

app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));

app.get('/favicon.ico', (req, res, next)=>{
    res.send();
});

app.use('/admin', adminData.router);
app.use(shopData.router);

app.use((req, res, next)=>{
    // res.status(404).sendFile(path.join(__dirname, 'views', '404.html'));

    /*
    - buduci da smo setovali ovo: app.set('view engine', 'handlebars') nema potrebe da navodimo u putanji 404.handlebars,
        jer ce svejedno express tragati za tom extenzijom

    - nema potrebe za ovim: res.render('shop.pug');
    */

    /*
    - kod template engine-a express-handlebars bitno je navesti dok jos nemas layouts/main.handlebars 
      res.status(404).render('404', {title: 'Page Not Found', layout: false}) <- ovaj layout: false, da ne bi tragao za njim,
      ako imamo layout -> layout: false treba izbaciti inace nece raditi
      
    - kod template engine-a pug on nema taj problem
    */
    // NE ZABORAVI NA STATUS
    res.status(404).render('404', {title: 'Page Not Found'});
});

app.listen(3000);