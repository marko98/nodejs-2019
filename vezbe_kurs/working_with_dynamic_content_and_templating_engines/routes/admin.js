const express = require('express');
const path = require('path');

const rootDir = require('../util/path');

const router = express.Router();

const products = [];

// /admin/add-product => GET
router.get('/add-product', (req, res, next)=>{
    // console.log('url: ' + '/admin/' + req.url + ', metod: ' + req.method + ' -> middleware!');
    // res.sendFile(path.join(rootDir, 'views', 'add-product.html'));

    // za layout: false pogledaj procitaj.txt 1)
    res.render('add-product', {
                                title: 'Add Product', 
                                path: '/admin/add-product', 
                                activeAddProduct: true,
                                productCSS: true,
                                formsCSS: true
                            });
});

// /admin/add-product => POST
router.post('/add-product', (req, res, next)=>{
    // console.log('url: ' + '/admin/' + req.url + ', metod: ' + req.method + ' -> middleware!');
    this.products.push({title: req.body.title})
    console.log(this.products);
    res.redirect('/');
});

module.exports.router = router;
module.exports.products = products;