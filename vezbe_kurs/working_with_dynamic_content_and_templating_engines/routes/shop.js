const express = require('express');
const path = require('path');

const rootDir = require('../util/path');
const adminData = require('./admin');

const router = express.Router();

router.get('/', (req, res, next)=>{
  // console.log('url: ' + req.url + ', metod: ' + req.method + ' -> middleware!');

  /*
  - buduci da smo setovali ovo: app.set('view engine', 'pug') u app.js dokumentu, nema potrebe da navodimo u putanji shop.pug,
    jer ce svejedno express tragati za tom extenzijom

  - nema potrebe za ovim: res.render('shop.pug');

  - ne korisitmo res.sendFile(...) jer zelimo da saljemo dinamicke fajlove 
    pa ih renderujemo sa podacima koje im prosledimo i onda ih saljemo kao odgovor 
   
  - za layout: false pogledaj procitaj.txt 1)

  - BITNO -> vrednosti koje ne prosledimo, a trazne su u template-u tretiraju se kao false
  */
  res.render('shop', {
                      products: adminData.products, 
                      title: 'Shop', 
                      path: '/', 
                      hasProducts: adminData.products.length > 0, 
                      activeShop: true,
                      productCSS: true
                    });
});

module.exports.router = router;