const Product = require('../models/product');

module.exports.getAddProduct = (req, res, next)=>{
    res.render('add-product', {
                                title: 'Add Product', 
                                path: '/admin/add-product', 
                                activeAddProduct: true,
                                productCSS: true,
                                formsCSS: true
                            });
};

module.exports.postAddProduct = (req, res, next)=>{
    const product = new Product(req.body.title);
    product.save();
    res.redirect('/');
};

module.exports.getProducts = (req, res, next)=>{
    Product.fetchAll((products)=>{
        res.render('shop', {
            products: products, 
            title: 'Shop', 
            path: '/', 
            hasProducts: products.length > 0, 
            activeShop: true,
            productCSS: true
          });
    });
};