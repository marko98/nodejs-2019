const products = [];

const fs = require('fs');
const path = require('path');

const rootDir = require('../util/path');

const p = path.join(rootDir, 'data', 'products.json');

// cb - callback(ne mora bas cb, moze bilo sta)
const getAllProducts = cb => {
    // fs.readFile(...) je asinhrona f-ja
    fs.readFile(p, (err, data)=>{
        if(!err){
            cb(JSON.parse(data));
        } else {
            cb([]);
        };
    });
}

module.exports = class Product{
    constructor(title){
        this.title = title;
    }

    save(){
        getAllProducts(products=>{
            products.push(this);
            fs.writeFile(p, JSON.stringify(products), (err)=>{
                console.log(err);
            });
        });
    }

    static fetchAll(cb){
        getAllProducts(cb);
    }
}