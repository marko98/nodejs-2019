const express = require('express');
const path = require('path');
const { check, body } = require('express-validator/check');

const rootDir = require('../util/path');
const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product', isAuth, adminController.getAddProduct);

// /admin/add-product => POST
router.post('/add-product', isAuth, [
    body(
        'title',
        'Please enter a title with only numbers and text and at least 3 characters.'
        )
        .isLength({min: 3})
        .isString()
        // brise white space-ove na pocetku i kraju
        .trim(),
    // body('imageUrl')
    //     .isURL()
    //     .withMessage('Please enter a valid image url.'),
    body('price')
        .isFloat({gt: 10})
        .withMessage('Please enter a valid price.'),
    body('description')
        .trim()
        .isLength({min: 5, max: 400})
        .withMessage('Please enter a description with at least 5 characters.')
], adminController.postAddProduct);

// /admin/edit-product/:productId => GET
router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

// // /admin/edit-product => POST
router.post('/edit-product', isAuth, [
    body(
        'title',
        'Please enter a title with only numbers and text and at least 3 characters.'
        )
        .isString()
        .isLength({min: 3})
        .trim(),
    // body('imageUrl')
    //     .isURL()
    //     .withMessage('Please enter a valid image url.'),
    body('price')
        .isFloat({gt: 10})
        .withMessage('Please enter a valid price.'),
    body('description')
        .trim()
        .isLength({min: 5, max: 400})
        .withMessage('Please enter a description with at least 5 characters.')
], adminController.postEditProduct);

// /admin/products => GET
router.get('/products', isAuth, adminController.getProducts);

// /admin/delete-product => POST
// router.post('/delete-product', isAuth, adminController.postDeleteProduct);

// /admin/product/:productId => DELETE
router.delete('/product/:productId', isAuth, adminController.deleteProduct);

module.exports = router;