const fs = require('fs');
const path = require('path');

// require('pdfkit') vraca konstruktor
const PDFDocument = require('pdfkit');
const stripe = require('stripe')('sk_test_hML0AqkBSBFDT9W3Zg3AzHTL00QKOy8TVs');

const rootDir = require('../util/path');
const Product = require('../models/product');
const Order = require('../models/order');

const ITEMS_PRE_PAGE = 2;

module.exports.getProducts = (req, res, next)=>{
    // req.query.page || 1 znaci da ce page dobiti vrednost 1 ukoliko je req.query.page undefined
    const page = +req.query.page || 1;
    let totalProducts;

    Product.countDocuments()
        .then(numProducts => {
            totalProducts = numProducts;

            // Product.find() u ovom slucaju sa mongoose ne vraca cursor kao sto je slucaj sa mongoDB driver-om
            return Product.find()
                        .skip((page - 1) * ITEMS_PRE_PAGE)
                        .limit(ITEMS_PRE_PAGE);
        })
        .then(products=>{
            res.render('shop/product-list', {
                products: products, 
                pageTitle: 'All Products', 
                path: '/products',
                totalProducts: totalProducts,
                currentPage: page,
                hasNextPage: totalProducts > page * ITEMS_PRE_PAGE,
                nextPage: page + 1,
                hasPreviousPage: page > 1,
                previousPage: page - 1,
                lastPage: Math.ceil(totalProducts / ITEMS_PRE_PAGE)
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.getProduct = (req, res, next)=>{
    const productId = req.params.productId;
    /**
     * posto koristimo mongoose mozemo kao id proslediti i samo string,
     * a on ce ga pretvoriti u onaj objekat, kako je zapisan u bazi(ObjectId('...'))
     */
    Product.findById(productId)
        .then(product=>{
            res.render('shop/product-detail', {
                pageTitle: product.title, 
                path: '/products', 
                product: product                
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.getIndex = (req, res, next)=>{
    // req.query.page || 1 znaci da ce page dobiti vrednost 1 ukoliko je req.query.page undefined
    const page = +req.query.page || 1;
    let totalProducts;

    Product.countDocuments()
        .then(numProducts => {
            totalProducts = numProducts;

            // Product.find() u ovom slucaju sa mongoose ne vraca cursor kao sto je slucaj sa mongoDB driver-om
            return Product.find()
                        .skip((page - 1) * ITEMS_PRE_PAGE)
                        .limit(ITEMS_PRE_PAGE);
        })
        .then(products=>{
            res.render('shop/index', {
                products: products, 
                pageTitle: 'Shop', 
                path: '/',
                totalProducts: totalProducts,
                currentPage: page,
                hasNextPage: totalProducts > page * ITEMS_PRE_PAGE,
                nextPage: page + 1,
                hasPreviousPage: page > 1,
                previousPage: page - 1,
                lastPage: Math.ceil(totalProducts / ITEMS_PRE_PAGE)
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.getCart = (req, res, next)=>{
    req.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user => {
            // console.log(user.cart.items);
            const products = user.cart.items;
            res.render('shop/cart', {
                pageTitle: 'Your Cart',
                path: '/cart',
                productsInCart: products                
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.postCart = (req, res, next)=>{
    const productId = req.body.id;
    Product.findById(productId)
        .then(product=>{
            return req.user.addToCart(product);
        })
        .then(result=>{
            // console.log(result);
            res.redirect('/cart');
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.postDeleteCartProduct = (req, res, next)=>{
    req.user.deleteItemFromCart(req.body.id)
        .then(result=>{
            // console.log(result);
            res.redirect('/cart');
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
};

module.exports.getOrders = (req, res, next)=>{
    Order.find({'user.userId': req.user._id})
        .then(orders=>{
            // console.log(orders);
            res.render('shop/orders', {
                pageTitle: 'Your Orders',
                path: '/orders',
                orders: orders                
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

module.exports.postCreateOrder = (req, res, next)=>{
    let total = 0;
    req.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user => {
            user.cart.items.forEach(p => {
                total += p.quantity * p.productId.price;
            });

            // console.log(user.cart.items);
            const products = user.cart.items.map(i => {
                /**
                 * {...i.productId._doc}, ._doc je metadata od strane mongoose-a koja nam omogucava 
                 * da na mestu gde ocekujemo tip Objekat zaista i to dobijemo, a ne da 
                 * sacuvamo ObjectId('...')
                 */
                return {quantity: i.quantity, product: {...i.productId._doc}};
            });
            // console.log(products);
            const order = new Order({
                products: products,
                user: {
                    email: req.user.email,
                    userId: req.user
                }
            });
            return order.save();
        })
        .then(result=>{
            const charge = stripe.charges.create({
                source: req.body.stripeToken,
                description: "Demo order",
                amount: total * 100,
                currency: 'usd',
                metadata: {
                    order_id: result._id.toString()
                }
            });

            return req.user.clearCart();
        })
        .then(() => {
            res.redirect('/orders');
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

module.exports.getCheckout = (req, res, next)=>{
    req.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user => {
            const products = user.cart.items;
            let total = 0;
            products.forEach(p => {
                total += p.quantity * p.productId.price;
            });
            res.render('shop/checkout', {
                pageTitle: 'Checkout',
                path: '/checkout',
                productsInCart: products,
                total: total
            });
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

module.exports.getInvoice = (req, res, next) => {
    const orderId = req.params.orderId;

    Order.findById(orderId)
        .then(order => {
            if(!order){
                return next(new Error('No order found.'));
            }
            if(order.user.userId.toString() !== req.user._id.toString()){
                return next(new Error('Unauthorized.'));
            }
            const invoiceName = 'invoice-' + orderId + '.pdf';
            const invoicePath = path.join(rootDir, 'data', 'invoices', invoiceName);

            // -------- Kreiranje pdf fajla, njegovo skladistenje i slanje na browser -------------
            const pdfDoc = new PDFDocument();
            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'inline; filename="' + invoiceName + '"');

            /**
             * podaci koji se budu upisivali u pdf ce biti u delovima slani i u 
             * fs.createWriteStream(invoicePath) koji ce stvoriti taj pdf na fajl sistemu, a i u
             * res koji ce ih slati na browser
             */
            pdfDoc.pipe(fs.createWriteStream(invoicePath));
            pdfDoc.pipe(res);

            pdfDoc.fontSize(26).text('Invoice', {
                underline: true
            });            
            pdfDoc.fontSize(16).text('\n-----------------------------------------\n\n');

            let total = 0;
            order.products.forEach(prod => {
                total += prod.product.price * prod.quantity;
                pdfDoc.text(prod.product.title + ' - ' + prod.quantity + ' x $' + prod.product.price);
            });
            pdfDoc.text('\n----------------------');
            pdfDoc.fontSize(20).text('Total Price: $' + total);

            pdfDoc.end();
            // ------------------------------------------------------------------------------------

            /**
             * ovaj nacin download-ovanja fajlova i nije bas najbolji jer nodeJs prvo cita ceo fajl, 
             * smesta ga u memoriju i tek onda salje browser-u
             * 
             * za vece fajlove to ce potrajati, a zbog ostalih requestova postoji mogucnost da
             * se popuni sva memorija i onda smo u problemu
             */
            // fs.readFile(invoicePath, (err, data) => {
            //     if(err){
            //         return next(err);
            //     }
            //     res.setHeader('Content-Type', 'application/pdf');
            //     // sa ovakvim hederom, koji ima ovu vrednost -> browser ce ga prikazati na prozoru
            //     res.setHeader('Content-Disposition', 'inline; filename="' + invoiceName + '"');

            //     /**
            //      * sa ovakvim hederom, koji ima ovu vrednost -> browser ce ponuditi da se download-uje file,
            //      * doduse i dalje postoji mogucnost da ce ga samo prikazati, zavisi od toga kako je browser podesen
            //      */
            //     // res.setHeader('Content-Disposition', 'attachment; filename="' + invoiceName + '"');
            //     return res.send(data);
            // })

            /**
             * ovakav nacin download-ovanja je mnogo bolji jer stream-uje delic po delic browseru,
             * dakle sacuvamo delic u memoriju(const file) i potom koristimo pipe da ga preusmerimo
             * na res(on je izmedju ostalog writable stream objekat) koji ce te delice slati browser-u, a
             * on ce ih spojiti u jedan fajl
             * 
             * tako da sto se tice memorije pamtimo samo delic po delic fajla
             */
            // const file = fs.createReadStream(invoicePath);
            // res.setHeader('Content-Type', 'application/pdf');
            // // sa ovakvim hederom, koji ima ovu vrednost -> browser ce ga prikazati na prozoru
            // res.setHeader('Content-Disposition', 'inline; filename="' + invoiceName + '"');

            /**
             * sa ovakvim hederom, koji ima ovu vrednost -> browser ce ponuditi da se download-uje file,
             * doduse i dalje postoji mogucnost da ce ga samo prikazati, zavisi od toga kako je browser podesen
             */
            // res.setHeader('Content-Disposition', 'attachment; filename="' + invoiceName + '"');
            // file.pipe(res);
        })
        .catch(err=>{
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}