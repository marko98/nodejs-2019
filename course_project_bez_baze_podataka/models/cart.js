const fs = require('fs');
const path = require('path');

const rootDir = require('../util/path');

const p = path.join(rootDir, 'data', 'cart.json');

module.exports = class Cart{
    static addProduct(id, productPrice){
        fs.readFile(p, (err, data)=>{
            let cart = {products: [], totalPrice: 0};
            if(!err){
                cart = JSON.parse(data);
            }
            const existingProductIndex = cart.products.findIndex(p => p.id === id);
            const existingProduct = cart.products[existingProductIndex];
            let updatedProduct;
            if(existingProduct){
                updatedProduct = {...existingProduct};
                updatedProduct.quantity = updatedProduct.quantity + 1;
                cart.products = [...cart.products];
                cart.products[existingProductIndex] = updatedProduct;
            } else {
                updatedProduct = {id: id, quantity: 1};
                cart.products = [...cart.products, updatedProduct];
            }
            cart.totalPrice = cart.totalPrice + +productPrice;
            fs.writeFile(p, JSON.stringify(cart), (err)=>{
                // console.log(err);
            });
        })
    }

    static deleteProductFromCart(id, price, cb){
        price = +price;
        fs.readFile(p, (err, data)=>{
            data = JSON.parse(data);
            if(!err){
                const productsToDelete = data.products.filter(p => p.id === id);
                if(productsToDelete.length < 1){
                    return cb('There is no product in cart to delete!');
                }
                data.products = data.products.filter(p => p.id !== id);
                data.totalPrice = data.totalPrice - price*productsToDelete[0].quantity;
                // console.log(productsToDelete);
                // console.log(data);
                fs.writeFile(p, JSON.stringify(data), (err)=>{
                    cb(err);
                });
            }
        });
    }

    static getCartData(cb){
        fs.readFile(p, (err, data)=>{
            if(!err){
                cb(JSON.parse(data));
            } else {
                cb(null);
            }
        });
    }
}