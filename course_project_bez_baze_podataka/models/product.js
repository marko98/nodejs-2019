const products = [];

const fs = require('fs');
const path = require('path');

const rootDir = require('../util/path');

const p = path.join(rootDir, 'data', 'products.json');

const getAllProducts = cb => {
    fs.readFile(p, (err, data)=>{
        if(!err){
            cb(JSON.parse(data));
        } else {
            cb([]);
        };
    });
}

module.exports = class Product{
    constructor(title, imageUrl, price, description){
        this.title = title;
        this.imageUrl = imageUrl;
        this.price = price;
        this.description = description;
    }

    save(){
        this.id = Math.random().toString();
        getAllProducts(products=>{
            products.push(this);
            fs.writeFile(p, JSON.stringify(products), (err)=>{
                console.log(err);
            });
        });
    }

    update(id, cb){
        getAllProducts(products=>{
            const productToUpdateIndex = products.findIndex(p => p.id === id);
            products[productToUpdateIndex] = {...this, id: id};
            fs.writeFile(p, JSON.stringify(products), (err)=>{
                cb(err);
            });
        });
    }

    static delete(id, cb){
        getAllProducts(products=>{
            products = products.filter(p => p.id !== id);
            fs.writeFile(p, JSON.stringify(products), (err)=>{
                cb(err);
            });
        });
    }

    static fetchAll(cb){
        getAllProducts(cb);
    }

    static findById(id, cb){
        getAllProducts(products => {
            const product = products.find(p => p.id === id);
            cb(product);
        });
    }
}