const Product = require('../models/product');
const Cart = require('../models/cart');

module.exports.getProducts = (req, res, next)=>{
    Product.fetchAll((products)=>{
        res.render('shop/product-list', {
            products: products, 
            title: 'All Products', 
            path: '/products'
          });
    });
};

module.exports.getProduct = (req, res, next)=>{
    const productId = req.params.productId;
    Product.findById(productId, product => {
        res.render('shop/product-detail', {title: product.title, path: '/products', product: product});
    });
};

module.exports.getIndex = (req, res, next)=>{
    Product.fetchAll((products)=>{
        res.render('shop/index', {
            products: products, 
            title: 'Shop', 
            path: '/'
          });
    });
};

module.exports.getCart = (req, res, next)=>{
    Cart.getCartData(cart => {
        let productsInCart = [];
        Product.fetchAll(products => {
            cart.products.forEach(p => {
                const product = products.find(product => product.id === p.id);
                if(product){
                    productsInCart.push({...product, quantity: p.quantity});
                }
            });
            // console.log(productsInCart);
            res.render('shop/cart', {
                title: 'Your Cart',
                path: '/cart',
                productsInCart: productsInCart,
                totalPrice: cart.totalPrice
            });
        });
    });
};

module.exports.postCart = (req, res, next)=>{
    const productId = req.body.id;
    Product.findById(productId, (product)=>{
        Cart.addProduct(productId, product.price);
    });
    res.redirect('/products');
};

module.exports.postDeleteCartProduct = (req, res, next)=>{
    Product.findById(req.body.id, product => {
        Cart.deleteProductFromCart(product.id, product.price, (err)=>{
            // console.log(err);
            res.redirect('/products');
        });
    });
};

module.exports.getCheckout = (req, res, next)=>{
    res.render('shop/checkout', {
        title: 'Checkout',
        path: '/checkout'
    });
}

module.exports.getOrders = (req, res, next)=>{
    res.render('shop/orders', {
        title: 'Your Orders',
        path: '/orders'
    });
}