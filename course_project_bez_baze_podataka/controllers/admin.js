const Product = require('../models/product');
const Cart = require('../models/cart');

module.exports.getAddProduct = (req, res, next)=>{
    res.render('admin/add-edit-product', {
                                title: 'Add Product', 
                                path: '/admin/add-product',
                                editMode: false
                            });
};

module.exports.postAddProduct = (req, res, next)=>{
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;
    const product = new Product(title, imageUrl, price, description);
    product.save();
    res.redirect('/');
};

module.exports.getEditProduct = (req, res, next)=>{
    const editMode = req.query.edit;
    if(!editMode){
        res.redirect('/');
    }
    const productId = req.params.productId;
    Product.findById(productId, product=>{
        if(!product){
            res.redirect('/');
        }
        res.render('admin/add-edit-product', {
            title: 'Edit Product', 
            path: '/admin/edit-product',
            editMode: true,
            product: product
        });
    });
};

module.exports.postEditProduct = (req, res, next)=>{
    const updatedProduct = new Product(req.body.title, req.body.imageUrl, req.body.price, req.body.description);
    updatedProduct.update(req.body.id, (err) => {        
        // console.log(err);
        res.redirect('/admin/products');
    });
}

module.exports.getProducts = (req, res, next)=>{
    Product.fetchAll((products)=>{
        res.render('admin/products', {
            products: products, 
            title: 'Admin Products',
            path: '/admin/products'
          });
    });
};

module.exports.postDeleteProduct = (req, res, next)=>{
    Product.delete(req.body.id, (err)=>{
        if(!err){
            Cart.deleteProductFromCart(req.body.id, req.body.price, (err)=>{
                console.log(err);
                res.redirect('/admin/products');
            });
        }
    });
}